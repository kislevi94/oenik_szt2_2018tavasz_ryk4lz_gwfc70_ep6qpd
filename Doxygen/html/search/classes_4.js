var searchData=
[
  ['iaccommodationmanagement',['IAccommodationManagement',['../interface_logic_1_1_i_accommodation_management.html',1,'Logic']]],
  ['iauthentication',['IAuthentication',['../interface_logic_1_1_i_authentication.html',1,'Logic']]],
  ['icommentmanagement',['ICommentManagement',['../interface_logic_1_1_i_comment_management.html',1,'Logic']]],
  ['idata',['IData',['../interface_data_layer_1_1_i_data.html',1,'DataLayer']]],
  ['imessagemanagement',['IMessageManagement',['../interface_logic_1_1_i_message_management.html',1,'Logic']]],
  ['ireservationmanagement',['IReservationManagement',['../interface_logic_1_1_i_reservation_management.html',1,'Logic']]],
  ['isearch',['ISearch',['../interface_logic_1_1_i_search.html',1,'Logic']]],
  ['iswitchable',['ISwitchable',['../interface_ogavir_tours_1_1_i_switchable.html',1,'OgavirTours']]],
  ['iuserandadministratormanagement',['IUserAndAdministratorManagement',['../interface_logic_1_1_i_user_and_administrator_management.html',1,'Logic']]],
  ['iuserentity',['IUserEntity',['../interface_ogavir_tours_1_1_interfaces_1_1_i_user_entity.html',1,'OgavirTours::Interfaces']]]
];
