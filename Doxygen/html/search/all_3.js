var searchData=
[
  ['dataadvertisertoadvertiserconverterclass_5fwhen_5ftoadvertiser_5fcorrectoutput',['DataAdvertiserToAdvertiserConverterClass_When_ToAdvertiser_CorrectOutput',['../class_unit_tests_1_1_converter_test.html#ae2d9e536511bd0152b91a9bb207d6293',1,'UnitTests::ConverterTest']]],
  ['databaseentities',['DatabaseEntities',['../class_data_layer_1_1_database_entities.html',1,'DataLayer']]],
  ['datalayer',['DataLayer',['../namespace_data_layer.html',1,'']]],
  ['datamoderatortomoderatorclass_5fwhen_5ftomoderator_5fcorrectoutput',['DataModeratorToModeratorClass_When_ToModerator_CorrectOutput',['../class_unit_tests_1_1_converter_test.html#a30e57570f92299c9d1400a20dec48f7c',1,'UnitTests::ConverterTest']]],
  ['datausertouserconverterclass_5fwhen_5ftouser_5fcorrectoutput',['DataUserToUserConverterClass_When_ToUser_CorrectOutput',['../class_unit_tests_1_1_converter_test.html#a9d6acdc2fc316b1111a098a6f2b6603c',1,'UnitTests::ConverterTest']]],
  ['date',['Date',['../class_ogavir_tours_1_1_util_1_1_entities_1_1_message.html#a82cb3bb53384792ed81c2fd9bfd4cb56',1,'OgavirTours::Util::Entities::Message']]],
  ['deleteaccomondation',['DeleteAccomondation',['../class_logic_1_1_accommodation_management.html#a5d66fc4399df07e73bd5244378d2e41a',1,'Logic.AccommodationManagement.DeleteAccomondation()'],['../interface_logic_1_1_i_accommodation_management.html#a5bad61c372e7b8aeaec9ba43411a5668',1,'Logic.IAccommodationManagement.DeleteAccomondation()']]],
  ['deleteadvertiser',['DeleteAdvertiser',['../class_logic_1_1_user_and_administrator_management.html#a60ccd5f8658958902ffc7929815a408c',1,'Logic.UserAndAdministratorManagement.DeleteAdvertiser()'],['../interface_logic_1_1_i_user_and_administrator_management.html#a5d1c66c59f26721e1da0835385525cd5',1,'Logic.IUserAndAdministratorManagement.DeleteAdvertiser()']]],
  ['deleteuser',['DeleteUser',['../class_logic_1_1_user_and_administrator_management.html#afdbbaccf5ef3a0b7a6283c558a23489c',1,'Logic.UserAndAdministratorManagement.DeleteUser()'],['../interface_logic_1_1_i_user_and_administrator_management.html#ad154bb782ebec00f000adee8cf737887',1,'Logic.IUserAndAdministratorManagement.DeleteUser()']]]
];
