var searchData=
[
  ['rating',['Rating',['../class_ogavir_tours_1_1_util_1_1_entities_1_1_accommodation.html#a5c9c48e7ec1abdf5e3165390844b1381',1,'OgavirTours.Util.Entities.Accommodation.Rating()'],['../class_ogavir_tours_1_1_util_1_1_entities_1_1_user.html#a9d7fcd36e637502c0ac5bacf7a5d5e60',1,'OgavirTours.Util.Entities.User.Rating()']]],
  ['ratingnumber',['RatingNumber',['../class_ogavir_tours_1_1_util_1_1_entities_1_1_accommodation.html#a7521b51c919e3f3d32b4c49d89f42883',1,'OgavirTours.Util.Entities.Accommodation.RatingNumber()'],['../class_ogavir_tours_1_1_util_1_1_entities_1_1_user.html#a3c18f1627f59f13b48290518f1f3aaeb',1,'OgavirTours.Util.Entities.User.Ratingnumber()']]],
  ['receivedmessages',['ReceivedMessages',['../class_ogavir_tours_1_1_view_models_1_1_message_page_view_model.html#a37807f2c1dcea6fba425fe4a2ac72116',1,'OgavirTours::ViewModels::MessagePageViewModel']]],
  ['reservation',['Reservation',['../class_ogavir_tours_1_1_util_1_1_entities_1_1_accommodation.html#ae3b24e36cd81072209e5910b7d568ffb',1,'OgavirTours::Util::Entities::Accommodation']]],
  ['reservationdate',['ReservationDate',['../class_ogavir_tours_1_1_util_1_1_entities_1_1_reservation.html#a8dc845c60e4596fa18920f089f5fa791',1,'OgavirTours::Util::Entities::Reservation']]],
  ['reservationid',['ReservationId',['../class_ogavir_tours_1_1_util_1_1_entities_1_1_reservation.html#a92273b95c588eaef44576ff8d8d6cbfc',1,'OgavirTours::Util::Entities::Reservation']]],
  ['reservationmanagement',['ReservationManagement',['../class_ogavir_tours_1_1_logics_handler.html#afeb1d7eb894ee31720c7dedfabb359db',1,'OgavirTours::LogicsHandler']]],
  ['reservations',['Reservations',['../class_ogavir_tours_1_1_view_models_1_1_reservation_handler_page_view_model.html#a1095b2bf729a46335513af58c71f9ec8',1,'OgavirTours.ViewModels.ReservationHandlerPageViewModel.Reservations()'],['../class_ogavir_tours_1_1_view_models_1_1_user_profile_view_model.html#ad7e1871aa3e96fc78d897cf532c2774b',1,'OgavirTours.ViewModels.UserProfileViewModel.Reservations()']]],
  ['roomtype',['RoomType',['../class_ogavir_tours_1_1_util_1_1_entities_1_1_reservation.html#a8ba762802bb00767bc337d61a728bfbb',1,'OgavirTours::Util::Entities::Reservation']]],
  ['roomtypes',['RoomTypes',['../class_ogavir_tours_1_1_view_models_1_1_accommodation_profile_view_model.html#ac82a8860c723184acdce0eec9b10fc46',1,'OgavirTours::ViewModels::AccommodationProfileViewModel']]]
];
