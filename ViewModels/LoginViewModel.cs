﻿// <copyright file="LoginViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.ViewModels
{
    /// <summary>
    /// ViewModel of LogIn
    /// </summary>
    public class LoginViewModel : BaseViewModel
    {
        private string username;
        private string password;
        private string errorInfo;
        private bool isModerator;
        private bool isAdvertiser;

        /// <summary>
        /// Gets or sets a value indicating whether the user is moderator
        /// </summary>
        public bool IsModeratror
        {
            get
            {
                return this.isModerator;
            }

            set
            {
                this.isModerator = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user is advertiser
        /// </summary>
        public bool IsAdvertiser
        {
            get
            {
                return this.isAdvertiser;
            }

            set
            {
                this.isAdvertiser = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets errorInfo
        /// </summary>
        public string ErrorInfo
        {
            get
            {
                return this.errorInfo;
            }

            set
            {
                this.errorInfo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets username
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }

            set
            {
                this.username = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets password
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                this.password = value;
                this.OnPropertyChanged();
            }
        }
    }
}
