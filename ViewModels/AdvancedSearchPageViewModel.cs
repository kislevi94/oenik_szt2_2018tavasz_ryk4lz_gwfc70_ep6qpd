﻿// <copyright file="AdvancedSearchPageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.ViewModels
{
    using System.Collections.ObjectModel;
    using OgavirTours.Util.Entities;

    public class AdvancedSearchPageViewModel : BaseViewModel
    {
        private string location;
        private string name;
        private bool wifi;
        private bool petFriendly;
        private bool pool;
        private bool luggage;
        private ObservableCollection<Accommodation> searchResults;
        private Accommodation selectedAccommodation;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchPageViewModel"/> class.
        /// </summary>
        public AdvancedSearchPageViewModel()
        {
            this.searchResults = new ObservableCollection<Accommodation>();
        }

        /// <summary>
        /// Gets or sets selected accommodation
        /// </summary>
        public Accommodation SelectedAccommodation
        {
            get
            {
                return this.selectedAccommodation;
            }

            set
            {
                this.selectedAccommodation = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets search results
        /// </summary>
        public ObservableCollection<Accommodation> SearchResults
        {
            get
            {
                return this.searchResults;
            }

            set
            {
                this.searchResults = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets location
        /// </summary>
        public string Location
        {
            get
            {
                return this.location;
            }

            set
            {
                this.location = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is there wifi in the accommodation
        /// </summary>
        public bool Wifi
        {
            get
            {
                return this.wifi;
            }

            set
            {
                this.wifi = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is the accommodation pet friendly
        /// </summary>
        public bool PetFriendly
        {
            get
            {
                return this.petFriendly;
            }

            set
            {
                this.petFriendly = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is there pool in the accommodation
        /// </summary>
        public bool Pool
        {
            get
            {
                return this.pool;
            }

            set
            {
                this.pool = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is there luggage room in the accommodation
        /// </summary>
        public bool Luggage
        {
            get
            {
                return this.luggage;
            }

            set
            {
                this.luggage = value;
                this.OnPropertyChanged();
            }
        }
    }
}
