﻿namespace OgavirTours.ViewModels
{
    using System.Collections.ObjectModel;
    using OgavirTours.Util.Entities;

    public class UserProfileViewModel : BaseViewModel
    {
        private ObservableCollection<Reservation> reservations;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserProfileViewModel"/> class.
        /// </summary>
        public UserProfileViewModel()
        {
            this.reservations = new ObservableCollection<Reservation>();
        }

        /// <summary>
        /// Gets or sets reservations
        /// </summary>
        public ObservableCollection<Reservation> Reservations
        {
            get
            {
                return this.reservations;
            }

            set
            {
                this.reservations = value;
                this.OnPropertyChanged();
            }
        }
    }
}
