﻿// <copyright file="MessagePageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.ViewModels
{
    using System.Collections.ObjectModel;
    using OgavirTours.Interfaces;
    using OgavirTours.Util.Entities;

    public class MessagePageViewModel : BaseViewModel
    {
        private ObservableCollection<Message> receivedMessages;
        private ObservableCollection<Message> sentMessages;
        private ObservableCollection<Message> currentMessageCollection;
        private Message selectedMessage;
        private string from;
        private User to;
        private ObservableCollection<IUserEntity> existingUsers;
        private string message;
        private bool isMessageUnloaded;
        private string subject;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagePageViewModel"/> class.
        /// </summary>
        public MessagePageViewModel()
        {
            this.existingUsers = new ObservableCollection<IUserEntity>();
        }

        /// <summary>
        /// Gets or sets subject
        /// </summary>
        public string Subject
        {
            get
            {
                return this.subject;
            }

            set
            {
                this.subject = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets existing users
        /// </summary>
        public ObservableCollection<IUserEntity> ExistingUsers
        {
            get
            {
                return this.existingUsers;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the message is unloaded
        /// </summary>
        public bool IsMessageUnloaded
        {
            get
            {
                return this.isMessageUnloaded;
            }

            set
            {
                this.isMessageUnloaded = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets selected message
        /// </summary>
        public Message SelectedMessage
        {
            get
            {
                return this.selectedMessage;
            }

            set
            {
                this.selectedMessage = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets currens messages
        /// </summary>
        public ObservableCollection<Message> CurrentMessageCollection
        {
            get
            {
                return this.currentMessageCollection;
            }

            set
            {
                this.currentMessageCollection = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets sender
        /// </summary>
        public string From
        {
            get
            {
                return this.from;
            }

            set
            {
                this.from = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets receiver
        /// </summary>
        public User To
        {
            get
            {
                return this.to;
            }

            set
            {
                this.to = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets message
        /// </summary>
        public string Message
        {
            get
            {
                return this.message;
            }

            set
            {
                this.message = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets received messages
        /// </summary>
        public ObservableCollection<Message> ReceivedMessages
        {
            get
            {
                return this.receivedMessages;
            }

            set
            {
                this.receivedMessages = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets sent messages
        /// </summary>
        public ObservableCollection<Message> SentMessages
        {
            get
            {
                return this.sentMessages;
            }

            set
            {
                this.sentMessages = value;
                this.OnPropertyChanged();
            }
        }
    }
}
