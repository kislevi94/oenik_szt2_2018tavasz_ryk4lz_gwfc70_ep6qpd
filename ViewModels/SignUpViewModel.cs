﻿// <copyright file="SignUpViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.ViewModels
{
    using System.Collections.ObjectModel;
    using System.Windows.Controls;

    public class SignUpViewModel : BaseViewModel
    {
        private bool isAdvertiser;
        private string username;
        private string email;
        private string phoneNumber;
        private string firstName;
        private string lastName;
        private string password;
        private string passwordAgain;
        private string errorInfo;
        private string postCode;
        private string city;
        private string address;

        /// <summary>
        /// Gets or sets a value indicating whether the user is advertiser
        /// </summary>
        public bool IsAdvertiser
        {
            get
            {
                return this.isAdvertiser;
            }

            set
            {
                this.isAdvertiser = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets username
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }

            set
            {
                this.username = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets email
        /// </summary>
        public string Email
        {
            get
            {
                return this.email;
            }

            set
            {
                this.email = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets phonenumber
        /// </summary>
        public string PhoneNumber
        {
            get
            {
                return this.phoneNumber;
            }

            set
            {
                this.phoneNumber = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets firstname
        /// </summary>
        public string FirstName
        {
            get
            {
                return this.firstName;
            }

            set
            {
                this.firstName = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets lastname
        /// </summary>
        public string LastName
        {
            get
            {
                return this.lastName;
            }

            set
            {
                this.lastName = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets password
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                this.password = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets password again
        /// </summary>
        public string PasswordAgain
        {
            get
            {
                return this.passwordAgain;
            }

            set
            {
                this.passwordAgain = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets error info
        /// </summary>
        public string ErrorInfo
        {
            get
            {
                return this.errorInfo;
            }

            set
            {
                this.errorInfo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets postcode
        /// </summary>
        public string PostCode
        {
            get
            {
                return this.postCode;
            }

            set
            {
                this.postCode = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets city
        /// </summary>
        public string City
        {
            get
            {
                return this.city;
            }

            set
            {
                this.city = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets address
        /// </summary>
        public string Address
        {
            get
            {
                return this.address;
            }

            set
            {
                this.address = value;
                this.OnPropertyChanged();
            }
        }
    }
}
