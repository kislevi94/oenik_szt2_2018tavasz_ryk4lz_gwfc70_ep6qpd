﻿// <copyright file="SearchResultPageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.ViewModels
{
    using System.Collections.ObjectModel;
    using OgavirTours.Util.Entities;

    public class SearchResultPageViewModel : BaseViewModel
    {
        private ObservableCollection<Accommodation> searchResults;
        private Accommodation selectedAccommodation;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchResultPageViewModel"/> class.
        /// </summary>
        public SearchResultPageViewModel()
        {
            this.searchResults = new ObservableCollection<Accommodation>();
        }

        /// <summary>
        /// Gets or sets selected accommodation
        /// </summary>
        public Accommodation SelectedAccommodation
        {
            get
            {
                return this.selectedAccommodation;
            }

            set
            {
                this.selectedAccommodation = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets search results
        /// </summary>
        public ObservableCollection<Accommodation> SearchResults
        {
            get
            {
                return this.searchResults;
            }
        }
    }
}
