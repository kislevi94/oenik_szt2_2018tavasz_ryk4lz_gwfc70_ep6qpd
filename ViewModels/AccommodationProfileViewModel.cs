﻿// <copyright file="AccommodationProfileViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using OgavirTours.Util.Entities;

    /// <summary>
    /// ViewModel of AccommodationProfile
    /// </summary>
    public class AccommodationProfileViewModel : BaseViewModel
    {
        private Accommodation currentAccommodation;
        private bool canRate;
        private Visibility visibilityPool;
        private Visibility visibilityPetFriendly;
        private Visibility visibilityLuggageRoom;
        private Visibility visibilityWiFi;
        private string errorInfo;
        private string selectedRoomType;
        private int selectedNumberOfNights;
        private DateTime selectedDate;
        private Comment newComment;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccommodationProfileViewModel"/> class.
        /// </summary>
        public AccommodationProfileViewModel()
        {
            this.newComment = new Comment();
        }

        /// <summary>
        /// Gets types of rooms
        /// </summary>
        public List<string> RoomTypes
        {
            get
            {
                return new List<string>() { "one bed", "double bed", "three bed (no double bed)", "four bed (two double beds)" };
            }
        }

        /// <summary>
        /// Gets possible number of nights
        /// </summary>
        public List<int> NumberOfNights
        {
            get
            {
                return new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            }
        }

        /// <summary>
        /// Gets or sets new comment
        /// </summary>
        public Comment NewComment
        {
            get
            {
                return this.newComment;
            }

            set
            {
                this.newComment = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets selected date
        /// </summary>
        public DateTime SelectedDate
        {
            get
            {
                return this.selectedDate;
            }

            set
            {
                this.selectedDate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets selected room type
        /// </summary>
        public string SelectedRoomType
        {
            get
            {
                return this.selectedRoomType;
            }

            set
            {
                this.selectedRoomType = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// Gets or sets nuber of nights selected
        /// </summary>
        public int SelectedNumberOfNights
        {
            get
            {
                return this.selectedNumberOfNights;
            }

            set
            {
                this.selectedNumberOfNights = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets current accommodation
        /// </summary>
        public Accommodation CurrentAccommodation
        {
            get
            {
                return this.currentAccommodation;
            }

            set
            {
                this.currentAccommodation = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Errorinfo
        /// </summary>
        public string ErrorInfo
        {
            get
            {
                return this.errorInfo;
            }

            set
            {
                this.errorInfo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Pool icons' visibility
        /// </summary>
        public Visibility VisibilityPool
        {
            get
            {
                return this.visibilityPool;
            }

            set
            {
                this.visibilityPool = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Paw icons' visibility
        /// </summary>
        public Visibility VisibilityPetFriendly
        {
            get
            {
                return this.visibilityPetFriendly;
            }

            set
            {
                this.visibilityPetFriendly = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Luggage icons' visibility
        /// </summary>
        public Visibility VisibilityLuggageRoom
        {
            get
            {
                return this.visibilityLuggageRoom;
            }

            set
            {
                this.visibilityLuggageRoom = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Wifi icons' visibility
        /// </summary>
        public Visibility VisibilityWiFi
        {
            get
            {
                return this.visibilityWiFi;
            }

            set
            {
                this.visibilityWiFi = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether user has permission to rate
        /// </summary>
        public bool CanRate
        {
            get
            {
                return this.canRate;
            }

            set
            {
                this.canRate = value;
            }
        }
    }
}
