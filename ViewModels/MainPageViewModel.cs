﻿// <copyright file="MainPageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.ViewModels
{
    using System.Collections.ObjectModel;
    using System.Windows;
    using OgavirTours.Util.Entities;

    /// <summary>
    /// ViewModel of MainPage
    /// </summary>
    public class MainPageViewModel : BaseViewModel
    {
        private ObservableCollection<Accommodation> bestRated;
        private Accommodation selectedAccommodation;
        private string errorMessage;
        private Visibility isLoggedInVisibility;
        private string searchingText;

        /// <summary>
        /// Gets or sets selected accommodation
        /// </summary>
        public Accommodation SelectedAccommodation
        {
            get
            {
                return this.selectedAccommodation;
            }

            set
            {
                this.selectedAccommodation = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets bestRated
        /// </summary>
        public ObservableCollection<Accommodation> BestRated
        {
            get
            {
                return this.bestRated;
            }

            set
            {
                this.bestRated = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets searchingtext
        /// </summary>
        public string SearchingText
        {
            get
            {
                return this.searchingText;
            }

            set
            {
                this.searchingText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }

            set
            {
                this.errorMessage = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets visibility of login and signup buttons
        /// </summary>
        public Visibility IsLoggedInVisibility
        {
            get
            {
                return this.isLoggedInVisibility;
            }

            set
            {
                this.isLoggedInVisibility = value;
                this.OnPropertyChanged();
            }
        }
    }
}
