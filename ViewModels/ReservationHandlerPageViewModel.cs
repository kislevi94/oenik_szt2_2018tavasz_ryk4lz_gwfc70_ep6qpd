﻿// <copyright file="ReservationHandlerPageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.ViewModels
{
    using System.Collections.ObjectModel;
    using System.Windows.Media.Imaging;
    using OgavirTours.Util.Entities;

    public class ReservationHandlerPageViewModel : BaseViewModel
    {
        private Accommodation newAccommodation;
        private Accommodation selectedAccommodation;
        private string errorInfo;
        private ObservableCollection<Accommodation> accommodations;
        private ObservableCollection<Reservation> reservations;
        private ObservableCollection<BitmapImage> listExistingImages;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReservationHandlerPageViewModel"/> class.
        /// </summary>
        public ReservationHandlerPageViewModel()
        {
            this.accommodations = new ObservableCollection<Accommodation>();
        }

        /// <summary>
        /// Gets or sets existing images
        /// </summary>
        public ObservableCollection<BitmapImage> ListExistingImages
        {
            get
            {
                return this.listExistingImages;
            }

            set
            {
                this.listExistingImages = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets reservations
        /// </summary>
        public ObservableCollection<Reservation> Reservations
        {
            get
            {
                return this.reservations;
            }

            set
            {
                this.reservations = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets error info
        /// </summary>
        public string ErrorInfo
        {
            get
            {
                return this.errorInfo;
            }

            set
            {
                this.errorInfo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets accommodations
        /// </summary>
        public ObservableCollection<Accommodation> Accommodations
        {
            get
            {
                return this.accommodations;
            }

            set
            {
                this.accommodations = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets selected accommodation
        /// </summary>
        public Accommodation SelectedAccommodation
        {
            get
            {
                return this.selectedAccommodation;
            }

            set
            {
                this.selectedAccommodation = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets new accommodation
        /// </summary>
        public Accommodation NewAccommodation
        {
            get
            {
                return this.newAccommodation;
            }

            set
            {
                this.newAccommodation = value;
                this.OnPropertyChanged();
            }
        }
    }
}
