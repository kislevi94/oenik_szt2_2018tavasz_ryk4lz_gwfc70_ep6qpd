﻿// <copyright file="AdminHandlerPageViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.ViewModels
{
    using System.Collections.ObjectModel;
    using OgavirTours.Util.Entities;

    public class AdminHandlerPageViewModel : BaseViewModel
    {
        private ObservableCollection<User> users;
        private ObservableCollection<Advertiser> advertisers;
        private User selectedUser;
        private Advertiser selectedAdvertiser;

        /// <summary>
        /// Gets or sets selected user
        /// </summary>
        public User SelectedUser
        {
            get
            {
                return this.selectedUser;
            }

            set
            {
                this.selectedUser = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets selected advertiser
        /// </summary>
        public Advertiser SelectedAdvertiser
        {
            get
            {
                return this.selectedAdvertiser;
            }

            set
            {
                this.selectedAdvertiser = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets users
        /// </summary>
        public ObservableCollection<User> Users
        {
            get
            {
                return this.users;
            }

            set
            {
                this.users = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets advertisers
        /// </summary>
        public ObservableCollection<Advertiser> Advertisers
        {
            get
            {
                return this.advertisers;
            }

            set
            {
                this.advertisers = value;
                this.OnPropertyChanged();
            }
        }
    }
}
