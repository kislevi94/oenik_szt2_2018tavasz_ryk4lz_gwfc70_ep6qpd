﻿//-----------------------------------------------------------------------
// <copyright file="Queries.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Logic;
using DataLayer;

namespace UnitTests.LogicTests
{
    /// <summary>
    /// Logic Project AccommodationManagement class test.
    /// </summary>
    [TestFixture]
    class AccommodationManagementTest
    {
        /// <summary>
        /// ListBestRatedAccommodation method test.
        /// </summary>
        [Test]
        public void AccommodationManagementClass_When_ListBestRatedAccommodation_CorrectListOutput()
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            Queries q = new Queries();
            IData testIData = new Queries();
            AccommodationManagement accommodationManagementClass = new AccommodationManagement(testIData);
            //ACT
            List<Accommodation> result = accommodationManagementClass.ListBestRatedAccommodation(2);
            double temp = (double)ot.Accommodation.Max(x => x.Rating);
            Accommodation a = ot.Accommodation.Single(x => x.Rating == temp);
            //ASSERT
            Assert.That(result.First().Accommodationid, Is.EqualTo(a.Accommodationid));
        }
    }
}
