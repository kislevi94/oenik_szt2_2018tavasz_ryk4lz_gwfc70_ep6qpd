﻿//-----------------------------------------------------------------------
// <copyright file="Queries.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Logic;
using DataLayer;

namespace UnitTests.LogicTests
{
    /// <summary>
    /// Logic Project Search class test.
    /// </summary>
    [TestFixture]
    class SearchTest
    {
        /// <summary>
        /// ShortSearch method test.
        /// </summary>
        [Test]
        public void SearchClass_When_ShortSearch_CorrectListCount()
        {
            //ARRANGE
            IData data = new Queries();
            Search searchClass = new Search(data);
            //ACT
            List<Accommodation> list = searchClass.ShortSearch("Gyor");
            //ASSERT
            Assert.That(list.Count(), Is.EqualTo(2));
        }
        /// <summary>
        /// LongSearch method test.
        /// </summary>
        /// <param name="location">Location</param>
        /// <param name="name">Name</param>
        /// <param name="wifi">Available wifi or not</param>
        /// <param name="animal">Allowed animal or not</param>
        /// <param name="pool">Available pool or not</param>
        /// <param name="panorama">Available panorama or not</param>
        /// <param name="expectedCount">How many result has to be</param>
        [TestCase("Gyor", "Hotel Atlantic",true,false,true,false,2)]
        [TestCase("Gyor", null, true, false, true, false, 2)]
        [TestCase(null, "Hotel Atlantic", true, false, true, false, 1)]
        public void SearchClass_When_LongSearch_CorrectListCount(string location, string name, bool wifi, bool animal, bool pool, bool panorama, int expectedCount)
        {
            //ARRANGE
            IData data = new Queries();
            Search searchClass = new Search(data);
            //ACT
            List<Accommodation> list = searchClass.LongSearch(location, name, wifi, animal, pool, panorama);
            //ASSERT
            Assert.That(list.Count(), Is.EqualTo(expectedCount));
        }
    }
}
