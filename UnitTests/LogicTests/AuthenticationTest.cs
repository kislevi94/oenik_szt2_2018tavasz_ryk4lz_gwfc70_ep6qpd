﻿//-----------------------------------------------------------------------
// <copyright file="Queries.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Logic;
using DataLayer;

namespace UnitTests.LogicTests
{
    /// <summary>
    /// Logic Project AccommodationManagement class tests.
    /// </summary>
    [TestFixture]
    class AuthenticationTest
    {
        /// <summary>
        /// Registration method test.
        /// </summary>
        /// <param name="isAdv">Advertiser or not</param>
        /// <param name="username">Username</param>
        /// <param name="pw">Password</param>
        /// <param name="name">User name</param>
        /// <param name="mail">Email</param>
        /// <param name="phone">Phone</param>
        /// <param name="addresscode">Address code</param>
        /// <param name="city">Address citiy</param>
        /// <param name="address">Address</param>
        [TestCase(true,"TestAdver","TestPW","VegBela","random.test@aol.com","06901111111","1234","Debrecen","Test utca 3")]
        [TestCase(false, "TestUser", "TestPW", "TrabAntal", "random.test@gmail.com", "06902222222", "4321", "Siofok", "Test ut 13")]
        public void AuthenticationClass_When_Registration_InputUserOrAdvertiser_Then_IsPutNewMemberInDB(bool isAdv, string username, string pw, 
            string name, string mail, string phone, string addresscode, string city, string address)
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            IData testIData = new Queries();
            Authentication authenticationClass = new Authentication(testIData);
            
            int DbCountBeforeInput = default(int);
            if (isAdv)
            {
                DbCountBeforeInput = ot.Advertiser.Count();
            }
            else
            {
                DbCountBeforeInput = ot.Users.Count();
            }
            //ACT
            authenticationClass.Registration(isAdv, username, pw, name, mail, phone, addresscode, city, address);
            int DbCountAfterInput = default(int);
            if (isAdv)
            {
                DbCountAfterInput = ot.Advertiser.Count();
            }
            else
            {
                DbCountAfterInput = ot.Users.Count();
            }
            //ASSERT
            Assert.That(DbCountBeforeInput, Is.EqualTo(DbCountAfterInput - 1));
        }
        /// <summary>
        /// InputAllreadyExistUserOrAdvertiser method test.
        /// </summary>
        /// <param name="isAdv">Advertiser or not</param>
        /// <param name="username">Username</param>
        /// <param name="pw">Password</param>
        /// <param name="name">User name</param>
        /// <param name="mail">Email</param>
        /// <param name="phone">Phone</param>
        /// <param name="addresscode">Address code</param>
        /// <param name="city">Address citiy</param>
        /// <param name="address">Address</param>
        [TestCase(true, "RVallalat", "TestPW", "VegBela", "random.test@aol.com", "06901111111", "1234", "Debrecen", "Test utca 3")]
        [TestCase(false, "RZoltan", "TestPW", "TrabAntal", "random.test@gmail.com", "06902222222", "4321", "Siofok", "Test ut 13")]
        public void AuthenticationClass_When_Registration_InputAllreadyExistUserOrAdvertiser_Then_Thrown(bool isAdv, string username, string pw,
            string name, string mail, string phone, string addresscode, string city, string address)
        {
            //ARRANGE - ACT
            OtEntities ot = new OtEntities();
            IData testIData = new Queries();
            Authentication authenticationClass = new Authentication(testIData);
            //ASSERT
            Assert.That(()=> authenticationClass.Registration(isAdv, username, pw, name, mail, phone, addresscode, city, address), Throws.Exception);
        }
        /// <summary>
        /// Private UserAuthenticationTestSource generator for AuthenticationClass_When_UserAuthentication_ValidUserOutput test method.
        /// </summary>
        private static List<Users> UserAuthenticationTestSource
        {
            get
            {
                List<Users> testCases = new List<Users>
                {
                    new Users()
                    {
                        Userid = -1,
                        Username = "Admin",
                        Password = "admin",
                        Name = null,
                        Email = null,
                        Phonenumber = null,
                        Address = null,
                        IsModerator = false,
                        Rating = 0,
                        Ratingnumber = 0,
                    },
                    new Users()
                    {
                        Userid = 1,
                        Username = "RBela",
                        Password = "rbela",
                        Name = "Rbela",
                        Email = null,
                        Phonenumber = null,
                        Address = null,
                        IsModerator = false,
                        Rating = 0,
                        Ratingnumber = 0,
                    }
                };
                return testCases;
            }
        }
        /// <summary>
        /// UserAuthentication method test.
        /// </summary>
        /// <param name="user"></param>
        [TestCaseSource("UserAuthenticationTestSource")]
        public void AuthenticationClass_When_UserAuthentication_ValidUserOutput(Users user)
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            IData testIData = new Queries();
            Authentication authenticationClass = new Authentication(testIData);
            //ACT
            Users testUser = authenticationClass.UserAuthentication(user.Username, user.Password);
            //ASSERT
            // TODO: Amíg nincs meg az eredeti pw, addig nem tudom ellenőrizni, hogy jó-e (HASH gondok):p
            Assert.That(testUser.Userid, Is.EqualTo(user.Userid));
        }
        /// <summary>
        /// UserAuthentication method test.
        /// </summary>
        [Test]
        public void AuthenticationClass_When_UserAuthentication_DidNotMatchPW_Thrown()
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            IData testIData = new Queries();
            Authentication authenticationClass = new Authentication(testIData);
            //ACT
            string username = "DeletedUser";
            string pw = "DefiniatelyNotMatchPassword";
            //ASSERT
            Assert.That(() => authenticationClass.UserAuthentication(username, pw), Throws.Exception);
        }

        /// <summary>
        /// Private AdvertiserAuthenticationTestSource generator for AuthenticationClass_When_AdvertiserAuthentication_ValidAdvertiserOutput test method.
        /// </summary>
        private static List<Advertiser> AdvertiserAuthenticationTestSource
        {
            get
            {
                List<Advertiser> testCases = new List<Advertiser>
                {
                    /*new Advertiser()
                    {
                         Advertiserid = 0,
                         Password = "tadvertiser",
                         Username = "DeletedAdvertiser"
                    },*/
                    new Advertiser()
                    {
                         Advertiserid = 2,
                         Password = "rcompany",
                         Username = "RCompany"
                    }
                };
                return testCases;
            }
        }
        /// <summary>
        /// AdvertiserAuthentication method test.
        /// </summary>
        /// <param name="user"></param>
        [TestCaseSource("AdvertiserAuthenticationTestSource")]
        public void AuthenticationClass_When_AdvertiserAuthentication_ValidAdvertiserOutput(Advertiser user)
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            IData testIData = new Queries();
            Authentication authenticationClass = new Authentication(testIData);
            //ACT
            Advertiser testUser = authenticationClass.AdvertiserAuthentication(user.Username, user.Password);
            //ASSERT
            Assert.That(testUser.Advertiserid, Is.EqualTo(user.Advertiserid));
        }
        /// <summary>
        /// AdvertiserAuthentication method test.
        /// </summary>
        [Test]
        public void AuthenticationClass_When_AdvertiserAuthentication_DidNotMatchPW_Thrown()
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            IData testIData = new Queries();
            Authentication authenticationClass = new Authentication(testIData);
            //ACT
            string username = "RCompany";
            string pw = "DefiniatelyNotMatchPassword";
            //ASSERT
            Assert.That(() => authenticationClass.AdvertiserAuthentication(username, pw), Throws.Exception);
        }
    }
}
