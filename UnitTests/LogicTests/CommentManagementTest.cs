﻿//-----------------------------------------------------------------------
// <copyright file="Queries.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Logic;
using DataLayer;

namespace UnitTests.LogicTests
{
    /// <summary>
    /// Logic Project CommentManagement class test.
    /// </summary>
    [TestFixture]
    class CommentManagementTest
    {
        /// <summary>
        /// GetComments method test.
        /// </summary>
        /// <param name="accid">Tested account id</param>
        [TestCase(1)]
        [TestCase(2)]
        public void CommentManagementClass_When_GetComments_CorrectCount(int accid)
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            IData data = new Queries();
            CommentManagement commentManagementClass = new CommentManagement(data);
            //ACT
            var neededComments = ot.Comment.Where(x => x.Accommodationid == accid).ToList();
            List<Comment> listComment = commentManagementClass.GetComments(accid);
            //ASSERT
            Assert.That(listComment.Count(), Is.EqualTo(neededComments.Count()));
        }
    }
}
