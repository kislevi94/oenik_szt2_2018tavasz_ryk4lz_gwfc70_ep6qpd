﻿//-----------------------------------------------------------------------
// <copyright file="Queries.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace UnitTests.DataLayerTests
{
    using NUnit.Framework;
    using DataLayer;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    /// <summary>
    /// DataLayer ProjectQueries class tests.
    /// </summary>
    [TestFixture]
    public class QueriesTest
    {
        /// <summary>
        /// InsertIntoUser method test.
        /// Check, if a new user is added to the DB, the user count is rise
        /// </summary>
        /// <param name="username">New user username</param>
        /// <param name="password">New user pw</param>
        /// <param name="name">New user simple name</param>
        /// <param name="email">New user email</param>
        /// <param name="phone">New user phone</param>
        /// <param name="address">New user address</param>
        [TestCase("User_1", "PW_U_1", "User_Name_1", "Mail_U_1", "06111111111", "address_U_1")]
        public void QueriesClass_When_InsertIntoUser_AddNewUser(string username, string password, string name, string email, string phone, string address)
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int UsercountBeforeAddNew = ot.Users.Count();
            queries.InsertIntoUser(username, password, name, email, phone, address);
            //ASSERT
            Assert.That(ot.Users.Count(), Is.EqualTo(UsercountBeforeAddNew + 1));
        }
        /// <summary>
        /// InsertIntoAdvertiser method test.
        /// Check, if a new user is added to the DB, the user count is rise
        /// </summary>
        /// <param name="username">New advertiser username</param>
        /// <param name="password">New advertiser pw</param>
        /// <param name="name">New advertiser simple name</param>
        /// <param name="email">New advertiser email</param>
        /// <param name="phone">New advertiser phone</param>
        /// <param name="address">New advertiser address</param>
        [TestCase("Advertiser_1", "PW_A_1", "Advertiser_Name_1", "Mail_Advertiser_1", "06111111111", "address_Advertiser_1")]
        public void QueriesClass_When_InsertIntoAdvertiser_AdvertiserCountPlus(string username, string password, string name, string email, string phone, string address)
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int AdvertiserBeforaAddNew = ot.Advertiser.Count();
            queries.InsertIntoAdvertiser(username, password, name, email, phone, address);
            //ASSERT
            Assert.That(ot.Advertiser.Count(), Is.EqualTo(AdvertiserBeforaAddNew + 1));
        }
        /// <summary>
        /// InsertIntoReservation method test.
        /// </summary>
        /// <param name="userid">user id</param>
        /// <param name="accomondationid">accomodation id</param>
        /// <param name="roomtype">roomtype</param>
        /// <param name="price">price</param>
        /// <param name="length">length (days)</param>
        /// <param name="status">statues</param>
        [TestCase(1,1,2, 4000, 2, Status.Unprocessed)]
        public void QueriesClass_When_InsertIntoReservation_ReservationCountPlus(int userid, int accomondationid, int roomtype, int price, int length, Status status)
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int ReservationBeforaAddNew = ot.Reservation.Count();
            //var price =ot.Accommodation.Where(x => x.Accommodationid == accomondationid).Select(x => x.TwoPersonRoomPrice).ToString();
            queries.InsertIntoReservation(userid, accomondationid, roomtype, price, new DateTime(2019,9,1,12,30,12), length, status);
            //ASSERT
            Assert.That(ot.Reservation.Count(), Is.EqualTo(ReservationBeforaAddNew + 1));
        }
        /// <summary>
        /// InsertIntoMessage method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_InsertIntoMessage_MessageCountPlus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int MessageBeforaAddNew = ot.Message.Count();
            queries.InsertIntoMessage("RBela", "MBela", 2, DateTime.Now, "TestSubject", "TestText");
            //ASSERT
            Assert.That(ot.Message.Count(), Is.EqualTo(MessageBeforaAddNew + 1));
        }
        /// <summary>
        /// ReturnIncamingMessagesById method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_ReturnIncamingMessagesById_ReturnListCountCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            string testID = "u2";
            //ACT
            var db = ot.Message.Where(x => x.Mto == testID).Count();
            List<Message> list = queries.ReturnIncamingMessagesById(testID);
            //ASSERT
            Assert.That(list.Count, Is.EqualTo(db));
        }
        /// <summary>
        /// ReturnOutGoingMessagesById method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_ReturnOutGoingMessagesById_ReturnListCountCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            string testID = "u4";
            //ACT
            var db = ot.Message.Where(x => x.Mfrom == testID).Count();
            List<Message> list = queries.ReturnOutGoingMessagesById(testID);
            //ASSERT
            Assert.That(list.Count, Is.EqualTo(db));
        }
        /// <summary>
        /// ModeratedAccommodation method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_ModeratedAccommodation_ReturnListCountCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int testID = 4;
            //ACT
            var db = ot.Moderator.Where(x => x.Moderatorid == testID).Count();
            List<Accommodation> list = queries.ModeratedAccommodation(testID);
            //ASSERT
            Assert.That(list.Count, Is.EqualTo(db));
        }
        /// <summary>
        /// OwnedAccomondation method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_OwnedAccomondation_ReturnListCountCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int testID = 4;
            //ACT
            var db = ot.Accommodation.Where(x => x.Advertiserid == testID).Count();
            List<Accommodation> list = queries.OwnedAccomondation(testID);
            //ASSERT
            Assert.That(list.Count, Is.EqualTo(db));
        }
        /// <summary>
        /// ReturnUserById method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_ReturnUserById_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int testID = 2;
            //ACT
            var user = ot.Users.Where(x => x.Userid == testID).Select(x=>x.Userid);
            Users userM = queries.ReturnUserById(testID);
            int ID = default(int);
            foreach (var akt in user)
            {
                ID = akt;
            }
            //ASSERT
            Assert.That(userM.Userid, Is.EqualTo(ID));
        }
        /// <summary>
        /// ReturnAccommodationById method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_ReturnAccommodationById_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int testID = 3;
            //ACT
            var user = ot.Accommodation.Where(x => x.Accommodationid == testID).Select(x=>x.Accommodationid);
            Accommodation userM = queries.ReturnAccommodationById(testID);
            int ID = default(int);
            foreach (var akt in user)
            {
                ID = akt;
            }
            //ASSERT
            Assert.That(userM.Accommodationid, Is.EqualTo(ID));
        }
        /// <summary>
        /// UpdateUserRating method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_UpdateUserRating_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int uID = 4;
            int aID = 1;
            int rating = 2;
            double beforRatingDouble = default(int);
            double beforRatingNumberDouble = default(int);
            double newRatingDouble = default(int);
            double afterRatingDouble = default(int);
            //ACT
            var beforRating = ot.Users.Where(x => x.Userid == uID).Select(x => x.Rating);
            var beforRatingNumber = ot.Users.Where(x => x.Userid == uID).Select(x => x.Ratingnumber);
            foreach (var akt in beforRatingNumber)
            {
                beforRatingNumberDouble = akt.Value;
            }
            foreach (var akt in beforRating)
            {
                beforRatingDouble = akt.Value;
            }
            newRatingDouble = ((beforRatingDouble * beforRatingNumberDouble) + rating) / (beforRatingNumberDouble + 1);

            queries.UpdateUserRating(uID, aID, rating);
            var afterRating = ot.Users.Where(x => x.Userid == uID).Select(x => x.Rating);
            foreach (var akt in beforRating)
            {
                afterRatingDouble = akt.Value;
            }
            //ASSERT
            Assert.That(afterRatingDouble,Is.EqualTo(newRatingDouble));
        }
        /// <summary>
        /// UpdateAccRating method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_UpdateAccRating_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int uID = 4;
            int aID = 1;
            int rating = 2;
            double beforRatingDouble = default(int);
            double beforRatingNumberDouble = default(int);
            double newRatingDouble = default(int);
            double afterRatingDouble = default(int);
            //ACT
            var beforRating = ot.Accommodation.Where(x => x.Accommodationid == aID).Select(x => x.Rating);
            var beforRatingNumber = ot.Accommodation.Where(x => x.Accommodationid == aID).Select(x => x.Ratingnumber);
            foreach (var akt in beforRatingNumber)
            {
                beforRatingNumberDouble = akt.Value;
            }
            foreach (var akt in beforRating)
            {
                beforRatingDouble = akt.Value;
            }
            newRatingDouble = ((beforRatingDouble * beforRatingNumberDouble) + rating) / (beforRatingNumberDouble + 1);

            queries.UpdateAccRating(uID, aID, rating);
            var afterRating = ot.Accommodation.Where(x => x.Accommodationid == uID).Select(x => x.Rating);
            foreach (var akt in beforRating)
            {
                afterRatingDouble = akt.Value;
            }
            //ASSERT
            Assert.That(afterRatingDouble, Is.EqualTo(newRatingDouble));
        }
        /// <summary>
        /// InsertIntoComment method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_InsertIntoComment_CountPlus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int CommentBeforaAddNew = ot.Comment.Count();
            queries.InsertIntoComment(1,1,"asd");
            //ASSERT
            Assert.That(ot.Comment.Count(), Is.EqualTo(CommentBeforaAddNew + 1));
        }
        /*
        public void QueriesClass_When_InsertIntoModerator_CountPlus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int ModeratorBeforaAddNew = ot.Moderator.Count();
            queries.InsertIntoModerator(4, 1);
            //ASSERT
            Assert.That(ot.Moderator.Count(), Is.EqualTo(ModeratorBeforaAddNew + 1));
        }*/
        /*
        [Test]
        public void QueriesClass_When_UpdateReservationStatus_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int uID = 1;

            Status statusAfter = default(Status);
            //ACT
            queries.UpdateReservationStatus(1, Status.Deleted);
            var statusAfterLINQ = ot.Reservation.Where(x => x.Reservationid == uID).Select(x => x.Status);
            foreach (var akt in statusAfterLINQ)
            {
                statusAfter = (Status)Enum.Parse(typeof(Status), akt);
            }
            //ASSERT
            Assert.That(statusAfter, Is.EqualTo(Status.Deleted));
        }*/
        /// <summary>
        /// ReturnReservationsByUseridAndAccid method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_ReturnReservationsByUseridAndAccid_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int uID = 1;
            int aID = 1;
            //ACT
            var reservations = ot.Reservation.Where(x => x.Userid == uID && x.Accommodationid==aID).ToList();
            List<Reservation> list = queries.ReturnReservationsByUseridAndAccid(uID, aID);
            //ASSERT
            Assert.That(list.Count, Is.EqualTo(reservations.Count));
        }
        /*[Test]
        public void QueriesClass_When_ReturnReservationByUserid_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int uID = 1;
            //ACT
            var reservations = ot.Reservation.Where(x => x.Userid == uID).ToList();
            List<Reservation> list = queries.ReturnReservationByUserid(uID);
            //ASSERT
            Assert.That(list.Count, Is.EqualTo(reservations.Count));
        }
        [Test]
        public void QueriesClass_When_ReturnReservationByUseridAndStatus_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int uID = 1;
            Status status = Status.Completed;
            //ACT
            var reservations = ot.Reservation.Where(x => x.Userid == uID && x.Status==status.ToString()).ToList();
            List<Reservation> list = queries.ReturnReservationByUseridAndStatus(uID, status);
            //ASSERT
            Assert.That(list.Count, Is.EqualTo(reservations.Count));
        }
        [Test]
        public void QueriesClass_When_ReturnReservationByAccid_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int aID = 1;
            //ACT
            var reservations = ot.Reservation.Where(x => x.Accommodationid == aID).ToList();
            List<Reservation> list = queries.ReturnReservationByAccid(aID);
            //ASSERT
            Assert.That(list.Count, Is.EqualTo(reservations.Count));
        }
        [Test]
        public void QueriesClass_When_ReturnReservationByAccidAndStatus_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            int aID = 1;
            Status status = Status.Completed;
            //ACT
            var reservations = ot.Reservation.Where(x => x.Accommodationid == aID && x.Status == status.ToString()).ToList();
            List<Reservation> list = queries.ReturnReservationByAccidAndStatus(aID, status);
            //ASSERT
            Assert.That(list.Count, Is.EqualTo(reservations.Count));
        }*/
        /// <summary>
        /// InsertIntoAccomodation method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_InsertIntoAccomodation_CountPlus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int AccBeforaAddNew = ot.Accommodation.Count();
            queries.InsertIntoAccomodation("teszt", "tesz teszt teszt", 1, "teszt", 10, 10, 10, 10, true, true, true, false);
            //ASSERT
            Assert.That(ot.Accommodation.Count(), Is.EqualTo(AccBeforaAddNew + 1));
        }
        /// <summary>
        /// UpdateAccommodation method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_UpdateAccommodation_ReturnCorrect()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            string NewName = "asd";
            var acc = queries.ReturnAccommodationById(2);
            queries.UpdateAccommodation(acc.Accommodationid, NewName, acc.Address,acc.Picture,(int)(acc.OnePersonRoomPrice),
                (int)(acc.TwoPersonRoomPrice), (int)(acc.ThreePersonRoomPrice), (int)(acc.FourPersonRoomPrice),(bool)(acc.Wifi),
                (bool)(acc.Animal), (bool)(acc.Panorama), (bool)(acc.Pool));
            var acc2 = queries.ReturnAccommodationById(2);
            //ASSERT
            Assert.That(acc2.Name, Is.EqualTo(NewName));
        }
        /*[Test]
        public void QueriesClass_When_RemoveAccommodation_CountMinus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int accBeforaAddNew = ot.Accommodation.Count();
            queries.RemoveAccommodation(2);
            //ASSERT
            Assert.That(ot.Accommodation.Count(), Is.EqualTo(accBeforaAddNew - 1));
        }*/
        /// <summary>
        /// RemoveAccIdFromReservation method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_RemoveAccIdFromReservation_CountMinus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int accID = 1;
            queries.RemoveAccIdFromReservation(1);
            int accAfter = (from res in ot.Reservation
                            where res.Accommodationid == accID
                            select res).Count();
            //ASSERT
            Assert.That(accAfter, Is.EqualTo(0));
        }
        /*
        [Test]
        public void QueriesClass_When_RemoveUserIdFromReservation_CountMinus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int uID = 1;
            queries.RemoveUserIdFromReservation(uID);
            int uAfter = (from res in ot.Reservation
                          where res.Userid == uID
                          select res).Count();
            //ASSERT
            Assert.That(uAfter, Is.EqualTo(0));
        }*/
        /// <summary>
        /// RemoveIdFromMessage method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_RemoveIdFromMessage_CountMinus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            string uID = "u1";
            queries.InsertIntoMessage("RBela", "MBela", 2, DateTime.Now, "TestSubject", "TestText");
            queries.RemoveIdFromMessage(uID);
            int uAfter = (from msg in ot.Message
                          where msg.Mto == uID || msg.Mfrom == uID
                          select msg).Count();
            //ASSERT
            Assert.That(uAfter, Is.EqualTo(0));
        }
        /*
        public void QueriesClass_When_ReturnMessagebyId_CountMinus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            Message m = queries.ReturnMessagebyId(1);
            //ASSERT
            Assert.That(m.Messageid, Is.EqualTo(1));
        }*/
        /// <summary>
        /// RemoveMessage method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_RemoveMessage_CountMinus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int mID = 1;
            queries.RemoveMessage(1);
            int uAfter = (from msg in ot.Message
                          where msg.Messageid == mID
                          select msg).Count();
            //ASSERT
            Assert.That(uAfter, Is.EqualTo(0));
        }
        /// <summary>
        /// RemoveUserIdFromComment method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_RemoveUserIdFromComment_CountMinus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int uID = 1;
            queries.RemoveUserIdFromComment(uID);
            int uAfter = (from cmt in ot.Comment
                          where cmt.Userid == uID
                          select cmt).Count();
            //ASSERT
            Assert.That(uAfter, Is.EqualTo(0));
        }
        /// <summary>
        /// RemoveCommentByAccId method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_RemoveCommentByAccId_CountMinus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int aID = 1;
            queries.RemoveCommentByAccId(aID);
            int uAfter = (from cmt in ot.Comment
                          where cmt.Accommodationid == aID
                          select cmt).Count();
            //ASSERT
            Assert.That(uAfter, Is.EqualTo(0));
        }
        /// <summary>
        /// RemoveComment method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_RemoveComment_CountMinus()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int cID = 1;
            queries.RemoveComment(cID);
            int uAfter = (from cmt in ot.Comment
                          where cmt.Commentid == cID
                          select cmt).Count();
            //ASSERT
            Assert.That(uAfter, Is.EqualTo(0));
        }
        /// <summary>
        /// ReturnCommentbyID method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_ReturnCommentbyID_CountOutput()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int cID = 2;
            queries.InsertIntoComment(2, 2, "asd");
            queries.InsertIntoComment(2, 2, "asd");
            Comment comment = queries.ReturnCommentbyID(cID);
            //ASSERT
            Assert.That(comment.Commentid, Is.EqualTo(cID));
        }
        /*
        [Test]
        public void QueriesClass_When_RemoveAllModeratorbyUserId_CountOutput()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int uID = 1;
            queries.RemoveAllModeratorbyUserId(uID);
            int db = ot.Moderator.Select(x => x.Userid == uID).Count();
            //ASSERT
            Assert.That(db, Is.EqualTo(0));
        }*/
        /*
        [Test]
        public void QueriesClass_When_RemoveAllModeratorbyAccId_CountOutput()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int aID = 2;
            queries.RemoveAllModeratorbyAccId(aID);
            int db = ot.Moderator.Select(x => x.Accommodationid == aID).Count();
            //ASSERT
            Assert.That(db, Is.EqualTo(0));
        }*/
        /// <summary>
        /// RemoveModeratorByUserAndAccId method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_RemoveModeratorByUserAndAccId_CountOutput()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int aID = 1;
            int uID = 1;
            queries.RemoveModeratorByUserAndAccId(uID,aID);
            int db = (from mod in ot.Moderator
                      where mod.Userid == uID && mod.Accommodationid == aID
                      select mod).Count();
            //ASSERT
            Assert.That(db, Is.EqualTo(0));
        }
        /*
        [Test]
        public void QueriesClass_When_RemoveUser_CountOutput()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int uID = 1;
            queries.RemoveUser(uID);
            int db = (from user in ot.Users
                      where user.Userid == uID
                      select user).Count();
            //ASSERT
            Assert.That(db, Is.EqualTo(0));
        }*/
        /*
        [Test]
        public void QueriesClass_When_RemoveAdvertiser_CountOutput()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int aID = 1;
            queries.RemoveAdvertiser(aID);
            int db = (from adv in ot.Advertiser
                      where adv.Advertiserid == aID
                      select adv).Count();
            //ASSERT
            Assert.That(db, Is.EqualTo(0));
        }*/
        /*[Test]
        public void QueriesClass_When_UpdateUser_CorrectUpdate()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int uID = 2;
            string newName = "ValtoztastMeg";
            var users = ot.Users.Where(x => x.Userid == uID);
            Users wanted = null;
            foreach (var akt in users)
            {
                wanted = akt;
            }
            queries.UpdateUser(uID, wanted.Password, newName, wanted.Email, wanted.Phonenumber, wanted.Address);
            var users2 = ot.Users.Where(x => x.Userid == uID);
            Users expected = null;
            foreach (var akt in users2)
            {
                expected = akt;
            }
            //ASSERT
            Assert.That(expected.Name, Is.EqualTo(newName));
        }*/
        /*[Test]
        public void QueriesClass_When_UpdateAdvertiser_CorrectUpdate()
        {
            //ARRANGE
            Queries queries = new Queries();
            OtEntities ot = new OtEntities();
            //ACT
            int aID = 2;
            string newName = "ValtoztastMeg";
            var users = from adv in ot.Advertiser
                        where adv.Advertiserid == aID
                        select adv;
            Advertiser wanted = null;
            foreach (var akt in users)
            {
                wanted = akt;
            }
            queries.UpdateAdvertiser(aID, wanted.Password, newName, wanted.Email, wanted.Phonenumber, wanted.Address);
            var users2 = from adv in ot.Advertiser
                         where adv.Advertiserid == aID
                         select adv;
            Advertiser expected = null;
            foreach (var akt in users2)
            {
                expected = akt;
            }
            //ASSERT
            Assert.That(expected.Name, Is.EqualTo(newName));
        }*/
        /// <summary>
        /// GetAllAccommodation method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_GetAllAccommodation_CountCorrect()
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            Queries queries = new Queries();
            //ACT
            List<Accommodation> listUsers = queries.GetAllAccommodation();
            int aCount = ot.Accommodation.Where(x=>x.Accommodationid > 0).Count();
            //ASSERT
            Assert.That(listUsers.Count(), Is.EqualTo(aCount));
        }
        /// <summary>
        /// GetAllComment method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_GetAllComment_CountCorrect()
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            Queries queries = new Queries();
            //ACT
            List<Comment> listUsers = queries.GetAllComment();
            int cCount = ot.Comment.Count();
            //ASSERT
            Assert.That(listUsers.Count(), Is.EqualTo(cCount));
        }
        /// <summary>
        /// GetAllUser method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_GetAllUser_CountCorrect()
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            Queries queries = new Queries();
            //ACT
            List<Users> listUsers = queries.GetAllUser();
            int userCount = ot.Users.Count()-1;
            //ASSERT
            Assert.That(listUsers.Count(), Is.EqualTo(userCount));
        }
        /// <summary>
        /// GetAllAdvertiser method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_GetAllAdvertiser_CountCorrect()
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            Queries queries = new Queries();
            //ACT
            List<Advertiser> listUsers = queries.GetAllAdvertiser();
            int advertiserCount = ot.Advertiser.Count()-1;
            //ASSERT
            Assert.That(listUsers.Count(), Is.EqualTo(advertiserCount));
        }
        /// <summary>
        /// ReturnAdvertiserbyI method test.
        /// </summary>
        [Test]
        public void QueriesClass_When_ReturnAdvertiserbyID_CountCorrect()
        {
            //ARRANGE
            OtEntities ot = new OtEntities();
            Queries queries = new Queries();
            //ACT
            int aID = 1;
            Advertiser advertiser = queries.ReturnAdvertiserbyID(aID);
            var advertiser2 = ot.Advertiser.Where(x => x.Advertiserid == aID);
            Advertiser expected = null;
            foreach (var akt in advertiser2)
            {
                expected = akt;
            }
            //ASSERT
            Assert.That(advertiser.Name.ToString(), Is.EqualTo(expected.Name));
        }
    }
}
