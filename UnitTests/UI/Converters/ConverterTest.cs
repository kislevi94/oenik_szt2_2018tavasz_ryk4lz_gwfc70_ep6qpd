﻿//-----------------------------------------------------------------------
// <copyright file="Queries.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace UnitTests
{
    using DataLayer;
    using NUnit.Framework;
    using OgavirTours.Converters;
    using OgavirTours.Util.Entities;
    /// <summary>
    /// UI Project Converter class tests
    /// </summary>
    [TestFixture]
    class ConverterTest
    {
        /// <summary>
        /// ToUser converter test.
        /// </summary>
        [Test]
        public void DataUserToUserConverterClass_When_ToUser_CorrectOutput()
        {
            //ARRANGE
            Users userIn = new Users();
            //ACT
            userIn.Name = "Teszt";
            userIn.Address = "asd";
            userIn.Email = "asd";
            userIn.IsModerator = false;
            userIn.Phonenumber = "asd";
            User userOut = DataUserToUserConverter.ToUser(userIn);
            //ASSERT
            Assert.That(userOut.Name, Is.EqualTo(userIn.Name));
        }
        /// <summary>
        /// ToModerator converter test.
        /// </summary>
        [Test]
        public void DataModeratorToModeratorClass_When_ToModerator_CorrectOutput()
        {
            //ARRANGE
            DataLayer.Moderator userIn = new DataLayer.Moderator();
            //ACT
            userIn.Moderatorid = 30;
            userIn.Accommodationid = 3;
            userIn.Accommodation = new Queries().ReturnAccommodationById(userIn.Accommodationid);
            userIn.Userid = 3;
            userIn.Users = new Queries().ReturnUserById(userIn.Userid);
            OgavirTours.Util.Entities.Moderator userOut = DataModeratorToModerator.ToModerator(userIn);
            //ASSERT
            Assert.That(userOut.ModeratorId, Is.EqualTo(userIn.Moderatorid));
        }
        /// <summary>
        /// ToAdvertiser converter test.
        /// </summary>
        [Test]
        public void DataAdvertiserToAdvertiserConverterClass_When_ToAdvertiser_CorrectOutput()
        {
            //ARRANGE
            DataLayer.Advertiser userIn = new DataLayer.Advertiser();
            //ACT
            userIn.Advertiserid = 30;
            OgavirTours.Util.Entities.Advertiser userOut = DataAdvertiserToAdvertiserConverter.ToAdvertiser(userIn);
            //ASSERT
            Assert.That(userOut.Advertiserid, Is.EqualTo(userIn.Advertiserid));
        }
    }
}
