﻿// <copyright file="Login.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System.Windows;
    using System.Windows.Controls;
    using OgavirTours.Converters;
    using OgavirTours.ViewModels;
    using Util.Entities;

    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : UserControl, ISwitchable
    {
        private LoginViewModel vm;
        private ApplicationState applicationState;

        /// <summary>
        /// Initializes a new instance of the <see cref="Login"/> class.
        /// </summary>
        public Login()
        {
            this.InitializeComponent();
            this.vm = new LoginViewModel();
            this.DataContext = this.vm;
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="applicationState">Information about the applications current state</param>
        public void UtilizeState(ApplicationState applicationState)
        {
            this.applicationState = applicationState;
            this.vm.Menu = this.applicationState.Menu;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.vm.Password = this.pwbox.Password;
            this.vm.Username = this.textbox.Text;
            
            try
            {
                if (!this.vm.IsAdvertiser)
                {
                    this.applicationState.CurrentUser = this.applicationState.LogicsHandler.Authentication
                        .UserAuthentication(this.vm.Username, this.vm.Password)
                        .ToUser();
                }
               else if (this.vm.IsAdvertiser)
                {
                    this.applicationState.CurrentUser = this.applicationState.LogicsHandler.Authentication
                        .AdvertiserAuthentication(this.vm.Username, this.vm.Password)
                        .ToAdvertiser();
                }

                Switcher.Switch(new MainPage(), this.applicationState);
            }
            catch (System.Exception error)
            {
                this.vm.IsAdvertiser = false;
              this.vm.ErrorInfo = error.Message;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new MainPage(), this.applicationState);
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }
    }
}
