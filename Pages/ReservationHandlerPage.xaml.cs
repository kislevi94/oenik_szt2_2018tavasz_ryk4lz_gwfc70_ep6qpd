﻿// <copyright file="ReservationHandlerPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media.Imaging;
    using Converters;
    using Microsoft.Win32;
    using OgavirTours.ViewModels;
    using Util.Entities;

    /// <summary>
    /// Interaction logic for ReservationHandlerPage.xaml
    /// </summary>
    public partial class ReservationHandlerPage : UserControl, ISwitchable
    {
        private ReservationHandlerPageViewModel vm;
        private ApplicationState applicationState;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReservationHandlerPage"/> class.
        /// </summary>
        public ReservationHandlerPage()
        {
            this.InitializeComponent();
            this.vm = new ReservationHandlerPageViewModel();
            this.DataContext = this.vm;
            this.vm.NewAccommodation = new Accommodation();
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="state">Information about the applications current state</param>
        public void UtilizeState(ApplicationState applicationState)
        {
            this.applicationState = applicationState;
            this.vm.Menu = this.applicationState.Menu;
            if (this.applicationState.CurrentUser is Advertiser)
            {
                this.vm.Accommodations = this.applicationState.LogicsHandler.AccommodationManagement.ListAdvertiserOwnedAccommodation((this.applicationState.CurrentUser as Advertiser).Advertiserid).ToAccommodationObservableCollection();
            }
            else
            {
                this.vm.Accommodations = this.applicationState.LogicsHandler.AccommodationManagement.ListUserModeratedAccomodation((this.applicationState.CurrentUser as User).UserId).ToAccommodationObservableCollection();
            }

            if (this.vm.SelectedAccommodation != null)
            {
                this.vm.Reservations = this.applicationState.LogicsHandler.ReservationManagement.ListAccommodationReservation(this.vm.SelectedAccommodation.Accommodationid).ToReservationObservableCollection();
            }

            this.vm.ListExistingImages = new System.Collections.ObjectModel.ObservableCollection<BitmapImage>();
            foreach (Accommodation item in this.applicationState.LogicsHandler.AccommodationManagement.ListAccommodation().ToAccommodationObservableCollection())
            {
                this.vm.ListExistingImages.Add(item.Image);
            }
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            // TODO: átírni a helpmethodsba?
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            this.vm.NewAccommodation.Name = string.Empty;
            this.vm.NewAccommodation.Address = string.Empty;
            this.vm.NewAccommodation.Image = new BitmapImage(new Uri("pack://application:,,,/Images/temp.JPG"));
            this.vm.NewAccommodation.OnePersonRoomPrice = 0;
            this.vm.NewAccommodation.TwoPersonRoomPrice = 0;
            this.vm.NewAccommodation.ThreePersonRoomPrice = 0;
            this.vm.NewAccommodation.FourPersonRoomPrice = 0;
            this.vm.NewAccommodation.Wifi = false;
            this.vm.NewAccommodation.PetFriendly = false;
            this.vm.NewAccommodation.LuggageRoom = false;
            this.vm.NewAccommodation.Pool = false;
            this.vm.NewAccommodation.Advertiserid = 0;
        }

        private void SelectedAccommodation_Changed(object sender, SelectionChangedEventArgs e)
        {
            if (this.vm.SelectedAccommodation != null)
            {
                this.vm.Reservations = this.applicationState.LogicsHandler.ReservationManagement.ListAccommodationReservation(this.vm.SelectedAccommodation.Accommodationid).ToReservationObservableCollection();
                this.vm.NewAccommodation.Name = this.vm.SelectedAccommodation.Name;
                this.vm.NewAccommodation.LuggageRoom = this.vm.SelectedAccommodation.LuggageRoom;
                this.vm.NewAccommodation.PetFriendly = this.vm.SelectedAccommodation.PetFriendly;
                this.vm.NewAccommodation.Pool = this.vm.SelectedAccommodation.Pool;
                this.vm.NewAccommodation.Wifi = this.vm.SelectedAccommodation.Wifi;
                this.vm.NewAccommodation.Address = this.vm.SelectedAccommodation.Address;
                this.vm.NewAccommodation.Image = this.vm.SelectedAccommodation.Image;
            }
        }

        private void ImagePicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.vm.NewAccommodation.Image = sender as BitmapImage;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.LogicsHandler.AccommodationManagement.RegisterAccommodation(
               this.vm.NewAccommodation.Name,
               this.vm.NewAccommodation.Address,
               this.vm.NewAccommodation.Advertiserid,
               this.vm.NewAccommodation.Image == null ? string.Empty : this.vm.NewAccommodation.Image.UriSource.ToString(),
               this.vm.NewAccommodation.OnePersonRoomPrice,
               this.vm.NewAccommodation.TwoPersonRoomPrice,
               this.vm.NewAccommodation.ThreePersonRoomPrice,
               this.vm.NewAccommodation.FourPersonRoomPrice,
               this.vm.NewAccommodation.Wifi,
               this.vm.NewAccommodation.PetFriendly,
               this.vm.NewAccommodation.LuggageRoom,
               this.vm.NewAccommodation.Pool);
            this.vm.ErrorInfo = "Accommodation registered successfully";
        }

        private void StatusUpdateDone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.applicationState.LogicsHandler.ReservationManagement.UpdateReservationStatus(this.vm.SelectedAccommodation.Accommodationid, DataLayer.Status.Completed);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void StatusUpdateDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.applicationState.LogicsHandler.ReservationManagement.UpdateReservationStatus(this.vm.SelectedAccommodation.Accommodationid, DataLayer.Status.Deleted);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
