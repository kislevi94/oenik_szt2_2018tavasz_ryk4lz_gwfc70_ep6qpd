﻿// <copyright file="MessagesPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using Converters;
    using OgavirTours.Interfaces;
    using OgavirTours.Util.Entities;
    using OgavirTours.ViewModels;

    /// <summary>
    /// Interaction logic for MessagesPage.xaml
    /// </summary>
    public partial class MessagesPage : UserControl, ISwitchable
    {
        private ApplicationState applicationState;
        private MessagePageViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagesPage"/> class.
        /// </summary>
        public MessagesPage()
        {
            this.InitializeComponent();
            this.vm = new MessagePageViewModel();
            this.DataContext = this.vm;
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="applicationState">Information about the applications current state</param>
        public void UtilizeState(ApplicationState applicationState)
        {
            this.applicationState = applicationState;
            this.vm.Menu = this.applicationState.Menu;
            this.vm.IsMessageUnloaded = true;
            this.vm.From = this.applicationState.CurrentUser.Name;

            Dictionary<string, string> codeToName = new Dictionary<string, string>();

            foreach (IUserEntity item in this.applicationState.LogicsHandler.UserAndAdministratorManagement.ListUsers().ToUserObservableCollection())
            {
                if (!item.ToString().StartsWith("Deleted") && item.Email != this.applicationState.CurrentUser.Email)
                {
                    this.vm.ExistingUsers.Add(item);
                }

                codeToName.Add("u" + (item as User).UserId, item.Name);
            }

            foreach (IUserEntity item in this.applicationState.LogicsHandler.UserAndAdministratorManagement.ListAdvertiser().ToAdvertiserObservableCollection())
            {
                if (!item.ToString().StartsWith("Deleted") && item.Email != this.applicationState.CurrentUser.Email)
                {
                    this.vm.ExistingUsers.Add(item);
                }

                codeToName.Add("a" + (item as Advertiser).Advertiserid, item.Name);
            }

            if (this.applicationState.CurrentUser is User)
            {
                this.vm.SentMessages = this.applicationState.LogicsHandler.MessageManagement.GetOutgoingMessages("u", (this.applicationState.CurrentUser as User).UserId).ToMessageObservableCollection(codeToName, this.applicationState.CurrentUser);
                this.vm.ReceivedMessages = this.applicationState.LogicsHandler.MessageManagement.GetIncomingMessages("u", (this.applicationState.CurrentUser as User).UserId).ToMessageObservableCollection(this.applicationState.CurrentUser, codeToName);
            }
            else if (this.applicationState.CurrentUser is Advertiser)
            {
                this.vm.SentMessages = this.applicationState.LogicsHandler.MessageManagement.GetOutgoingMessages("a", (this.applicationState.CurrentUser as Advertiser).Advertiserid).ToMessageObservableCollection(codeToName, this.applicationState.CurrentUser);
                this.vm.ReceivedMessages = this.applicationState.LogicsHandler.MessageManagement.GetIncomingMessages("a", (this.applicationState.CurrentUser as Advertiser).Advertiserid).ToMessageObservableCollection(this.applicationState.CurrentUser, codeToName);
            }
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }

        private void Sent_Click(object sender, RoutedEventArgs e)
        {
            this.vm.CurrentMessageCollection = this.vm.SentMessages;
        }

        private void Received_Click(object sender, RoutedEventArgs e)
        {
            this.vm.CurrentMessageCollection = this.vm.ReceivedMessages;
        }

        private void SelectedMessage_DoubleClick(object sender, RoutedEventArgs e)
        {
            this.vm.Message = this.vm.SelectedMessage.Text;
            this.vm.Subject = this.vm.SelectedMessage.Subject;
            this.vm.IsMessageUnloaded = false;
            this.vm.To = null;
        }

        private void NewMessage_Click(object sender, RoutedEventArgs e)
        {
            this.vm.IsMessageUnloaded = true;
            this.vm.SelectedMessage = null;
            this.vm.Message = string.Empty;
            this.vm.Subject = string.Empty;
        }

        private void Send_Click(object sender, RoutedEventArgs e)
        {
            if (this.vm.To != null && !string.IsNullOrWhiteSpace(this.vm.Message))
            {
                this.applicationState.LogicsHandler.MessageManagement.SendMessage(this.applicationState.CurrentUser.Username, this.vm.To.Username, -1, this.vm.Subject, this.vm.Message);
                Switcher.Switch(new MessagesPage(), this.applicationState);
            }
        }
    }
}
