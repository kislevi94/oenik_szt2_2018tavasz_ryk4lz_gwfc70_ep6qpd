﻿// <copyright file="UserProfilePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System.Windows;
    using System.Windows.Controls;
    using Converters;
    using OgavirTours.Util.Entities;
    using OgavirTours.ViewModels;

    /// <summary>
    /// Interaction logic for UserProfilePage.xaml
    /// </summary>
    public partial class UserProfilePage : UserControl, ISwitchable
    {
        private ApplicationState applicationState;
        private UserProfileViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserProfilePage"/> class.
        /// </summary>
        public UserProfilePage()
        {
            this.vm = new UserProfileViewModel();
            this.DataContext = this.vm;
            this.InitializeComponent();
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="state">Information about the applications current state</param>
        public void UtilizeState(ApplicationState state)
        {
            this.applicationState = state;
            this.vm.Menu = this.applicationState.Menu;
            this.vm.Reservations = this.applicationState.LogicsHandler.ReservationManagement.ListUserReservation((this.applicationState.CurrentUser as User).UserId).ToReservationObservableCollection();
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }
    }
}
