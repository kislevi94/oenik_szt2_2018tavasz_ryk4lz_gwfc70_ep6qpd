﻿// <copyright file="MainPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;
    using Converters;
    using OgavirTours.ViewModels;
    using Util.Entities;

    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : UserControl, ISwitchable
    {
        private MainPageViewModel vm;
        private ApplicationState applicationState;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainPage"/> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();
            this.vm = new MainPageViewModel();
            this.vm.SearchingText = "Search...";
            this.DataContext = this.vm;
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="state">Information about the applications current state</param>
        public void UtilizeState(ApplicationState state)
        {
            this.applicationState = state;
            this.vm.Menu = this.applicationState.Menu;

            try
            {
                this.vm.BestRated = this.applicationState.LogicsHandler.AccommodationManagement
                    .ListBestRatedAccommodation(5)
                    .ToAccommodationObservableCollection();
            }
            catch (System.Exception e)
            {
                this.vm.ErrorMessage = e.Message;
            }

            if (this.applicationState.CurrentUser == null)
            {
                this.vm.IsLoggedInVisibility = Visibility.Visible;
            }
            else
            {
                this.vm.IsLoggedInVisibility = Visibility.Hidden;
            }
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new Login(), this.applicationState);
        }

        private void Signup_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new SignUp(), this.applicationState);
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }

        private void BestRatedAccommodation_DoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Switcher.Switch(new AccommodationProfile(this.vm.SelectedAccommodation), this.applicationState);
        }

        private void Key_Down(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                try
                {
                    ObservableCollection<Accommodation> searchResults = this.applicationState.LogicsHandler.Search
                        .ShortSearch((sender as TextBox).Text)
                        .ToAccommodationObservableCollection();
                    Switcher.Switch(new AdvancedSearchPage(searchResults), this.applicationState);
                }
                catch (System.Exception error)
                {
                    this.vm.ErrorMessage = error.Message;
                }
            }
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.vm.SearchingText = string.Empty;
        }
    }
}
