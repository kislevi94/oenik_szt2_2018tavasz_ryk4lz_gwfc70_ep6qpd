﻿// <copyright file="ModifyPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System.Windows;
    using System.Windows.Controls;
    using OgavirTours.Interfaces;
    using OgavirTours.Util.Entities;
    using OgavirTours.ViewModels;

    /// <summary>
    /// Interaction logic for ModifyPage.xaml
    /// </summary>
    public partial class ModifyPage : UserControl, ISwitchable
    {
        private ApplicationState applicationState;
        private ModifyPageViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModifyPage"/> class.
        /// </summary>
        /// <param name="entity">User or Advertiser to modify</param>
        public ModifyPage(IUserEntity entity)
        {
            this.InitializeComponent();
            this.vm = new ModifyPageViewModel();
            this.vm.CurrentUser = entity;
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="state">Information about the applications current state</param>
        public void UtilizeState(ApplicationState state)
        {
            this.applicationState = state;
            this.vm.Menu = this.applicationState.Menu;
            this.vm.Name = this.applicationState.CurrentUser.Name;
            this.vm.Password = this.applicationState.CurrentUser.Password;
            this.vm.Phone = this.applicationState.CurrentUser.PhoneNumber;
            this.vm.Email = this.applicationState.CurrentUser.Email;
            this.vm.Address = this.applicationState.CurrentUser.Address;
        }

        private void Modify_Click(object sender, RoutedEventArgs e)
        {
            if (this.vm.CurrentUser is User)
            {
                this.applicationState.LogicsHandler.UserAndAdministratorManagement.ModifyUser(
                    (this.vm.CurrentUser as User).UserId,
                    this.vm.Password,
                    this.vm.Name,
                    this.vm.Email,
                    this.vm.Phone,
                    this.vm.Address);
            }
            else
            {
                this.applicationState.LogicsHandler.UserAndAdministratorManagement.ModifyAdvertiser(
                    (this.vm.CurrentUser as Advertiser).Advertiserid,
                    this.vm.Password,
                    this.vm.Name,
                    this.vm.Email,
                    this.vm.Phone,
                    this.vm.Address);
            }
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new MainPage(), this.applicationState);
        }
    }
}
