﻿// <copyright file="SignUp.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using OgavirTours.ViewModels;

    /// <summary>
    /// Interaction logic for SignUp.xaml
    /// </summary>
    public partial class SignUp : UserControl, ISwitchable
    {
        private SignUpViewModel vm;
        private ApplicationState applicationState;

        /// <summary>
        /// Initializes a new instance of the <see cref="SignUp"/> class.
        /// </summary>
        public SignUp()
        {
            this.InitializeComponent();
            this.vm = new SignUpViewModel();
            this.DataContext = this.vm;
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="state">Information about the applications current state</param>
        public void UtilizeState(ApplicationState state)
        {
            this.applicationState = state;
            this.vm.Menu = this.applicationState.Menu;
        }

        private bool NotNullNotTooLong(string s)
        {
            if (s == null || s.Length <= 0 || s.Length > 50)
            {
                this.vm.ErrorInfo = "All fields should be more than 1 character, but less than 49! ";
                return false;
            }
            else
            {
                this.vm.ErrorInfo = string.Empty;
                return true;
            }
        }

        private bool PasswordAndPasswordAgainAreTheSame()
        {
            if (this.vm.Password == this.vm.PasswordAgain)
            {
                this.vm.ErrorInfo = string.Empty;
                return true;
            }
            else
            {
                this.vm.ErrorInfo = "The two password fields didn't match.";
                return false;
            }
        }

        private void SignUp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.PasswordAndPasswordAgainAreTheSame())
                {
                    this.Cursor = Cursors.Wait;
                    Task t = new Task(()=>this.applicationState.LogicsHandler.Authentication.Registration(
                                        this.vm.IsAdvertiser,
                                        this.vm.Username,
                                        this.vm.Password,
                                        this.vm.FirstName + " " + this.vm.LastName, // !!!!!!!!!!!!!!!!!!!!!!!!!!
                                        this.vm.Email,
                                        this.vm.PhoneNumber,
                                        this.vm.Address,
                                        this.vm.City,
                                        this.vm.PostCode))
                                        ;
                    t.Start();
                    System.Threading.Thread.Sleep(1000);
                    this.Cursor = Cursors.Arrow;
                    Switcher.Switch(new MainPage(), this.applicationState);
                }
            }
            catch (System.Exception exc)
            {
                this.vm.ErrorInfo = exc.Message;
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new MainPage(), this.applicationState);
        }

        private void TextBox_Clear(object sender, RoutedEventArgs e)
        {
            if ((sender as TextBox).Text == "Required data" || (sender as TextBox).Text == "Not required")
            {
                (sender as TextBox).Text = string.Empty;
            }
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }
    }
}
