﻿// <copyright file="AccommodationProfile.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using Converters;
    using OgavirTours.Util.Entities;
    using OgavirTours.ViewModels;

    /// <summary>
    /// Interaction logic for AccommodationProfile.xaml
    /// </summary>
    public partial class AccommodationProfile : UserControl, ISwitchable
    {
        private ApplicationState applicationState;
        private AccommodationProfileViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccommodationProfile"/> class.
        /// </summary>
        /// <param name="accommodation">Accommodation to show details about</param>
        public AccommodationProfile(Accommodation accommodation)
        {
            this.InitializeComponent();
            this.vm = new AccommodationProfileViewModel();
            this.DataContext = this.vm;
            this.vm.CurrentAccommodation = accommodation;
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="state">Information about the applications current state</param>
        public void UtilizeState(ApplicationState state)
        {
            this.applicationState = state;
            this.vm.Menu = this.applicationState.Menu;
            this.vm.CurrentAccommodation.Comments = this.applicationState.LogicsHandler.CommentManagement.GetComments(this.vm.CurrentAccommodation.Accommodationid).ToCommentObservableCollection();
            this.vm.CurrentAccommodation.Rating = Math.Round(this.vm.CurrentAccommodation.Rating, 2);
            this.vm.VisibilityWiFi = this.vm.CurrentAccommodation.Wifi ? Visibility.Visible : Visibility.Collapsed;
            this.vm.VisibilityPetFriendly = this.vm.CurrentAccommodation.PetFriendly ? Visibility.Visible : Visibility.Collapsed;
            this.vm.VisibilityLuggageRoom = this.vm.CurrentAccommodation.LuggageRoom ? Visibility.Visible : Visibility.Collapsed;
            this.vm.VisibilityPool = this.vm.CurrentAccommodation.Pool ? Visibility.Visible : Visibility.Collapsed;
        }

        private void RateAndUpdateRating(int rated)
        {
            this.vm.CurrentAccommodation.RatingNumber++;

            this.applicationState.LogicsHandler.AccommodationManagement
                .UpdateAccomodationreting((this.applicationState.CurrentUser as User).UserId, this.vm.CurrentAccommodation.Accommodationid, rated);

            this.vm.CurrentAccommodation.Rating = this.applicationState.LogicsHandler.AccommodationManagement
                .ListAccommodation()
                .Single(acc => acc.Accommodationid == this.vm.CurrentAccommodation.Accommodationid)
                .Rating.Value;

            this.vm.CurrentAccommodation.Rating = Math.Round(this.vm.CurrentAccommodation.Rating, 2);
        }

        private void Booking_Click(object sender, RoutedEventArgs e)
        {
            if (!(this.applicationState.CurrentUser is Advertiser))
            {
                try
                {
                    if (this.applicationState.CurrentUser != null)
                    {
                        this.applicationState.LogicsHandler.ReservationManagement.MakeReservation(
                            (this.applicationState.CurrentUser as User).UserId,
                            this.vm.CurrentAccommodation.Accommodationid,
                            this.vm.SelectedRoomType.ToDataRoomType(),
                            new DateTime(this.vm.SelectedDate.Year, this.vm.SelectedDate.Month, this.vm.SelectedDate.Day),
                            this.vm.SelectedNumberOfNights);
                        Switcher.Switch(new MainPage(), this.applicationState);
                    }
                    else
                    {
                        this.vm.ErrorInfo = "Log in or sign up to make a reservation!";
                    }
                }
                catch (Exception error)
                {
                    this.vm.ErrorInfo = error.Message;
                }
            }
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }

        private void RateThis_Click(object sender, RoutedEventArgs e)
        {
            if (this.applicationState.CurrentUser != null)
            {
                this.vm.ErrorInfo = string.Empty;
                if ((bool)this.fifth.IsChecked)
                {
                    try
                    {
                        this.RateAndUpdateRating(5);
                    }
                    catch (Exception error)
                    {
                        this.vm.ErrorInfo = error.Message;
                    }
                }
                else if ((bool)this.fourth.IsChecked)
                {
                    try
                    {
                        this.RateAndUpdateRating(4);
                    }
                    catch (Exception error)
                    {
                        this.vm.ErrorInfo = error.Message;
                    }
                }
                else if ((bool)this.third.IsChecked)
                {
                    try
                    {
                        this.RateAndUpdateRating(3);
                    }
                    catch (Exception error)
                    {
                        this.vm.ErrorInfo = error.Message;
                    }
                }
                else if ((bool)this.second.IsChecked)
                {
                    try
                    {
                        this.RateAndUpdateRating(2);
                    }
                    catch (Exception error)
                    {
                        this.vm.ErrorInfo = error.Message;
                    }
                }
                else if ((bool)this.first.IsChecked)
                {
                    try
                    {
                        this.RateAndUpdateRating(1);
                    }
                    catch (Exception error)
                    {
                        this.vm.ErrorInfo = error.Message;
                    }
                }
            }
            else
            {
                this.vm.ErrorInfo = "Log in or sign up to rate this accommodation!";
            }
        }

        private void RatingButtonClickEventHandler(object sender, RoutedEventArgs e)
        {
            string i = (string)(sender as ToggleButton).Tag;
            foreach (ToggleButton item in this.stargrid.Children)
            {
                if (int.Parse((string)item.Tag) <= int.Parse(i))
                {
                    item.IsChecked = true;
                }
                else
                {
                    item.IsChecked = false;
                }
            }
        }

        private void Comment_Send_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.applicationState.CurrentUser != null)
                {
                   // this.vm.NewComment.Username = this.applicationState.CurrentUser.Username;
                    this.vm.CurrentAccommodation.Comments.Add(this.vm.NewComment);
                    this.applicationState.LogicsHandler.CommentManagement.MakeComment((this.applicationState.CurrentUser as User).UserId, this.vm.CurrentAccommodation.Accommodationid, this.vm.NewComment.Text);
                }
            }
            catch (Exception)
            {
                throw;
            }

            Switcher.Switch(new AccommodationProfile(this.vm.CurrentAccommodation), this.applicationState);
        }
    }
}
