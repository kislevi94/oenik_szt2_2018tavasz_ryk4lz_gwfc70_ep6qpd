﻿// <copyright file="SearchResultPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;
    using OgavirTours.Util.Entities;
    using OgavirTours.ViewModels;

    /// <summary>
    /// Interaction logic for SearchResultPage.xaml
    /// </summary>
    public partial class SearchResultPage : UserControl, ISwitchable
    {
        private SearchResultPageViewModel vm;
        private ApplicationState applicationState;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchResultPage"/> class.
        /// </summary>
        /// <param name="searchResults">results from short search</param>
        public SearchResultPage(ObservableCollection<Accommodation> searchResults)
        {
            this.InitializeComponent();

            this.vm = new SearchResultPageViewModel();

            foreach (Accommodation item in searchResults)
            {
                this.vm.SearchResults.Add(item);
            }

            this.DataContext = this.vm;
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="state">Information about the applications current state</param>
        public void UtilizeState(ApplicationState state)
        {
            this.applicationState = state;
            this.vm.Menu = this.applicationState.Menu;
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }

        private void AccItem_DoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Switcher.Switch(new AccommodationProfile(this.vm.SelectedAccommodation), this.applicationState);
        }
    }
}
