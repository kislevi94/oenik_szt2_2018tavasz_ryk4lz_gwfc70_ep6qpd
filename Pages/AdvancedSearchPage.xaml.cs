﻿// <copyright file="AdvancedSearchPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Converters;
    using OgavirTours.ViewModels;
    using Util.Entities;

    /// <summary>
    /// Interaction logic for AdvancedSearchPage.xaml
    /// </summary>
    public partial class AdvancedSearchPage : UserControl, ISwitchable
    {
        private ApplicationState applicationState;
        private AdvancedSearchPageViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchPage"/> class.
        /// </summary>
        public AdvancedSearchPage()
        {
            this.InitializeComponent();
            this.vm = new AdvancedSearchPageViewModel();
            this.DataContext = this.vm;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSearchPage"/> class.
        /// </summary>
        /// <param name="searchResults">Accommodation ObservableCollection from short search</param>
        public AdvancedSearchPage(ObservableCollection<Accommodation> searchResults)
        {
            this.InitializeComponent();
            this.vm = new AdvancedSearchPageViewModel();
            this.vm.SearchResults = searchResults;
            this.DataContext = this.vm;
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="state">Information about the applications current state</param>
        public void UtilizeState(ApplicationState state)
        {
            this.applicationState = state;
            this.vm.Menu = this.applicationState.Menu;
        }

        private void AdvancedSearch_Click(object sender, RoutedEventArgs e)
        {
            this.vm.SearchResults = this.applicationState.LogicsHandler.Search.LongSearch(
                 this.vm.Location,
                 this.vm.Name,
                 this.vm.Wifi,
                 this.vm.PetFriendly,
                 this.vm.Pool,
                 this.vm.Luggage).ToAccommodationObservableCollection();
           this.vm.Location = string.Empty;
            this.vm.Name = string.Empty;
            this.vm.Wifi = false;
            this.vm.PetFriendly = false;
            this.vm.Pool = false;
            this.vm.Luggage = false;
        }

        private void AccItem_DoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Switcher.Switch(new AccommodationProfile(this.vm.SelectedAccommodation), this.applicationState);
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }
    }
}
