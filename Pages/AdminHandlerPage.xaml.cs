﻿// <copyright file="AdminHandlerPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Pages
{
    using System.Windows;
    using System.Windows.Controls;
    using Converters;
    using OgavirTours.ViewModels;

    /// <summary>
    /// Interaction logic for AdminHandlerPage.xaml
    /// </summary>
    public partial class AdminHandlerPage : UserControl, ISwitchable
    {
        private ApplicationState applicationState;
        private AdminHandlerPageViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminHandlerPage"/> class.
        /// </summary>
        public AdminHandlerPage()
        {
            this.InitializeComponent();
            this.vm = new AdminHandlerPageViewModel();
            this.DataContext = this.vm;
        }

        /// <summary>
        /// Loads the necessary information to the viewmodel
        /// </summary>
        /// <param name="state">Information about the applications current state</param>
        public void UtilizeState(ApplicationState state)
        {
            this.applicationState = state;
            this.vm.Menu = this.applicationState.Menu;
            this.vm.Advertisers = this.applicationState.LogicsHandler.UserAndAdministratorManagement.ListAdvertiser().ToAdvertiserObservableCollection();
            this.vm.Users = this.applicationState.LogicsHandler.UserAndAdministratorManagement.ListUsers().ToUserObservableCollection();
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.applicationState.MenuHandler((string)(e.OriginalSource as MenuItem).Header);
        }

        private void DeleteUser_Click(object sender, RoutedEventArgs e)
        {
            if (this.vm.SelectedUser != null)
            {
                this.applicationState.LogicsHandler.UserAndAdministratorManagement.DeleteUser(this.vm.SelectedUser.UserId);
            }
        }

        private void DeleteAdvertiser_Click(object sender, RoutedEventArgs e)
        {
            if (this.vm.SelectedAdvertiser != null)
            {
                this.applicationState.LogicsHandler.UserAndAdministratorManagement.DeleteAdvertiser(this.vm.SelectedAdvertiser.Advertiserid);
            }
        }

        private void ModifyUser_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ModifyPage(this.vm.SelectedUser), this.applicationState);
        }

        private void ModifyAdvertiser_Click(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ModifyPage(this.vm.SelectedAdvertiser), this.applicationState);
        }
    }
}
