﻿//-----------------------------------------------------------------------
// <copyright file="ISwitchable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OgavirTours
{
    /// <summary>
    /// Interface that must be implemented by every page (UserControls in Pages folder.).
    /// </summary>
    public interface ISwitchable
    {
        /// <summary>
        /// Passes information one page to another.
        /// </summary>
        /// <param name="state">Contains information about the page</param>
        void UtilizeState(ApplicationState state);
    }
}
