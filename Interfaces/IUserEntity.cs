﻿// <copyright file="IUserEntity.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Interfaces
{
    /// <summary>
    /// Common fields of User and Advertiser
    /// </summary>
    public interface IUserEntity
    {
        string Username { get; set; }

        string Password { get; set; }

        string Name { get; set; }

        string Email { get; set; }

        string PhoneNumber { get; set; }

        string Address { get; set; }
    }
}
