﻿//-----------------------------------------------------------------------
// <copyright file="IData.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DataLayer
#pragma warning restore SA1652 // Enable XML documentation output
#pragma warning restore SA1652 // Enable XML documentation output
#pragma warning restore SA1652 // Enable XML documentation output
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Datalayer interface
    /// </summary>
    public interface IData
    {
        /// <summary>
        /// Insert new advertiser into Advertiser table
        /// </summary>
        /// <param name="username">Username (unique)</param>
        /// <param name="password">Password MD5 hash generated from string</param>
        /// <param name="name">Name</param>
        /// <param name="email">Email</param>
        /// <param name="phone">PhoneNumber</param>
        /// <param name="address">Addres (fix format)</param>
        void InsertIntoAdvertiser(string username, string password, string name, string email, string phone, string address);

        /// <summary>
        /// Insert new user into Users table
        /// </summary>
        /// <param name="username">Username (unique)</param>
        /// <param name="password">Password generated from string with MD5</param>
        /// <param name="name">Name</param>
        /// <param name="email">Email</param>
        /// <param name="phone">Phonenumber</param>
        /// <param name="address">Address (fix format)</param>
        void InsertIntoUser(string username, string password, string name, string email, string phone, string address);

        /// <summary>
        /// Returns accommodation by id
        /// </summary>
        /// <param name="id">Id of the accommodation</param>
        /// <returns>Accomodation with correct id</returns>
        Accommodation ReturnAccommodationById(int id);

        /// <summary>
        /// Insert new reservation into Reservation Table
        /// </summary>
        /// <param name="userid">Userid of the user whose the reservation is</param>
        /// <param name="accommodationid">Accomondationid of the accommodation where the reservation is</param>
        /// <param name="roomtype">Number of how many people the room for</param>
        /// <param name="price">Price of the room</param>
        /// <param name="startDate">Date when the reservation start</param>
        /// <param name="length">Number of days</param>
        /// <param name="status">Status of the reservation</param>
        void InsertIntoReservation(int userid, int accommodationid, int roomtype, int price, DateTime startDate, int length, Status status);

        /// <summary>
        /// Insert a new message to the Message table
        /// </summary>
        /// <param name="nameFrom">Username of the sender</param>
        /// <param name="nameTo">Username of the cather</param>
        /// <param name="accid">Accommondationid if the addressed is accommodation</param>
        /// <param name="date">Sending date</param>
        /// <param name="subject">Subject of text</param>
        /// <param name="text">The text itself</param>
        void InsertIntoMessage(string nameFrom, string nameTo, int accid, DateTime date, string subject, string text);

        /// <summary>
        /// Return incoming messages
        /// </summary>
        /// <param name="idcode">Code generated from type and id</param>
        /// <returns>Return list of incoming messages</returns>
        List<Message> ReturnIncamingMessagesById(string idcode);

        /// <summary>
        /// Return outgoing messages
        /// </summary>
        /// <param name="idcode">Code generated from type and id</param>
        /// <returns>Return list of outgoing messages</returns>
        List<Message> ReturnOutGoingMessagesById(string idcode);

        /// <summary>
        /// Returns reservtions by user id and accommodation id
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        /// <returns>Returns list of reservtions by user id and accommodation id</returns>
        List<Reservation> ReturnReservationsByUseridAndAccid(int userid, int accid);

        /// <summary>
        /// Returns user by id
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns>User with correct id</returns>
        Users ReturnUserById(int id);

        /// <summary>
        /// Returns advertiser by id
        /// </summary>
        /// <param name="id">Id of the advertiser</param>
        /// <returns>Advertiser with correct id</returns>
        Advertiser ReturnAdvertiserbyID(int id);

        /// <summary>
        /// Update user rating
        /// </summary>
        /// <param name="userid">Rated user id</param>
        /// <param name="accid">Rater accommodation id</param>
        /// <param name="newRating">New rating</param>
        void UpdateUserRating(int userid, int accid, int newRating);

        /// <summary>
        /// Update accommodation rating
        /// </summary>
        /// <param name="userid">Rater user id</param>
        /// <param name="accid">Rated acccomodation id</param>
        /// <param name="newRating">New rating</param>
        void UpdateAccRating(int userid, int accid, int newRating);

        /// <summary>
        /// Insert a new comment into Comment table
        /// </summary>
        /// <param name="userid">Commenter user id</param>
        /// <param name="accomondationid">Commented accommodation id</param>
        /// <param name="text">Text of the comment</param>
        void InsertIntoComment(int userid, int accomondationid, string text);

        /// <summary>
        /// Insert new Modarator connection into Moderator table
        /// </summary>
        /// <param name="userid">Moderator user id</param>
        /// <param name="accid">Moderated acccommodation id</param>
        void InsertIntoModerator(int userid, int accid);

        /// <summary>
        /// Update reservation status
        /// </summary>
        /// <param name="resid">updated reservation id</param>
        /// <param name="endStatus">Status changed to</param>
        void UpdateReservationStatus(int resid, Status endStatus);

        /// <summary>
        /// Insert a new accommodation into the Accommodation table
        /// </summary>
        /// <param name="name">Name of the accommodation</param>
        /// <param name="address">Address of the accommodation</param>
        /// <param name="advid">Id of the Advertiser</param>
        /// <param name="picture">Picture of the accommodation</param>
        /// <param name="oneprice">One man room price</param>
        /// <param name="twoprice">Two man room price</param>
        /// <param name="threeprice">Three man room price</param>
        /// <param name="fourprice">Four man room price</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is the accommodation animal friendly</param>
        /// <param name="panorama">Is the panorama nice</param>
        /// <param name="pool">Is there a pool</param>
        void InsertIntoAccomodation(string name, string address, int advid, string picture, int oneprice, int twoprice, int threeprice, int fourprice, bool wifi, bool animal, bool panorama, bool pool);

        /// <summary>
        /// Update accommodation data
        /// </summary>
        /// <param name="id">Id of the accommodation</param>
        /// <param name="name">Name of the accommodation</param>
        /// <param name="address">Address of the accommodation</param>
        /// <param name="picture">Picture of the accommodation</param>
        /// <param name="oneprice">One man room price</param>
        /// <param name="twoprice">Two man room price</param>
        /// <param name="threeprice">Three man room price</param>
        /// <param name="fourprice">Four man room price</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is the accommodation animal friendly</param>
        /// <param name="panorama">Is the panorama nice</param>
        /// <param name="pool">Is there a pool</param>
        void UpdateAccommodation(int id, string name, string address, string picture, int oneprice, int twoprice, int threeprice, int fourprice, bool wifi, bool animal, bool panorama, bool pool);

        /// <summary>
        /// Remove Accommodation
        /// </summary>
        /// <param name="id">Id of the accommodation</param>
        void RemoveAccommodation(int id);

        /// <summary>
        /// Remove user
        /// </summary>
        /// <param name="id">Id of the user</param>
        void RemoveUser(int id);

        /// <summary>
        /// Remove advertiser
        /// </summary>
        /// <param name="id">Id of the advertiser</param>
        void RemoveAdvertiser(int id);

        /// <summary>
        /// Remove moderator connection by user id and accommodation id
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        void RemoveModeratorByUserAndAccId(int userid, int accid);

        /// <summary>
        /// Update user data
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="password">Password of the user</param>
        /// <param name="name">Name of the user</param>
        /// <param name="email">Email of the user</param>
        /// <param name="phone">Phone number of the user</param>
        /// <param name="address">Adress of the user</param>
        void UpdateUser(int userid, string password, string name, string email, string phone, string address);

        /// <summary>
        /// Update advertiser data
        /// </summary>
        /// <param name="advertiserid">Id of the advertiser</param>
        /// <param name="password">Password of the advertiser</param>
        /// <param name="name">Name of the advertiser</param>
        /// <param name="email">Email of the advertiser</param>
        /// <param name="phone">Phone number of the advertiser</param>
        /// <param name="address">Address of the advertiser</param>
        void UpdateAdvertiser(int advertiserid, string password, string name, string email, string phone, string address);

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns>List of all users</returns>
        List<Users> GetAllUser();

        /// <summary>
        /// Returns all advertiser
        /// </summary>
        /// <returns>List of all advertiser</returns>
        List<Advertiser> GetAllAdvertiser();

        /// <summary>
        /// Returns all accommodation
        /// </summary>
        /// <returns> List of all accommodation</returns>
        List<Accommodation> GetAllAccommodation();

        /// <summary>
        /// Returns all reservation
        /// </summary>
        /// <returns>List of all reservation</returns>
        List<Reservation> GetAllReservation();

        /// <summary>
        /// Raturns all comment
        /// </summary>
        /// <returns>List of all comment</returns>
        List<Comment> GetAllComment();

        /// <summary>
        /// Return accommodations where moderator is user with that id
        /// </summary>
        /// <param name="userid">id of the moderator</param>
        /// <returns>Return list of accommodations where moderator is user with that id</returns>
        List<Accommodation> ModeratedAccommodation(int userid);

        /// <summary>
        /// Return accommodations owned by advertiser with that id
        /// </summary>
        /// <param name="advid">id of the advertiser</param>
        /// <returns>Return list of accommodations owned by advertiser with that id</returns>
        List<Accommodation> OwnedAccomondation(int advid);
    }
}
