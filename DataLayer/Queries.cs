﻿//-----------------------------------------------------------------------
// <copyright file="Queries.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace DataLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    /// Reservation Status enum
    /// </summary>
    public enum Status
    {
        Unprocessed, Denied, Discarded, Appcetted, Completed, Notcompleted, SzRated, URated, MRated, Deleted
    }

    /// <summary>
    /// Database querry methods
    /// </summary>
    public class Queries : IData
    {
        /// <summary>
        /// ef connection
        /// </summary>
        private OtEntities ot;

        /// <summary>
        /// Initializes a new instance of the <see cref="Queries"/> class.
        /// Constructor for queries with ef connection
        /// </summary>
        public Queries()
        {
            this.ot = new OtEntities();
        }

        /// <summary>
        /// Insert new user into Users table
        /// </summary>
        /// <param name="username">Username (unique)</param>
        /// <param name="password">Password generated from string with MD5</param>
        /// <param name="name">Name</param>
        /// <param name="email">Email</param>
        /// <param name="phone">Phonenumber</param>
        /// <param name="address">Address (fix format)</param>
        public void InsertIntoUser(string username, string password, string name, string email, string phone, string address)
        {
            this.ot.Users.Add(
                new Users()
                {
                    Userid = this.NextIndex("Users"),
                    Username = username,
                    Password = password,
                    Name = name,
                    Email = email,
                    Phonenumber = phone,
                    Address = address,
                    IsModerator = false,
                    Rating = 0,
                    Ratingnumber = 0,
                });
            this.ot.SaveChanges();
        }

        /// <summary>
        /// Insert new advertiser into Advertiser table
        /// </summary>
        /// <param name="username">Username (unique)</param>
        /// <param name="password">Password MD5 hash generated from string</param>
        /// <param name="name">Name</param>
        /// <param name="email">Email</param>
        /// <param name="phone">PhoneNumber</param>
        /// <param name="address">Addres (fix format)</param>
        public void InsertIntoAdvertiser(string username, string password, string name, string email, string phone, string address)
        {
            this.ot.Advertiser.Add(
                new Advertiser()
                {
                    Advertiserid = this.NextIndex("Advertiser"),
                    Username = username,
                    Password = password,
                    Name = name,
                    Email = email,
                    Phonenumber = phone,
                    Address = address
                });
            this.ot.SaveChanges();
        }

        /// <summary>
        /// Insert new reservation into Reservation Table
        /// </summary>
        /// <param name="userid">Userid of the user whose the reservation is</param>
        /// <param name="accommodationid">Accomondationid of the accommodation where the reservation is</param>
        /// <param name="roomtype">Number of how many people the room for</param>
        /// <param name="price">Price of the room</param>
        /// <param name="startDate">Date when the reservation start</param>
        /// <param name="length">Number of days</param>
        /// <param name="status">Status of the reservation</param>
        public void InsertIntoReservation(int userid, int accommodationid, int roomtype, int price, DateTime startDate, int length, Status status)
        {
            this.ot.Reservation.Add(
                new Reservation()
                {
                    Reservationid = this.NextIndex("Reservation"),
                    Userid = userid,
                    Accommodationid = accommodationid,
                    Roomtype = roomtype,
                    Price = price,
                    Startdate = startDate,
                    Reservationdate = DateTime.Now,
                    Length = length,
                    Status = status.ToString()
                });
            this.ot.SaveChanges();
        }

        /// <summary>
        /// Insert a new message to the Message table
        /// </summary>
        /// <param name="nameFrom">Username of the sender</param>
        /// <param name="nameTo">Username of the cather</param>
        /// <param name="accid">Accommondationid if the addressed is accommodation</param>
        /// <param name="date">Sending date</param>
        /// <param name="subject">Subject of text</param>
        /// <param name="text">The text itself</param>
        public void InsertIntoMessage(string nameFrom, string nameTo, int accid, DateTime date, string subject, string text)
        {
            string typeCodeFrom = this.GenerateStringCodeFromEntity(nameFrom, accid);
            string typeCodeTo = this.GenerateStringCodeFromEntity(nameTo, accid);
            this.ot.Message.Add(
                new Message()
                {
                    Messageid = this.NextIndex("Message"),
                    Mfrom = typeCodeFrom,
                    Mto = typeCodeTo,
                    Mdate = date,
                    Msubject = subject,
                    Text = text
                });
            try
            {
                this.ot.SaveChanges();
            }
            catch (SystemException)
            {
            }
        }

        /// <summary>
        /// Return incoming messages
        /// </summary>
        /// <param name="idcode">Code generated from type and id</param>
        /// <returns>Return list of incoming messages</returns>
        public List<Message> ReturnIncamingMessagesById(string idcode)
        {
            List<Message> messages = new List<Message>();
            string typecode = idcode[0].ToString();
            int id = int.Parse(idcode.Substring(1));
            string accCode = string.Empty;
            if ((typecode == "u" && (bool)this.ReturnUserById(id).IsModerator) || typecode == "a")
            {
                if (typecode == "a")
                {
                    foreach (Accommodation acc in this.OwnedAccomondation(id))
                    {
                        accCode = "s" + acc.Accommodationid;
                        messages.AddRange((from msg in this.ot.Message
                                        where msg.Mto == accCode
                                        select msg).ToList<Message>());
                    }
                }
                else
                {
                    foreach (int i in this.ModeratedAccommodationIndexes(id))
                    {
                        accCode = "s" + i;
                        messages.AddRange((from msg in this.ot.Message
                                        where msg.Mto == accCode
                                        select msg).ToList<Message>());
                    }
                }
            }

            messages.AddRange((from msg in this.ot.Message
                            where msg.Mto == idcode
                            select msg).ToList<Message>());
            messages.OrderBy(x => x.Mdate);
            return messages;
        }

        /// <summary>
        /// Return outgoing messages
        /// </summary>
        /// <param name="idcode">Code generated from type and id</param>
        /// <returns>Return list of outgoing messages</returns>
        public List<Message> ReturnOutGoingMessagesById(string idcode)
        {
            List<Message> messages = new List<Message>();
            string typecode = idcode[0].ToString();
            int id = int.Parse(idcode.Substring(1));
            string accCode = string.Empty;
            if ((typecode == "u" && (bool)this.ReturnUserById(id).IsModerator) || typecode == "a")
            {
                if (typecode == "a")
                {
                    foreach (Accommodation acc in this.OwnedAccomondation(id))
                    {
                        accCode = "s" + acc.Accommodationid;
                        messages.AddRange((from msg in this.ot.Message
                                        where msg.Mfrom == accCode
                                        select msg).ToList<Message>());
                    }
                }
                else
                {
                    foreach (int i in this.ModeratedAccommodationIndexes(id))
                    {
                        accCode = "s" + i;
                        messages.AddRange((from msg in this.ot.Message
                                        where msg.Mfrom == accCode
                                        select msg).ToList<Message>());
                    }
                }
            }

            messages.AddRange((from msg in this.ot.Message
                            where msg.Mfrom == idcode
                            select msg).ToList<Message>());
            messages.OrderBy(x => x.Mdate);
            return messages;
        }

        /// <summary>
        /// Return accommodations where moderator is user with that id
        /// </summary>
        /// <param name="userid">id of the moderator</param>
        /// <returns>Return list of accommodations where moderator is user with that id</returns>
        public List<Accommodation> ModeratedAccommodation(int userid)
        {
            List<Accommodation> toReturn = new List<Accommodation>();
            foreach (int i in this.ModeratedAccommodationIndexes(userid))
            {
                toReturn.Add(this.ReturnAccommodationById(i));
            }

            return toReturn;
        }

        /// <summary>
        /// Return accommodations owned by advertiser with that id
        /// </summary>
        /// <param name="advid">id of the advertiser</param>
        /// <returns>Return list of accommodations owned by advertiser with that id</returns>
        public List<Accommodation> OwnedAccomondation(int advid)
        {
            var temp = from acc in this.ot.Accommodation
                       where acc.Advertiserid == advid
                       select acc;
            return temp.ToList();
        }

        /// <summary>
        /// Returns user by id
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns>User with correct id</returns>
        public Users ReturnUserById(int id)
        {
            try
            {
                return this.ot.Users.Single(x => x.Userid == id);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        /// <summary>
        /// Returns advertiser by id
        /// </summary>
        /// <param name="id">Id of the advertiser</param>
        /// <returns>Advertiser with correct id</returns>
        public Advertiser ReturnAdvertiserbyID(int id)
        {
            try
            {
                return this.ot.Advertiser.Single(x => x.Advertiserid == id);
            }
            catch (SystemException)
            {
                return null;
            }
        }

        /// <summary>
        /// Returns accommodation by id
        /// </summary>
        /// <param name="id">Id of the accommodation</param>
        /// <returns>Accomodation with correct id</returns>
        public Accommodation ReturnAccommodationById(int id)
        {
            try
            {
                return this.ot.Accommodation.Single(x => x.Accommodationid == id);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        /// <summary>
        /// Update user rating
        /// </summary>
        /// <param name="userid">Rated user id</param>
        /// <param name="accid">Rater accommodation id</param>
        /// <param name="newRating">New rating</param>
        public void UpdateUserRating(int userid, int accid, int newRating)
        {
            Users subject = this.ReturnUserById(userid);
            subject.Rating = ((subject.Rating * subject.Ratingnumber) + newRating) / (subject.Ratingnumber + 1);
            subject.Ratingnumber++;
            List<Reservation> reservations = this.ReturnReservationsByUseridAndAccid(userid, accid);
            foreach (Reservation res in reservations)
            {
                if (res.Status == Status.Completed.ToString())
                {
                    this.UpdateReservationStatus(res.Reservationid, Status.SzRated);
                }
                else if (res.Status == Status.URated.ToString())
                {
                    this.UpdateReservationStatus(res.Reservationid, Status.MRated);
                }
            }

            this.ot.SaveChanges();
        }

        /// <summary>
        /// Update accommodation rating
        /// </summary>
        /// <param name="userid">Rater user id</param>
        /// <param name="accid">Rated acccomodation id</param>
        /// <param name="newRating">New rating</param>
        public void UpdateAccRating(int userid, int accid, int newRating)
        {
            Accommodation subject = this.ReturnAccommodationById(accid);
                subject.Rating = ((subject.Rating * subject.Ratingnumber) + newRating) / (subject.Ratingnumber + 1);
                subject.Ratingnumber++;
                List<Reservation> reservations = this.ReturnReservationsByUseridAndAccid(userid, accid);
                foreach (Reservation res in reservations)
                {
                    if (res.Status == Status.Completed.ToString())
                    {
                        this.UpdateReservationStatus(res.Reservationid, Status.URated);
                    }
                    else if (res.Status == Status.SzRated.ToString())
                    {
                        this.UpdateReservationStatus(res.Reservationid, Status.MRated);
                    }
                }

                this.ot.SaveChanges();
        }

        /// <summary>
        /// Insert a new comment into Comment table
        /// </summary>
        /// <param name="userid">Commenter user id</param>
        /// <param name="accomondationid">Commented accommodation id</param>
        /// <param name="text">Text of the comment</param>
        public void InsertIntoComment(int userid, int accomondationid, string text)
        {
            this.ot.Comment.Add(new Comment
            {
                Commentid = this.NextIndex("Comment"),
                Userid = userid,
                Accommodationid = accomondationid,
                Commentdate = DateTime.Now,
                Text = text
            });
            this.ot.SaveChanges();
        }

        /// <summary>
        /// Insert new Modarator connection into Moderator table
        /// </summary>
        /// <param name="userid">Moderator user id</param>
        /// <param name="accid">Moderated acccommodation id</param>
        public void InsertIntoModerator(int userid, int accid)
        {
            this.ot.Moderator.Add(
                new Moderator()
                {
                    Moderatorid = this.NextIndex("Moderator"),
                    Userid = userid,
                    Accommodationid = accid
                });

            Users u = this.ReturnUserById(userid);
            if (!(bool)u.IsModerator)
            {
                u.IsModerator = true;
            }

            this.ot.SaveChanges();
        }

        /// <summary>
        /// Update reservation status
        /// </summary>
        /// <param name="resid">updated reservation id</param>
        /// <param name="endStatus">Status changed to</param>
        public void UpdateReservationStatus(int resid, Status endStatus)
        {
            Reservation res = this.ot.Reservation.Single(x => x.Reservationid == resid);
            Accommodation acctemp = this.ReturnAccommodationById((int)res.Accommodationid);
            Users utemp = this.ReturnUserById((int)res.Userid);
            res.Status = endStatus.ToString();
            if (endStatus == Status.Appcetted || endStatus == Status.Denied)
            {
                this.InsertIntoMessage(this.ReturnUserById(-1).Username, utemp.Username, -1, DateTime.Now, "Reservation status change", "Your reservation in the following place: " + acctemp.Name + " has changed to: " + endStatus.ToString());
            }

            if (endStatus == Status.Discarded)
            {
                this.InsertIntoMessage(this.ReturnUserById(-1).Username, acctemp.Name, (int)res.Accommodationid, DateTime.Now, "Reservation status change", "A reservation in the following place: " + acctemp.Name + "by the following user: " + utemp.Username + " has changed to: " + endStatus.ToString());
            }

            if (endStatus == Status.Deleted)
            {
                this.InsertIntoMessage(this.ReturnUserById(-1).Username, utemp.Username, -1, DateTime.Now, "Reservation status change", "Your reservation in the following place: " + acctemp.Name + " has changed to: " + endStatus.ToString() + " because deletion");
                this.InsertIntoMessage(this.ReturnUserById(-1).Username, acctemp.Name, (int)res.Accommodationid, DateTime.Now, "Reservation status change", "A reservation in the following place: " + acctemp.Name + " belonging to the following user : " + utemp.Username + " has changed to: " + endStatus.ToString() + " because deletion");
            }

            try
            {
                this.ot.SaveChanges();
            }
            catch (SystemException)
            {
            }
        }

        /// <summary>
        /// Returns reservtions by user id and accommodation id
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        /// <returns>Returns list of reservtions by user id and accommodation id</returns>
        public List<Reservation> ReturnReservationsByUseridAndAccid(int userid, int accid)
        {
            return (from res in this.ot.Reservation
             where res.Userid == userid && res.Accommodationid == accid
             select res).ToList();
        }

        /// <summary>
        /// Insert a new accommodation into the Accommodation table
        /// </summary>
        /// <param name="name">Name of the accommodation</param>
        /// <param name="address">Address of the accommodation</param>
        /// <param name="advid">Id of the Advertiser</param>
        /// <param name="picture">Picture of the accommodation</param>
        /// <param name="oneprice">One man room price</param>
        /// <param name="twoprice">Two man room price</param>
        /// <param name="threeprice">Three man room price</param>
        /// <param name="fourprice">Four man room price</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is the accommodation animal friendly</param>
        /// <param name="panorama">Is the panorama nice</param>
        /// <param name="pool">Is there a pool</param>
        public void InsertIntoAccomodation(string name, string address, int advid, string picture, int oneprice, int twoprice, int threeprice, int fourprice, bool wifi, bool animal, bool panorama, bool pool)
        {
            this.ot.Accommodation.Add(
                new Accommodation()
                {
                    Accommodationid = this.NextIndex("Accomondation"),
                    Name = name,
                    Address = address,
                    Advertiserid = advid,
                    Picture = picture,
                    OnePersonRoomPrice = oneprice,
                    TwoPersonRoomPrice = twoprice,
                    ThreePersonRoomPrice = threeprice,
                    FourPersonRoomPrice = fourprice,
                    Wifi = wifi,
                    Animal = animal,
                    Panorama = panorama,
                    Pool = pool,
                    Rating = 0,
                    Ratingnumber = 0
                });

            this.ot.SaveChanges();
        }

        /// <summary>
        /// Update accommodation data
        /// </summary>
        /// <param name="id">Id of the accommodation</param>
        /// <param name="name">Name of the accommodation</param>
        /// <param name="address">Address of the accommodation</param>
        /// <param name="picture">Picture of the accommodation</param>
        /// <param name="oneprice">One man room price</param>
        /// <param name="twoprice">Two man room price</param>
        /// <param name="threeprice">Three man room price</param>
        /// <param name="fourprice">Four man room price</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is the accommodation animal friendly</param>
        /// <param name="panorama">Is the panorama nice</param>
        /// <param name="pool">Is there a pool</param>
        public void UpdateAccommodation(int id, string name, string address, string picture, int oneprice, int twoprice, int threeprice, int fourprice, bool wifi, bool animal, bool panorama, bool pool)
        {
            Accommodation acc = this.ReturnAccommodationById(id);
            acc.Name = name;
            acc.Address = address;
            acc.Picture = picture;
            acc.OnePersonRoomPrice = oneprice;
            acc.TwoPersonRoomPrice = twoprice;
            acc.ThreePersonRoomPrice = threeprice;
            acc.FourPersonRoomPrice = fourprice;
            acc.Wifi = wifi;
            acc.Animal = animal;
            acc.Panorama = panorama;
            acc.Pool = pool;
            this.ot.SaveChanges();
        }

        /// <summary>
        /// Remove Accommodation
        /// </summary>
        /// <param name="id">Id of the accommodation</param>
        public void RemoveAccommodation(int id)
        {
            Accommodation toRemove = this.ReturnAccommodationById(id);
            this.RemoveAllModeratorbyAccId(id);
            this.RemoveCommentByAccId(id);
            this.RemoveAccIdFromReservation(id);
            this.RemoveIdFromMessage("s" + id);
            this.ot.Accommodation.Remove(toRemove);
            this.ot.SaveChanges();
        }

        /// <summary>
        /// Logical deletion of accommondation in Reservation table
        /// </summary>
        /// <param name="accid">Id of the accommondation</param>
        public void RemoveAccIdFromReservation(int accid)
        {
            foreach (Reservation res in this.ot.Reservation.ToList())
            {
                if (res.Accommodationid == accid)
                {
                    if (res.Status == Status.Unprocessed.ToString() || res.Status == Status.Appcetted.ToString())
                    {
                        this.UpdateReservationStatus(res.Reservationid, Status.Deleted);
                    }

                    res.Accommodationid = 0;
                }
            }

            this.ot.SaveChanges();
        }

        /// <summary>
        /// Logical deletion of user in Reservation table
        /// </summary>
        /// <param name="userid">Id of the user</param>
        public void RemoveUserIdFromReservation(int userid)
        {
            foreach (Reservation res in this.GetAllReservation())
            {
                if (res.Userid == userid)
                {
                    res.Userid = 0;
                    if (res.Status == Status.Unprocessed.ToString() || res.Status == Status.Appcetted.ToString())
                    {
                        this.UpdateReservationStatus(res.Reservationid, Status.Deleted);
                    }
                }
            }
        }

        /// <summary>
        /// Ligical deletion in Message table
        /// </summary>
        /// <param name="idcode">Code generated from type and id</param>
        public void RemoveIdFromMessage(string idcode)
        {
            foreach (Message msg in this.ot.Message.ToList())
            {
                if (msg.Mfrom == idcode)
                {
                    msg.Mfrom = "0";
                }

                if (msg.Mto == idcode)
                {
                    msg.Mto = "0";
                }
            }

            this.ot.SaveChanges();
        }

        /// <summary>
        /// Delete message
        /// </summary>
        /// <param name="id">Id of the message</param>
        public void RemoveMessage(int id)
        {
            Message e = this.ReturnMessagebyId(id);
            if (e != null)
            {
                this.ot.Message.Remove(this.ReturnMessagebyId(id));
                this.ot.SaveChanges();
            }
        }

        /// <summary>
        /// Return message by id
        /// </summary>
        /// <param name="id">Id of the message</param>
        /// <returns>Message with correct id</returns>
        public Message ReturnMessagebyId(int id)
        {
            try
            {
                return this.ot.Message.Single(x => x.Messageid == id);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        /// <summary>
        /// Logical deletion of user in Comment table
        /// </summary>
        /// <param name="id">Id of the user</param>
        public void RemoveUserIdFromComment(int id)
        {
            foreach (Comment cmt in this.GetAllComment())
            {
                if (cmt.Userid == id)
                {
                    cmt.Userid = 0;
                }
            }

            this.ot.SaveChanges();
        }

        /// <summary>
        /// Deletion of all comment for an accommodation
        /// </summary>
        /// <param name="id">Id of the comment</param>
        public void RemoveCommentByAccId(int id)
        {
            for (int i = 0; i < this.GetAllComment().Count(); i++)
            {
                if (this.GetAllComment().ElementAt(i).Accommodationid == id)
                {
                    this.ot.Comment.Remove(this.GetAllComment().ElementAt(i));
                }
            }

            this.ot.SaveChanges();
        }

        /// <summary>
        /// Delete comment by id
        /// </summary>
        /// <param name="id">Id of the comment</param>
        public void RemoveComment(int id)
        {
            Comment c = this.ReturnCommentbyID(id);
            if (c != null)
            {
                this.ot.Comment.Remove(c);
                this.ot.SaveChanges();
            }
        }

        /// <summary>
        /// Return comment by id
        /// </summary>
        /// <param name="id">Id of comment</param>
        /// <returns>Comment with correct id</returns>
        public Comment ReturnCommentbyID(int id)
        {
            try
            {
                return this.ot.Comment.Single(x => x.Commentid == id);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        /// <summary>
        /// Delete moderator record by user id
        /// </summary>
        /// <param name="userid">Id of the user</param>
        public void RemoveAllModeratorbyUserId(int userid)
        {
            Accommodation temp;
            Users user = this.ReturnUserById(userid);
            if (user != null)
            {
                for (int i = 0; i < this.ot.Moderator.Count(); i++)
                {
                    if (this.ot.Moderator.ToList().ElementAt(i).Userid == userid)
                    {
                        temp = this.ReturnAccommodationById(this.ot.Moderator.ToList().ElementAt(i).Accommodationid);
                        this.InsertIntoMessage(this.ReturnUserById(-1).Username, temp.Name, (int)temp.Accommodationid, DateTime.Now, "Moderator permisson returned", "In the following place: " + temp.Name + " the following user: " + user.Name + " moderation permisson has been returned");
                        this.ot.Moderator.Remove(this.ot.Moderator.ToList().ElementAt(i));
                        i--;
                    }
                }

                user.IsModerator = false;
                this.ot.SaveChanges();
            }
        }

        /// <summary>
        /// Delete moderator record by accommondation id
        /// </summary>
        /// <param name="accid"> Id of the accommodation</param>
        public void RemoveAllModeratorbyAccId(int accid)
        {
            Accommodation temp = this.ReturnAccommodationById(accid);
            for (int i = 0; i < this.ot.Moderator.Count(); i++)
            {
                if (this.ot.Moderator.ToList().ElementAt(i).Accommodationid == accid)
                {
                    this.InsertIntoMessage(this.ReturnUserById(-1).Username, this.ReturnUserById(this.ot.Moderator.ToList().ElementAt(i).Userid).Username, -1, DateTime.Now, "Moderator permisson revoked", "In the following place: " + temp.Name + " your moderation permisson has been revoked");
                    this.ot.Moderator.Remove(this.ot.Moderator.ToList().ElementAt(i));
                    if (this.ModeratedAccommodationIndexes(this.ot.Moderator.ToList().ElementAt(i).Userid).Count() == 0)
                    {
                        this.ReturnUserById(this.ot.Moderator.ToList().ElementAt(i).Userid).IsModerator = false;
                    }

                    i--;
                }
            }

            this.ot.SaveChanges();
        }

        /// <summary>
        /// Remove moderator connection by user id and accommodation id
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        public void RemoveModeratorByUserAndAccId(int userid, int accid)
        {
            for (int i = 0; i < this.ot.Moderator.Count(); i++)
            {
                Moderator m = this.ot.Moderator.ToList().ElementAt(i);
                if (m.Accommodationid == accid && m.Userid == userid)
                {
                    if (this.ModeratedAccommodationIndexes(m.Userid).Count() == 0)
                    {
                        this.ReturnUserById(m.Userid).IsModerator = false;
                    }

                    this.ot.Moderator.Remove(m);
                    i--;
                }
            }

            this.ot.SaveChanges();
        }

        /// <summary>
        /// Remove user
        /// </summary>
        /// <param name="id">Id of the user</param>
        public void RemoveUser(int id)
        {
            Users toRemove = this.ReturnUserById(id);
            if (toRemove != null)
            {
                this.RemoveUserIdFromComment(id);
                this.RemoveAllModeratorbyUserId(id);
                this.RemoveUserIdFromReservation(id);
                this.RemoveIdFromMessage("u" + id);
                this.ot.Users.Remove(toRemove); // !!!
                this.ot.SaveChanges();
            }
        }

        /// <summary>
        /// Remove advertiser
        /// </summary>
        /// <param name="id">Id of the advertiser</param>
        public void RemoveAdvertiser(int id)
        {
            Advertiser toRemove = this.ReturnAdvertiserbyID(id);
            if (toRemove != null)
            {
                for (int i = 0; i < this.GetAllAccommodation().Count(); i++)
                {
                    if (this.GetAllAccommodation().ElementAt(i).Advertiserid == id)
                    {
                        this.RemoveAccommodation(this.GetAllAccommodation().ElementAt(i).Accommodationid);
                        i--;
                    }
                }

                this.RemoveIdFromMessage("a" + id);
                this.ot.Advertiser.Remove(toRemove); // !!!
                this.ot.SaveChanges();
            }
        }

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns>List of all users</returns>
        public List<Users> GetAllUser()
        {
            List<Users> temp = this.ot.Users.ToList();
            temp.Remove(this.ReturnUserById(0));
            return temp;
        }

        /// <summary>
        /// Update user data
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="password">Password of the user</param>
        /// <param name="name">Name of the user</param>
        /// <param name="email">Email of the user</param>
        /// <param name="phone">Phone number of the user</param>
        /// <param name="address">Adress of the user</param>
        public void UpdateUser(int userid, string password, string name, string email, string phone, string address)
        {
            Users temp = this.ReturnUserById(userid);
            temp.Password = this.GenerateSHA2Hash(password);
            temp.Name = name;
            temp.Email = email;
            temp.Phonenumber = phone;
            temp.Address = address;
            this.ot.SaveChanges();
        }

        /// <summary>
        /// Update advertiser data
        /// </summary>
        /// <param name="advertiserid">Id of the advertiser</param>
        /// <param name="password">Password of the advertiser</param>
        /// <param name="name">Name of the advertiser</param>
        /// <param name="email">Email of the advertiser</param>
        /// <param name="phone">Phone number of the advertiser</param>
        /// <param name="address">Address of the advertiser</param>
        public void UpdateAdvertiser(int advertiserid, string password, string name, string email, string phone, string address)
        {
            Advertiser temp = this.ReturnAdvertiserbyID(advertiserid);
            temp.Password = this.GenerateSHA2Hash(password);
            temp.Name = name;
            temp.Email = email;
            temp.Phonenumber = phone;
            temp.Address = address;
            this.ot.SaveChanges();
        }

        /// <summary>
        /// Returns all accommodation
        /// </summary>
        /// <returns> List of all accommodation</returns>
        public List<Accommodation> GetAllAccommodation()
        {
            List<Accommodation> temp = this.ot.Accommodation.ToList();
            temp.Remove(this.ReturnAccommodationById(0));
            return temp;
        }

        /// <summary>
        /// Returns all advertiser
        /// </summary>
        /// <returns>List of all advertiser</returns>
        public List<Advertiser> GetAllAdvertiser()
        {
            List<Advertiser> temp = this.ot.Advertiser.ToList();
            temp.Remove(this.ReturnAdvertiserbyID(0));
            return temp;
        }

        /// <summary>
        /// Returns all reservation
        /// </summary>
        /// <returns>List of all reservation</returns>
        public List<Reservation> GetAllReservation()
        {
            return this.ot.Reservation.ToList();
        }

        /// <summary>
        /// Raturns all comment
        /// </summary>
        /// <returns>List of all comment</returns>
        public List<Comment> GetAllComment()
        {
            return this.ot.Comment.ToList();
        }

        /// <summary>
        /// Returns next index for tables
        /// </summary>
        /// <param name="typename">Name of tables</param>
        /// <returns>Next index</returns>
        private int NextIndex(string typename)
        {
            int maxIndex = 0;
            switch (typename)
            {
                case "Users": maxIndex = (int)this.ot.Users.Max(x => x.Userid); break;
                case "Advertiser": maxIndex = (int)this.ot.Advertiser.Max(x => x.Advertiserid); break;
                case "Reservation": maxIndex = (int)this.ot.Reservation.Max(x => x.Reservationid); break;
                case "Message": maxIndex = (int)this.ot.Message.Max(x => x.Messageid); break;
                case "Accomondation": maxIndex = this.ot.Accommodation.Max(x => x.Accommodationid); break;
                case "Moderator": maxIndex = (int)this.ot.Moderator.Max(x => x.Moderatorid); break;
                case "Comment": try
                                    {
                                        maxIndex = (int)this.ot.Comment.Max(x => x.Commentid);
                                    }
                                    catch (InvalidOperationException)
                                        {
                                        }

                                        break;
                default: maxIndex = 0; break;
            }

            maxIndex++;
            return maxIndex;
        }

        /// <summary>
        /// Generate code from username or accomodation
        /// </summary>
        /// <param name="name">Username or accommodation name</param>
        /// <param name="accid">Accommodation id for accommodations</param>
        /// <returns>Code from type and id</returns>
        private string GenerateStringCodeFromEntity(string name, int accid)
        {
            foreach (Users u in this.GetAllUser())
            {
                if (u.Username == name)
                {
                    return "u" + u.Userid;
                }
            }

            foreach (Advertiser a in this.GetAllAdvertiser())
            {
                if (a.Username == name)
                {
                    return "a" + a.Advertiserid;
                }
            }

            if (accid != -1)
            {
                foreach (Accommodation acc in this.GetAllAccommodation())
                {
                    if (acc.Accommodationid == accid && acc.Name == name)
                    {
                        return "s" + acc.Accommodationid;
                    }
                }
            }

            throw new Exception("Hibás felhasználónév");
        }

        /// <summary>
        /// Returns indexes of moderated accommodation of an user
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <returns>Returns list of indexes of moderated accommodation of an user</returns>
        private List<int> ModeratedAccommodationIndexes(int userid)
        {
            var temp = from mod in this.ot.Moderator
                       where mod.Userid == userid
                       select mod.Accommodationid;
            return temp.ToList<int>();
        }

        /// <summary>
        /// Generate SHA2 hash from a string
        /// </summary>
        /// <param name="input">String input for the method</param>
        /// <returns>Generated hash</returns>
        private string GenerateSHA2Hash(string input)
        {
            UnicodeEncoding uE = new UnicodeEncoding();
            byte[] hashValue;
            byte[] message = uE.GetBytes(input);
            SHA512Managed hashString = new SHA512Managed();
            string encodedData = Convert.ToBase64String(message);
            string hex = string.Empty;
            hashValue = hashString.ComputeHash(uE.GetBytes(encodedData));
            foreach (byte x in hashValue)
            {
                hex += string.Format("{0:X2}", x);
            }

            return hex;
        }
    }
}
