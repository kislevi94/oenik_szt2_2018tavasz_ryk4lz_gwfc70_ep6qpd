﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataLayer
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class OtEntities : DbContext
    {
        public OtEntities()
            : base("name=OtEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Accommodation> Accommodation { get; set; }
        public virtual DbSet<Advertiser> Advertiser { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<Moderator> Moderator { get; set; }
        public virtual DbSet<Reservation> Reservation { get; set; }
        public virtual DbSet<Users> Users { get; set; }
    }
}
