﻿// <copyright file="DataMessageICollectionToMessageICollection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using OgavirTours.Interfaces;

    public static class DataMessageICollectionToMessageICollection
    {
        /// <summary>
        /// /// <summary>
        /// Converts list of DataLayers' Message to UI Message ObservableCollection
        /// </summary>
        /// <param name="commentListIn">ICollection of DataLayer' Messages</param>
        /// <returns>UI Messages ObservableCollection</returns>
        /// </summary>
        /// <param name="messageListIn">ICollection of DataLayer' Messages</param>
        /// <param name="codeToName">Dictionary, from code to name</param>
        /// <param name="from">IUserEntity sender of the message</param>
        /// <returns>UI Messages ObservableCollection</returns>
        public static ObservableCollection<Util.Entities.Message> ToMessageObservableCollection(this List<DataLayer.Message> messageListIn, Dictionary<string, string> codeToName, IUserEntity from)
        {
            ObservableCollection<Util.Entities.Message> messageListOut = new ObservableCollection<Util.Entities.Message>();
            foreach (var msg in messageListIn)
            {
                messageListOut.Add(msg.ToMessage(codeToName, from));
            }

            return messageListOut;
        }

        /// <summary>
        /// /// <summary>
        /// Converts list of DataLayers' Message to UI Message ObservableCollection
        /// </summary>
        /// <param name="commentListIn">ICollection of DataLayer' Messages</param>
        /// <returns>UI Messages ObservableCollection</returns>
        /// </summary>
        /// <param name="messageListIn">ICollection of DataLayer' Messages</param>
        /// <param name="to">IUserEntity receiver of the message</param>
        /// <param name="codeToName">Dictionary, from code to name</param>
        /// <returns>UI Messages ObservableCollection</returns>
        public static ObservableCollection<Util.Entities.Message> ToMessageObservableCollection(this List<DataLayer.Message> messageListIn, IUserEntity to, Dictionary<string, string> codeToName)
        {
            ObservableCollection<Util.Entities.Message> messageListOut = new ObservableCollection<Util.Entities.Message>();
            foreach (var msg in messageListIn)
            {
                messageListOut.Add(msg.ToMessage(to, codeToName));
            }

            return messageListOut;
        }
    }
}
