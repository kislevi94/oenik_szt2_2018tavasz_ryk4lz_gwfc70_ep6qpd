﻿// <copyright file="DataReservationICollectionToReservationICollection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Converts list of DataLayers' Reservation to UI Reservation ICollection
    /// </summary>
    public static class DataReservationICollectionToReservationICollection
    {
        /// <summary>
        /// Converts list of DataLayers' Reservation to UI Reservation ObservableCollection
        /// </summary>
        /// <param name="reservationListIn">List of DataLayer' Reservations</param>
        /// <returns>UI Reservation ObservableCollection</returns>
        public static ObservableCollection<Util.Entities.Reservation> ToReservationObservableCollection(this List<DataLayer.Reservation> reservationListIn)
        {
            ObservableCollection<Util.Entities.Reservation> reservationListOut = new ObservableCollection<Util.Entities.Reservation>();
            foreach (var reservation in reservationListIn)
            {
                reservationListOut.Add(reservation.ToReservation());
            }

            return reservationListOut;
        }

        /// <summary>
        /// Converts list of DataLayers' Reservation to UI Reservation List
        /// </summary>
        /// <param name="reservationListIn">List of DataLayer' Reservations</param>
        /// <returns>UI Reservation ObservableCollection</returns>
        public static List<Util.Entities.Reservation> ToReservationList(this List<DataLayer.Reservation> reservationListIn)
        {
            List<Util.Entities.Reservation> reservationListOut = new List<Util.Entities.Reservation>();
            foreach (var reservation in reservationListIn)
            {
                reservationListOut.Add(reservation.ToReservation());
            }

            return reservationListOut;
        }
    }
}
