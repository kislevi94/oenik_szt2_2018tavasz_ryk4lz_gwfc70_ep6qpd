﻿// <copyright file="DataMessageToMessageConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using System;
    using System.Collections.Generic;
    using OgavirTours.Interfaces;

    public static class DataMessageToMessageConverter
    {
        /// <summary>
        /// Converts DataLayers' Message to UI Message
        /// </summary>
        /// <param name="messageIn">Instance of DataLayer' Messages</param>
        /// <param name="codeToName">Dictionary, from code to name</param>
        /// <param name="from">IUserEntity sender of the message</param>
        /// <returns>UI Messages</returns>
        public static Util.Entities.Message ToMessage(this DataLayer.Message messageIn, Dictionary<string, string> codeToName, IUserEntity from)
        {
            Util.Entities.Message messageOut = new Util.Entities.Message()
            {
                MessageId = messageIn.Messageid,
                From = from.Name,
                To = codeToName[messageIn.Mto.ToLower()],
                Date = messageIn.Mdate == null ? DateTime.MinValue : (DateTime)messageIn.Mdate,
                Subject = messageIn.Msubject,
                Text = messageIn.Text
            };
            return messageOut;
        }

        /// <summary>
        /// Converts DataLayers' Message to UI Message
        /// </summary
        /// <param name="messageIn">Instance of DataLayer' Messages</param>
        /// <param name="to">IUserEntity receiver of the message</param>
        /// <param name="codeToName">Dictionary, from code to name</param>
        /// <returns>UI Message</returns>
        public static Util.Entities.Message ToMessage(this DataLayer.Message messageIn, IUserEntity to, Dictionary<string, string> codeToName)
        {
            Util.Entities.Message messageOut = new Util.Entities.Message()
            {
                MessageId = messageIn.Messageid,
                From = codeToName[messageIn.Mfrom.ToLower()],
                To = to.Name,
                Date = messageIn.Mdate == null ? DateTime.MinValue : (DateTime)messageIn.Mdate,
                Subject = messageIn.Msubject,
                Text = messageIn.Text
            };
            return messageOut;
        }
    }
}
