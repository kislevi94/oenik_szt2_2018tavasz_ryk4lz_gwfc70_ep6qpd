﻿// <copyright file="DataReservationToReservationConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using System;

    /// <summary>
    /// Converts DataLayers' Reservation to UI Reservation
    /// </summary>
    public static class DataReservationToReservationConverter
    {
        /// <summary>
        /// Converts DataLayers' Reservation to UI Reservation
        /// </summary>
        /// <param name="resIn">Instance of DataLayer' Reservation</param>
        /// <returns>UI Reservation</returns>
        public static Util.Entities.Reservation ToReservation(this DataLayer.Reservation resIn)
        {
            Util.Entities.Reservation resOut = new Util.Entities.Reservation()
            {
                ReservationId = resIn.Reservationid,
                UserId = resIn.Userid,
                AccommodationId = resIn.Accommodationid,
                RoomType = resIn.Roomtype == null ? 0 : (int)resIn.Roomtype,
                Price = resIn.Price == null ? 0 : (int)resIn.Price,
                StartDate = resIn.Startdate == null ? DateTime.MinValue : (DateTime)resIn.Startdate,
                ReservationDate = resIn.Reservationdate == null ? DateTime.MinValue : (DateTime)resIn.Reservationdate,
                Length = resIn.Length == null ? 0 : (int)resIn.Length,
                Status = resIn.Status,
                User = resIn.Users.ToUser()
            };

            return resOut;
        }
    }
}
