﻿// <copyright file="DataAccommodationToAccommodationConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using System;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Converts DataLayers' Accommodation to UI Accommodation
    /// </summary>
    public static class DataAccommodationToAccommodationConverter
    {
        /// <summary>
        /// Converts DataLayers' Accommodation to UI Accommodation
        /// </summary>
        /// <param name="accIn">Instance of DataLayer' Accommodation</param>
        /// <returns>UI Accommodation</returns>
        public static Util.Entities.Accommodation ToAccommodation(this DataLayer.Accommodation accIn)
        {
            Util.Entities.Accommodation accOut = new Util.Entities.Accommodation()
            {
                Accommodationid = accIn.Accommodationid,
                Name = accIn.Name,
                Address = accIn.Address,
                Advertiserid = accIn.Advertiserid,
                Image = accIn.Picture == null || accIn.Picture == "pack://application:,,,/Images/temp.JPG" ? new BitmapImage(new Uri("pack://application:,,,/Images/temp.JPG")) : new BitmapImage(new Uri(@"/Images/" + accIn.Picture, UriKind.Relative)),
                OnePersonRoomPrice = accIn.OnePersonRoomPrice == null ? 0 : (int)accIn.OnePersonRoomPrice,
                TwoPersonRoomPrice = accIn.TwoPersonRoomPrice == null ? 0 : (int)accIn.TwoPersonRoomPrice,
                ThreePersonRoomPrice = accIn.ThreePersonRoomPrice == null ? 0 : (int)accIn.ThreePersonRoomPrice,
                FourPersonRoomPrice = accIn.FourPersonRoomPrice == null ? 0 : (int)accIn.FourPersonRoomPrice,
                Wifi = accIn.Wifi == null ? false : (bool)accIn.Wifi,
                PetFriendly = accIn.Animal == null ? false : (bool)accIn.Animal,
                LuggageRoom = accIn.Panorama == null ? false : (bool)accIn.Panorama,
                Pool = accIn.Pool == null ? false : (bool)accIn.Pool,
                Rating = accIn.Rating == null ? 0 : (double)accIn.Rating,
                RatingNumber = accIn.Ratingnumber == null ? 0 : (int)accIn.Ratingnumber,
                Advertiser = accIn.Advertiser.ToAdvertiser()
            };
            foreach (var comment in accIn.Comment)
            {
                accOut.Comments.Add(comment.ToComment());
            }

            foreach (var mod in accIn.Moderator)
            {
                accOut.Moderator.Add(mod.ToModerator());
            }

            foreach (var res in accIn.Reservation)
            {
                accOut.Reservation.Add(res.ToReservation());
            }

            return accOut;
        }
    }
}
