﻿// <copyright file="DataAdvertiserToAdvertiserConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    /// <summary>
    /// Converts DataLayers' Advertiser to UI Advertiser
    /// </summary>
    public static class DataAdvertiserToAdvertiserConverter
    {
        /// <summary>
        /// Converts DataLayers' Advertiser to UI Advertiser
        /// </summary>
        /// <param name="advIn">Instance of DataLayer' Advertiser</param>
        /// <returns>UI Advertiser</returns>
        public static Util.Entities.Advertiser ToAdvertiser(this DataLayer.Advertiser advIn)
        {
            Util.Entities.Advertiser advOut = new Util.Entities.Advertiser()
            {
                Advertiserid = advIn.Advertiserid,
                Username = advIn.Username,
                Password = advIn.Password,
                Name = advIn.Name,
                Email = advIn.Email,
                PhoneNumber = advIn.Phonenumber,
                Address = advIn.Address
            };
            return advOut;
        }
    }
}
