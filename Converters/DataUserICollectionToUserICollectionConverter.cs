﻿// <copyright file="DataUserICollectionToUserICollectionConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Converts list of DataLayers' Users to UI User ICollection
    /// </summary>
    public static class DataUserICollectionToUserICollectionConverter
    {
        /// <summary>
        /// Converts list of DataLayers' Users to UI User ObservableCollection
        /// </summary>
        /// <param name="userListIn">List of DataLayer' Users</param>
        /// <returns>UI User ObservableCollection</returns>
        public static ObservableCollection<Util.Entities.User> ToUserObservableCollection(this List<DataLayer.Users> userListIn)
        {
            ObservableCollection<Util.Entities.User> userListOut = new ObservableCollection<Util.Entities.User>();
            foreach (var user in userListIn)
            {
                userListOut.Add(user.ToUser());
            }

            return userListOut;
        }

        /// <summary>
        /// Converts list of DataLayers' Users to UI User List
        /// </summary>
        /// <param name="userListIn">List of DataLayer' Users</param>
        /// <returns>UI User List</returns>
        public static List<Util.Entities.User> ToUserList(this List<DataLayer.Users> userListIn)
        {
            List<Util.Entities.User> userListOut = new List<Util.Entities.User>();
            foreach (var user in userListIn)
            {
                userListOut.Add(user.ToUser());
            }

            return userListOut;
        }
    }
}
