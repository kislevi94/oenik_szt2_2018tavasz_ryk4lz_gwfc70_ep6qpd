﻿// <copyright file="DataUserToUserConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using DataLayer;
    using OgavirTours.Util.Entities;

    /// <summary>
    /// Converts DataLayers' User to UI User
    /// </summary>
    public static class DataUserToUserConverter
    {
        /// <summary>
        /// Converts DataLayers' User to UI User
        /// </summary>
        /// <param name="userin">Instance of DataLayer' User</param>
        /// <returns>UI User</returns>
        public static User ToUser(this Users userin)
        {
            User userout = new User()
            {
                Address = userin.Address,
                Email = userin.Email,
                IsModerator = (bool)userin.IsModerator,
                Name = userin.Name,
                UserId = userin.Userid,
                PhoneNumber = userin.Phonenumber,
                Rating = userin.Rating == null ? 0 : (double)userin.Rating,
                Ratingnumber = userin.Ratingnumber == null ? 0 : (int)userin.Ratingnumber,
                Username = userin.Username
            };

            return userout;
        }
    }
}
