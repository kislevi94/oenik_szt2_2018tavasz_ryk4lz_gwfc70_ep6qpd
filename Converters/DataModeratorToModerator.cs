﻿// <copyright file="DataModeratorToModerator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    /// <summary>
    /// Converts DataLayers' Moderator to UI Moderator
    /// </summary>
    public static class DataModeratorToModerator
    {
        /// <summary>
        /// Converts DataLayers' Moderator to UI Moderator
        /// </summary>
        /// <param name="modIn">Instance of DataLayer' Moderator</param>
        /// <returns>UI Moderator</returns>
        public static Util.Entities.Moderator ToModerator(this DataLayer.Moderator modIn)
        {
            Util.Entities.Moderator modOut = new Util.Entities.Moderator()
            {
                ModeratorId = modIn.Moderatorid,
                UserId = modIn.Userid,
                Accommodationid = modIn.Accommodationid,
                User = modIn.Users.ToUser()
            };
            return modOut;
        }
    }
}
