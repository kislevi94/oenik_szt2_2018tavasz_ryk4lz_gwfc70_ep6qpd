﻿// <copyright file="DataCommentToCommentConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using System;

    /// <summary>
    /// Converts DataLayers' Comment to UI Comment
    /// </summary>
    public static class DataCommentToCommentConverter
    {
        /// <summary>
        /// Converts DataLayers' Comment to UI Comment
        /// </summary>
        /// <param name="commentIn">Instance of DataLayer' Comment</param>
        /// <returns>UI Comment</returns>
        public static Util.Entities.Comment ToComment(this DataLayer.Comment commentIn)
        {
            Util.Entities.Comment commentOut = new Util.Entities.Comment()
            {
                Commentid = commentIn.Commentid,
                Userid = commentIn.Userid,
                Accommodationid = commentIn.Accommodationid,
                Commentdate = commentIn.Commentdate == null ? DateTime.MinValue : (DateTime)commentIn.Commentdate,
                Text = commentIn.Text
            };
            return commentOut;
        }
    }
}
