﻿// <copyright file="DataCommentICollectionToCommentICollection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Converts list of DataLayers' Comments to UI Comments ICollection
    /// </summary>
    public static class DataCommentICollectionToCommentICollection
    {
        /// <summary>
        /// Converts list of DataLayers' Comments to UI Comments ObservableCollection
        /// </summary>
        /// <param name="commentListIn">List of DataLayer' Comments</param>
        /// <returns>UI Comment ObservableCollection</returns>
        public static ObservableCollection<Util.Entities.Comment> ToCommentObservableCollection(this ICollection<DataLayer.Comment> commentListIn)
        {
            ObservableCollection<Util.Entities.Comment> commentListOut = new ObservableCollection<Util.Entities.Comment>();
            foreach (var c in commentListIn)
            {
                commentListOut.Add(c.ToComment());
            }

            return commentListOut;
        }

        /// <summary>
        /// Converts ICollection of UIs' Comments to UI Comments ObservableCollection
        /// </summary>
        /// <param name="commentListIn">List of DataLayer' Comments</param>
        /// <returns>UI Comment ObservableCollection</returns>
        public static ObservableCollection<Util.Entities.Comment> ToCommentObservableCollection(this ICollection<Util.Entities.Comment> commentListIn)
        {
            ObservableCollection<Util.Entities.Comment> commentListOut = new ObservableCollection<Util.Entities.Comment>();
            foreach (var c in commentListIn)
            {
                commentListOut.Add(c);
            }

            return commentListOut;
        }

        /// <summary>
        /// Converts list of DataLayers' Comments to UI Comments List
        /// </summary>
        /// <param name="commentListIn">ICollection of DataLayer' Comments</param>
        /// <returns>UI Comment List</returns>
        public static List<Util.Entities.Comment> ToCommentList(this ICollection<DataLayer.Comment> commentListIn)
        {
            List<Util.Entities.Comment> commentListOut = new List<Util.Entities.Comment>();
            foreach (var c in commentListIn)
            {
               commentListOut.Add(c.ToComment());
            }

            return commentListOut;
        }
    }
}
