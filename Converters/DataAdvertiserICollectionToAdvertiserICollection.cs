﻿// <copyright file="DataAdvertiserICollectionToAdvertiserICollection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Converts list of DataLayers' Advertisers to UI Advertisers ICollection
    /// </summary>
    public static class DataAdvertiserICollectionToAdvertiserICollection
    {
        /// <summary>
        /// Converts list of DataLayers' Advertisers to UI Advertisers ObservableCollection
        /// </summary>
        /// <param name="advertiserListIn">List of DataLayer' Advertisers</param>
        /// <returns>UI Advertiser ObservableCollection</returns>
        public static ObservableCollection<Util.Entities.Advertiser> ToAdvertiserObservableCollection(this List<DataLayer.Advertiser> advertiserListIn)
        {
            ObservableCollection<Util.Entities.Advertiser> advertiserListOut = new ObservableCollection<Util.Entities.Advertiser>();
            foreach (var advertiser in advertiserListIn)
            {
                advertiserListOut.Add(advertiser.ToAdvertiser());
            }

            return advertiserListOut;
        }

        /// <summary>
        /// Converts list of DataLayers' Advertisers to UI Advertisers List
        /// </summary>
        /// <param name="advertiserListIn">List of DataLayer' Advertisers</param>
        /// <returns>UI Advertiser List</returns>
        public static List<Util.Entities.Advertiser> ToAdvertiserList(this List<DataLayer.Advertiser> advertiserListIn)
        {
            List<Util.Entities.Advertiser> advertiserListOut = new List<Util.Entities.Advertiser>();
            foreach (var advertiser in advertiserListIn)
            {
                advertiserListOut.Add(advertiser.ToAdvertiser());
            }

            return advertiserListOut;
        }
    }
}
