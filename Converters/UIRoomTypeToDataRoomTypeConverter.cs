﻿// <copyright file="UIRoomTypeToDataRoomTypeConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    public static class UIRoomTypeToDataRoomTypeConverter
    {
        /// <summary>
        /// Converts UIs' roomtype text toDataLayers' RoomType
        /// </summary>
        /// <param name="roomtypein">Roomtype text</param>
        /// <returns>DataLayer roomtype</returns>
        public static int ToDataRoomType(this string roomtypein)
        {
            int roomtypeout = 0;
            switch (roomtypein)
            {
                case "one bed":
                    roomtypeout = 1;
                    break;
                case "double bed":
                    roomtypeout = 2;
                    break;
                case "three bed (no double bed)":
                    roomtypeout = 3;
                    break;
                case "four bed (two double beds)":
                    roomtypeout = 4;
                    break;
                default:
                    break;
            }

            return roomtypeout;
        }
    }
}
