﻿// <copyright file="DataAccommodationICollectionToAccommodationICollection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Converters
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using OgavirTours.Util.Entities;

    /// <summary>
    /// Extension methods, converts lists of DataLayers' Accommodations to UI Accommodation ICollections
    /// </summary>
    public static class DataAccommodationICollectionToAccommodationICollection
    {
        /// <summary>
        /// Converts list of DataLayers' Accommodations to UI Accommodation ObservableCollection
        /// </summary>
        /// <param name="accListIn">List of DataLayer' Accommodations</param>
        /// <returns>UI Accommodation ObservableCollection</returns>
        public static ObservableCollection<Util.Entities.Accommodation> ToAccommodationObservableCollection(this List<DataLayer.Accommodation> accListIn)
        {
            ObservableCollection<Util.Entities.Accommodation> accListOut = new ObservableCollection<Accommodation>();
            foreach (var acc in accListIn)
            {
                accListOut.Add(acc.ToAccommodation());
            }

            return accListOut;
        }

        /// <summary>
        /// Converts list of DataLayers' Accommodations to UI Accommodation List
        /// </summary>
        /// <param name="accListIn">List of DataLayer' Accommodations</param>
        /// <returns>UI Accommodation List</returns>
        public static List<Util.Entities.Accommodation> ToAccommodationList(this List<DataLayer.Accommodation> accListIn)
        {
            List<Util.Entities.Accommodation> accListOut = new List<Accommodation>();
            foreach (var acc in accListIn)
            {
                accListOut.Add(acc.ToAccommodation());
            }

            return accListOut;
        }
    }
}
