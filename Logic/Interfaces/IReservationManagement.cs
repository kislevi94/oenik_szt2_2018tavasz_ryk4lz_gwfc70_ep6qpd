﻿// <copyright file="IReservationManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using DataLayer;

    /// <summary>
    /// Management interface
    /// </summary>
    public interface IReservationManagement
    {
        IData Data { get; }

        /// <summary>
        /// Make reservation
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accomondationid">Id of the accommodation</param>
        /// <param name="roomtype">Number of people for the room</param>
        /// <param name="startDate">Reservation start time</param>
        /// <param name="length">Number of days</param>
        void MakeReservation(int userid, int accomondationid, int roomtype, DateTime startDate, int length);

        /// <summary>
        /// Update reservation status
        /// </summary>
        /// <param name="resid">Id of the reservation</param>
        /// <param name="endStatus">Status changed to</param>
        void UpdateReservationStatus(int resid, Status endStatus);

        /// <summary>
        /// Lists reservation by user
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <returns>List of reservations</returns>
        List<Reservation> ListUserReservation(int userid);

        /// <summary>
        /// List reservations by accommodation
        /// </summary>
        /// <param name="accid">Id of the accommodation</param>
        /// <returns>list of reservations</returns>
        List<Reservation> ListAccommodationReservation(int accid);

        /// <summary>
        /// Lists reservations by accommodation and status
        /// </summary>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="s">Status of the reservation</param>
        /// <returns>List of reservations</returns>
        List<Reservation> ListAccommodationReservationByStatus(int accid, Status s);

        /// <summary>
        /// List reservation by user and status
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="s">Status of the reservation</param>
        /// <returns>List of reservations</returns>
        List<Reservation> ListUserReservationByStatus(int userid, Status s);
    }
}
