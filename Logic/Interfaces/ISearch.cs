﻿// <copyright file="ISearch.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using DataLayer;

    /// <summary>
    /// Search interface
    /// </summary>
    public interface ISearch
    {
        /// <summary>
        /// Short search
        /// </summary>
        /// <param name="searchText">Text for search</param>
        /// <returns>Liat of accommodations with correct data</returns>
        List<Accommodation> ShortSearch(string searchText);

        /// <summary>
        /// Long search
        /// </summary>
        /// <param name="location">Search location</param>
        /// <param name="name">Location name</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is animal friendly</param>
        /// <param name="pool">Is there pool</param>
        /// <param name="panorama">is there good panorama</param>
        /// <returns>List of accommodations with correct data</returns>
        List<Accommodation> LongSearch(string location, string name, bool wifi, bool animal, bool pool, bool panorama);
    }
}
