﻿// <copyright file="ICommentManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using DataLayer;

    /// <summary>
    /// Management interface
    /// </summary>
    public interface ICommentManagement
    {
        IData Data { get; }

        /// <summary>
        /// Make a new comment
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="text">Text of the comment</param>
        void MakeComment(int userid, int accid, string text);

        /// <summary>
        /// Lists comments of an accommodation
        /// </summary>
        /// <param name="accid">Id of the accomodation</param>
        /// <returns>List of comment</returns>
        List<Comment> GetComments(int accid);
    }
}
