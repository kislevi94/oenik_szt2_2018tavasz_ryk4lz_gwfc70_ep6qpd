﻿// <copyright file="IMessageManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using DataLayer;

    /// <summary>
    /// Management interface
    /// </summary>
    public interface IMessageManagement
    {
        IData Data { get; }

        /// <summary>
        /// Send message
        /// </summary>
        /// <param name="nameFrom">Name of sender</param>
        /// <param name="nameTo">Name of receiver</param>
        /// <param name="accid">If of accommodation</param>
        /// <param name="subject">Subject of the message</param>
        /// <param name="text">Text of the message</param>
        void SendMessage(string nameFrom, string nameTo, int accid, string subject, string text);

        /// <summary>
        /// Return incoming message
        /// </summary>
        /// <param name="typeCode">Type code</param>
        /// <param name="id">Id of the user</param>
        /// <returns>List of incoming messages</returns>
        List<Message> GetIncomingMessages(string typeCode, int id);

        /// <summary>
        /// Return outgoing message
        /// </summary>
        /// <param name="typeCode">Type code</param>
        /// <param name="id">Id of the user</param>
        /// <returns>List of outgoing messages</returns>
        List<Message> GetOutgoingMessages(string typeCode, int id);
    }
}
