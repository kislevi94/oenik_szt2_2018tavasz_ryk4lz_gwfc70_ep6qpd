﻿// <copyright file="IUserAndAdministratorManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using DataLayer;

    /// <summary>
    /// Management interface
    /// </summary>
    public interface IUserAndAdministratorManagement
    {
        IData Data { get; }

        /// <summary>
        /// Returns bool value whether the user can rate the accommodation
        /// </summary>
        /// <param name="isUser">Is user</param>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        /// <returns>Whether the user can rate the accommodation</returns>
        bool IsAbleToRate(bool isUser, int userid, int accid);

        /// <summary>
        /// Rate user
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="newRating">New rating</param>
        void UpdateUserRating(int userid, int accid, int newRating);

        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="userid">Id of the user</param>
        void DeleteUser(int userid);

        /// <summary>
        /// Delete Advertiser
        /// </summary>
        /// <param name="advid">Id of the advertiser</param>
        void DeleteAdvertiser(int advid);

        /// <summary>
        /// Update user data
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <param name="password">Password of the user</param>
        /// <param name="name">Name of the user</param>
        /// <param name="email">Email of the user</param>
        /// <param name="phone">Phone number of the user</param>
        /// <param name="address">Adress of the user</param>
        void ModifyUser(int id, string password, string name, string email, string phone, string address);

        /// <summary>
        /// Update advertiser data
        /// </summary>
        /// <param name="advertiserid">Id of the advertiser</param>
        /// <param name="password">Password of the advertiser</param>
        /// <param name="name">Name of the advertiser</param>
        /// <param name="email">Email of the advertiser</param>
        /// <param name="phone">Phone number of the advertiser</param>
        /// <param name="address">Address of the advertiser</param>
        void ModifyAdvertiser(int advertiserid, string password, string name, string email, string phone, string address);

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns>List of all users</returns>
        List<Users> ListUsers();

        /// <summary>
        /// Returns all advertiser
        /// </summary>
        /// <returns>List of all advertiser</returns>
        List<Advertiser> ListAdvertiser();

        /// <summary>
        /// Returns user by id
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns>User with correct id</returns>
        Users GetUserById(int id);

        /// <summary>
        /// Returns advertiser by id
        /// </summary>
        /// <param name="id">Id of the advertiser</param>
        /// <returns>Advertiser with correct id</returns>
        Advertiser GetAdvertiserById(int id);

    }
}
