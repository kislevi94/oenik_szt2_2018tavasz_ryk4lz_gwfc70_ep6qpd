﻿// <copyright file="IAccommodationManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using DataLayer;

    /// <summary>
    /// Management interface
    /// </summary>
    public interface IAccommodationManagement
    {
        IData Data { get; }

        /// <summary>
        /// Update accommodation rating
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="newRating">New rating</param>
        void UpdateAccomodationreting(int userid, int accid, int newRating);

        /// <summary>
        /// Register accommodation
        /// </summary>
        /// <param name="name">Name of the accommodation</param>
        /// <param name="address">Address of the accommodation</param>
        /// <param name="advid">Id of the advertiser</param>
        /// <param name="picture">Picture of the accommodation</param>
        /// <param name="oneprice">One man room price</param>
        /// <param name="twoprice">Two man room price</param>
        /// <param name="threeprice">Three man room price</param>
        /// <param name="fourprice">Four man room price</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is the accommodation animal friendly</param>
        /// <param name="panorama">Is the panorama nice</param>
        /// <param name="pool">Is there a pool</param>
        void RegisterAccommodation(string name, string address, int advid, string picture, int oneprice, int twoprice, int threeprice, int fourprice, bool wifi, bool animal, bool panorama, bool pool);

        /// <summary>
        /// Update accommodation
        /// </summary>
        /// <param name="id">Id of the accommodation</param>
        /// <param name="name">Name of the accommodation</param>
        /// <param name="address">Address of the accommodation</param>
        /// <param name="picture">Picture of the accommodation</param>
        /// <param name="oneprice">One man room price</param>
        /// <param name="twoprice">Two man room price</param>
        /// <param name="threeprice">Three man room price</param>
        /// <param name="fourprice">Four man room price</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is the accommodation animal friendly</param>
        /// <param name="panorama">Is the panorama nice</param>
        /// <param name="pool">Is there a pool</param>
        void ModifyAccommodation(int id, string name, string address, string picture, int oneprice, int twoprice, int threeprice, int fourprice, bool wifi, bool animal, bool panorama, bool pool);

        /// <summary>
        /// Register new moderator connection
        /// </summary>
        /// <param name="userid">Id of the moderator</param>
        /// <param name="accid">Id of the accommodation</param>
        void AddModeratorConnetion(int userid, int accid);

        /// <summary>
        /// Delete accommodation
        /// </summary>
        /// <param name="accid">Id of the accommodation</param>
        void DeleteAccomondation(int accid);

        /// <summary>
        /// Remove moderator connection
        /// </summary>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="userid">Id of the user</param>
        void RemoveModerator(int accid, int userid);

        /// <summary>
        /// Lists all accommodation
        /// </summary>
        /// <returns>List of all accommodation</returns>
        List<Accommodation> ListAccommodation();

        /// <summary>
        /// Lists best rated accommodations
        /// </summary>
        /// <param name="count">Number of listed accommodation</param>
        /// <returns>List of best rated accommodation</returns>
        List<Accommodation> ListBestRatedAccommodation(int count);

        /// <summary>
        /// Lists accommodations owned by advertiser
        /// </summary>
        /// <param name="advid">Id of the advertiser</param>
        /// <returns>List of accommodation</returns>
        List<Accommodation> ListAdvertiserOwnedAccommodation(int advid);

        /// <summary>
        /// Lists accommodations moderated by same user
        /// </summary>
        /// <param name="userid">Id of the moderator</param>
        /// <returns>List of accommodation</returns>
        List<Accommodation> ListUserModeratedAccomodation(int userid);
    }
}
