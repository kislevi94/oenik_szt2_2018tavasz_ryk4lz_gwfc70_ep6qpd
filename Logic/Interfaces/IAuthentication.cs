﻿// <copyright file="IAuthentication.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using DataLayer;

    /// <summary>
    /// Authentication interface
    /// </summary>
    public interface IAuthentication
    {
        /// <summary>
        /// Registrate an user
        /// </summary>
        /// <param name="isAdv">Is advertiser</param>
        /// <param name="username">Username</param>
        /// <param name="password">Passord</param>
        /// <param name="name">Name</param>
        /// <param name="email">Email</param>
        /// <param name="phone">Phone</param>
        /// <param name="addresscode">Address code</param>
        /// <param name="city">City</param>
        /// <param name="address">Rest of address</param>
        void Registration(bool isAdv, string username, string password, string name, string email, string phone, string addresscode, string city, string address);

        /// <summary>
        /// Authenticate an user
        /// </summary>
        /// <param name="username">Username of the user</param>
        /// <param name="password">Password of the user</param>
        /// <returns>Authenticated user</returns>
        Users UserAuthentication(string username, string password);

        /// <summary>
        /// Authenticate an advertiser
        /// </summary>
        /// <param name="username">Username of the advetiser</param>
        /// <param name="password">Password of the advertiser</param>
        /// <returns>Authenticated advertiser</returns>
        Advertiser AdvertiserAuthentication(string username, string password);
    }
}
