﻿// <copyright file="Search.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using DataLayer;

    /// <summary>
    /// Search methods
    /// </summary>
    public class Search : ISearch
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Search"/> class.
        /// </summary>
        /// <param name="data">DataLayer connection</param>
        public Search(IData data)
        {
            this.Data = data;
        }

        private IData Data { get; }

        /// <summary>
        /// Short search
        /// </summary>
        /// <param name="searchText">Text for search</param>
        /// <returns>Liat of accommodations with correct data</returns>
        public List<Accommodation> ShortSearch(string searchText)
        {
            List<Accommodation> toReturn = new List<Accommodation>();
            List<Accommodation> temploc = new List<Accommodation>();
            foreach (Accommodation acc in this.Data.GetAllAccommodation())
            {
                if (acc.Name == searchText)
                {
                    toReturn.Add(acc);
                }
                else if (this.GetCityFromAddress(acc.Address) == searchText)
                {
                    temploc.Add(acc);
                }
            }

            toReturn = toReturn.OrderByDescending(x => x.Rating).ToList();
            temploc = temploc.OrderByDescending(x => x.Rating).ToList();
            toReturn.AddRange(temploc);
            return toReturn;
        }

        /// <summary>
        /// Long search
        /// </summary>
        /// <param name="location">Search location</param>
        /// <param name="name">Location name</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is animal friendly</param>
        /// <param name="pool">Is there pool</param>
        /// <param name="panorama">is there good panorama</param>
        /// <returns>List of accommodations with correct data</returns>
        public List<Accommodation> LongSearch(string location, string name, bool wifi, bool animal, bool pool, bool panorama)
        {
            List<Accommodation> toReturn = new List<Accommodation>();
            if (location != null && name != null)
            {
                List<Accommodation> temploc = new List<Accommodation>();
                List<Accommodation> tempname = new List<Accommodation>();
                foreach (Accommodation acc in this.Data.GetAllAccommodation())
                {
                    if (this.GetCityFromAddress(acc.Address) == location && acc.Name == name)
                    {
                        toReturn.Add(acc);
                    }
                    else if (this.GetCityFromAddress(acc.Address) == location && acc.Name != name)
                    {
                        temploc.Add(acc);
                    }
                    else if (this.GetCityFromAddress(acc.Address) != location && acc.Name == name)
                    {
                        tempname.Add(acc);
                    }
                }

                toReturn = this.SortAccomodationByBools(toReturn, wifi, animal, pool, panorama);
                temploc = this.SortAccomodationByBools(temploc, wifi, animal, pool, panorama);
                tempname = this.SortAccomodationByBools(tempname, wifi, animal, pool, panorama);
                toReturn.AddRange(temploc);
                toReturn.AddRange(tempname);
            }
            else if (location == null && name != null)
            {
                foreach (Accommodation acc in this.Data.GetAllAccommodation())
                {
                    if (acc.Name == name)
                    {
                        toReturn.Add(acc);
                    }
                }

                toReturn = this.SortAccomodationByBools(toReturn, wifi, animal, pool, panorama);
            }
            else if (location != null && name == null)
            {
                foreach (Accommodation acc in this.Data.GetAllAccommodation())
                {
                    if (this.GetCityFromAddress(acc.Address) == location)
                    {
                        toReturn.Add(acc);
                    }
                }

                toReturn = this.SortAccomodationByBools(toReturn, wifi, animal, pool, panorama);
            }
            else
            {
                toReturn = this.SortAccomodationByBools(this.Data.GetAllAccommodation(), wifi, animal, pool, panorama);
            }

            return toReturn;
        }

        /// <summary>
        /// Order list by properties
        /// </summary>
        /// <param name="accToSort">List of accommodation</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is animal friendly</param>
        /// <param name="pool">Is there pool</param>
        /// <param name="panorama">is there good panorama</param>
        /// <returns>Ordered list of accommodations</returns>
        private List<Accommodation> SortAccomodationByBools(List<Accommodation> accToSort, bool wifi, bool animal, bool pool, bool panorama)
        {
            List<Accommodation> temp0 = new List<Accommodation>();
            List<Accommodation> temp1 = new List<Accommodation>();
            List<Accommodation> temp2 = new List<Accommodation>();
            List<Accommodation> temp3 = new List<Accommodation>();
            List<Accommodation> temp4 = new List<Accommodation>();
            int boolComplated;
            foreach (Accommodation acc in accToSort)
            {
                boolComplated = 0;
                if (wifi && (bool)acc.Wifi)
                {
                    boolComplated++;
                }

                if (animal && (bool)acc.Animal)
                {
                    boolComplated++;
                }

                if (pool && (bool)acc.Pool)
                {
                    boolComplated++;
                }

                if (panorama && (bool)acc.Panorama)
                {
                    boolComplated++;
                }

                switch (boolComplated)
                {
                    case 0: temp0.Add(acc); break;
                    case 1: temp1.Add(acc); break;
                    case 2: temp2.Add(acc); break;
                    case 3: temp3.Add(acc); break;
                    case 4: temp4.Add(acc); break;
                }
            }

            temp0.OrderByDescending(x => x.Rating);
            temp1.OrderByDescending(x => x.Rating);
            temp2.OrderByDescending(x => x.Rating);
            temp3.OrderByDescending(x => x.Rating);
            temp4.OrderByDescending(x => x.Rating);
            temp4.AddRange(temp3);
            temp4.AddRange(temp2);
            temp4.AddRange(temp1);
            temp4.AddRange(temp0);
            return temp4;
        }

        /// <summary>
        /// Returns city name from address
        /// </summary>
        /// <param name="address">Address</param>
        /// <returns>City name</returns>
        private string GetCityFromAddress(string address)
        {
            if (address != null)
            {
                return address.Split(' ')[1];
            }

            return string.Empty;
        }
    }
}
