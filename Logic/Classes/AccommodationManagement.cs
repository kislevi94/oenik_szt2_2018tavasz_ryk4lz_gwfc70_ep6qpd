﻿// <copyright file="AccommodationManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using DataLayer;

    /// <summary>
    /// Accommodation management methods
    /// </summary>
    public class AccommodationManagement : IAccommodationManagement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccommodationManagement"/> class.
        /// </summary>
        /// <param name="data">Datalayer interface</param>
        public AccommodationManagement(IData data)
        {
            this.Data = data;
        }

        public IData Data { get; }

        /// <summary>
        /// Update accommodation rating
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="newRating">New rating</param>
        public void UpdateAccomodationreting(int userid, int accid, int newRating)
        {
            this.Data.UpdateAccRating(userid, accid, newRating);
        }

        /// <summary>
        /// Register accommodation
        /// </summary>
        /// <param name="name">Name of the accommodation</param>
        /// <param name="address">Address of the accommodation</param>
        /// <param name="advid">Id of the advertiser</param>
        /// <param name="picture">Picture of the accommodation</param>
        /// <param name="oneprice">One man room price</param>
        /// <param name="twoprice">Two man room price</param>
        /// <param name="threeprice">Three man room price</param>
        /// <param name="fourprice">Four man room price</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is the accommodation animal friendly</param>
        /// <param name="panorama">Is the panorama nice</param>
        /// <param name="pool">Is there a pool</param>
        public void RegisterAccommodation(string name, string address, int advid, string picture, int oneprice, int twoprice, int threeprice, int fourprice, bool wifi, bool animal, bool panorama, bool pool)
        {
            this.Data.InsertIntoAccomodation(name, address, advid, picture, oneprice, twoprice, threeprice, fourprice, wifi, animal, panorama, pool);
        }

        /// <summary>
        /// Update accommodation
        /// </summary>
        /// <param name="id">Id of the accommodation</param>
        /// <param name="name">Name of the accommodation</param>
        /// <param name="address">Address of the accommodation</param>
        /// <param name="picture">Picture of the accommodation</param>
        /// <param name="oneprice">One man room price</param>
        /// <param name="twoprice">Two man room price</param>
        /// <param name="threeprice">Three man room price</param>
        /// <param name="fourprice">Four man room price</param>
        /// <param name="wifi">Is there wifi</param>
        /// <param name="animal">Is the accommodation animal friendly</param>
        /// <param name="panorama">Is the panorama nice</param>
        /// <param name="pool">Is there a pool</param>
        public void ModifyAccommodation(int id, string name, string address, string picture, int oneprice, int twoprice, int threeprice, int fourprice, bool wifi, bool animal, bool panorama, bool pool)
        {
            this.Data.UpdateAccommodation(id, name, address, picture, oneprice, twoprice, threeprice, fourprice, wifi, animal, panorama, pool);
        }

        /// <summary>
        /// Register new moderator connection
        /// </summary>
        /// <param name="userid">Id of the moderator</param>
        /// <param name="accid">Id of the accommodation</param>
        public void AddModeratorConnetion(int userid, int accid)
        {
            this.Data.InsertIntoModerator(userid, accid);
        }

        /// <summary>
        /// Delete accommodation
        /// </summary>
        /// <param name="accid">Id of the accommodation</param>
        public void DeleteAccomondation(int accid)
        {
            this.Data.RemoveAccommodation(accid);
        }

        /// <summary>
        /// Remove moderator connection
        /// </summary>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="userid">Id of the user</param>
        public void RemoveModerator(int accid, int userid)
        {
            this.Data.RemoveModeratorByUserAndAccId(accid, userid);
        }

        /// <summary>
        /// Lists all accommodation
        /// </summary>
        /// <returns>List of all accommodation</returns>
        public List<Accommodation> ListAccommodation()
        {
            return this.Data.GetAllAccommodation();
        }

        /// <summary>
        /// Lists best rated accommodations
        /// </summary>
        /// <param name="count">Number of listed accommodation</param>
        /// <returns>List of best rated accommodation</returns>
        public List<Accommodation> ListBestRatedAccommodation(int count)
        {
            var temp = from acc in this.Data.GetAllAccommodation()
                       orderby acc.Rating descending
                       select acc;
            List<Accommodation> toReturn = new List<Accommodation>();
            for (int i = 0; i < temp.Count() && i < count; i++)
            {
                toReturn.Add(temp.ElementAt(i));
            }

            return toReturn;
        }

        /// <summary>
        /// Lists accommodations owned by advertiser
        /// </summary>
        /// <param name="advid">Id of the advertiser</param>
        /// <returns>List of accommodation</returns>
        public List<Accommodation> ListAdvertiserOwnedAccommodation(int advid)
        {
            return this.Data.OwnedAccomondation(advid);
        }

        /// <summary>
        /// Lists accommodations moderated by same user
        /// </summary>
        /// <param name="userid">Id of the moderator</param>
        /// <returns>List of accommodation</returns>
        public List<Accommodation> ListUserModeratedAccomodation(int userid)
        {
            return this.Data.ModeratedAccommodation(userid);
        }
    }
}
