﻿// <copyright file="MessageManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using DataLayer;

    /// <summary>
    /// Message management methods
    /// </summary>
    public class MessageManagement : IMessageManagement
    {
        public MessageManagement(IData data)
        {
            this.Data = data;
        }

        public IData Data { get; }

        /// <summary>
        /// Send message
        /// </summary>
        /// <param name="nameFrom">Name of sender</param>
        /// <param name="nameTo">Name of receiver</param>
        /// <param name="accid">If of accommodation</param>
        /// <param name="subject">Subject of the message</param>
        /// <param name="text">Text of the message</param>
        public void SendMessage(string nameFrom, string nameTo, int accid, string subject, string text)
        {
            this.Data.InsertIntoMessage(nameFrom, nameTo, accid, DateTime.Now, subject, text);
        }

        /// <summary>
        /// Return incoming message
        /// </summary>
        /// <param name="typeCode">Type code</param>
        /// <param name="id">Id of the user</param>
        /// <returns>List of incoming messages</returns>
        public List<Message> GetIncomingMessages(string typeCode, int id)
        {
            return this.Data.ReturnIncamingMessagesById(typeCode + id);
        }

        /// <summary>
        /// Return outgoing message
        /// </summary>
        /// <param name="typeCode">Type code</param>
        /// <param name="id">Id of the user</param>
        /// <returns>List of outgoing messages</returns>
        public List<Message> GetOutgoingMessages(string typeCode, int id)
        {
            return this.Data.ReturnOutGoingMessagesById(typeCode + id);
        }
    }
}
