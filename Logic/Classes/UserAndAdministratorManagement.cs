﻿// <copyright file="UserAndAdministratorManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using DataLayer;

    /// <summary>
    /// User management methods
    /// </summary>
    public class UserAndAdministratorManagement : IUserAndAdministratorManagement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserAndAdministratorManagement"/> class.
        /// </summary>
        /// <param name="data">DataLayer connection</param>
        public UserAndAdministratorManagement(IData data)
        {
            this.Data = data;
        }

        public IData Data { get; }

        /// <summary>
        /// Returns bool value whether the user can rate the accommodation
        /// </summary>
        /// <param name="isUser">Is user</param>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        /// <returns>Whether the user can rate the accommodation</returns>
        public bool IsAbleToRate(bool isUser, int userid, int accid)
        {
            List<Reservation> reservations = this.Data.ReturnReservationsByUseridAndAccid(userid, accid);
            int index = 0;
            bool decesion = false;
            while (!decesion && index < reservations.Count)
            {
                if (isUser)
                {
                    if (reservations.ElementAt(index).Status == Status.Completed.ToString() || reservations.ElementAt(index).Status == Status.SzRated.ToString())
                    {
                        decesion = true;
                    }
                }
                else
                {
                    if (reservations.ElementAt(index).Status == Status.Completed.ToString() || reservations.ElementAt(index).Status == Status.URated.ToString())
                    {
                        decesion = true;
                    }
                }

                index++;
            }

            return decesion;
        }

        /// <summary>
        /// Rate user
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="newRating">New rating</param>
        public void UpdateUserRating(int userid, int accid, int newRating)
        {
            this.Data.UpdateUserRating(userid, accid, newRating);
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="userid">Id of the user</param>
        public void DeleteUser(int userid)
        {
            this.Data.RemoveUser(userid);
        }

        /// <summary>
        /// Delete Advertiser
        /// </summary>
        /// <param name="advid">Id of the advertiser</param>
        public void DeleteAdvertiser(int advid)
        {
            this.Data.RemoveAdvertiser(advid);
        }

        /// <summary>
        /// Remove moderator connection
        /// </summary>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="userid">Id of the user</param>
        public void RemoveModerator(int accid, int userid)
        {
            this.Data.RemoveModeratorByUserAndAccId(accid, userid);
        }

        /// <summary>
        /// Update user data
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <param name="password">Password of the user</param>
        /// <param name="name">Name of the user</param>
        /// <param name="email">Email of the user</param>
        /// <param name="phone">Phone number of the user</param>
        /// <param name="address">Adress of the user</param>
        public void ModifyUser(int id, string password, string name, string email, string phone, string address)
        {
            this.Data.UpdateUser(id, password, name, email, phone, address);
        }

        /// <summary>
        /// Update advertiser data
        /// </summary>
        /// <param name="advertiserid">Id of the advertiser</param>
        /// <param name="password">Password of the advertiser</param>
        /// <param name="name">Name of the advertiser</param>
        /// <param name="email">Email of the advertiser</param>
        /// <param name="phone">Phone number of the advertiser</param>
        /// <param name="address">Address of the advertiser</param>
        public void ModifyAdvertiser(int advertiserid, string password, string name, string email, string phone, string address)
        {
            this.Data.UpdateAdvertiser(advertiserid, password, name, email, phone, address);
        }

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns>List of all users</returns>
        public List<Users> ListUsers()
        {
            return this.Data.GetAllUser();
        }

        /// <summary>
        /// Returns all advertiser
        /// </summary>
        /// <returns>List of all advertiser</returns>
        public List<Advertiser> ListAdvertiser()
        {
            return this.Data.GetAllAdvertiser();
        }

        /// <summary>
        /// Returns user by id
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns>User with correct id</returns>
        public Users GetUserById(int id)
        {
            return this.Data.ReturnUserById(id);
        }

        /// <summary>
        /// Returns advertiser by id
        /// </summary>
        /// <param name="id">Id of the advertiser</param>
        /// <returns>Advertiser with correct id</returns>
        public Advertiser GetAdvertiserById(int id)
        {
            return this.Data.ReturnAdvertiserbyID(id);
        }
    }
}
