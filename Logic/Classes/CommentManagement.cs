﻿// <copyright file="CommentManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using DataLayer;

    /// <summary>
    /// Comment management methods
    /// </summary>
    public class CommentManagement : ICommentManagement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommentManagement"/> class.
        /// </summary>
        /// <param name="data">Datalayer connection</param>
        public CommentManagement(IData data)
        {
            this.Data = data;
        }

        public IData Data { get; }

        /// <summary>
        /// Make a new comment
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="text">Text of the comment</param>
        public void MakeComment(int userid, int accid, string text)
        {
            this.Data.InsertIntoComment(userid, accid, text);
        }

        /// <summary>
        /// Lists comments of an accommodation
        /// </summary>
        /// <param name="accid">Id of the accomodation</param>
        /// <returns>List of comment</returns>
        public List<Comment> GetComments(int accid)
        {
            var temp = from cmt in this.Data.GetAllComment()
                              where cmt.Accommodationid == accid
                              select cmt;
            return temp.ToList();
        }
    }
}
