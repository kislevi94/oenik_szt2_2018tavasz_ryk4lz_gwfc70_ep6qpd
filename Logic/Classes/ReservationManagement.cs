﻿// <copyright file="ReservationManagement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataLayer;

    /// <summary>
    ///  Reservation management methods
    /// </summary>
    public class ReservationManagement : IReservationManagement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReservationManagement"/> class.
        /// </summary>
        /// <param name="data">DataLayer connection</param>
        public ReservationManagement(IData data)
        {
            this.Data = data;
        }

        public IData Data { get; }

        /// <summary>
        /// Make reservation
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="accomondationid">Id of the accommodation</param>
        /// <param name="roomtype">Number of people for the room</param>
        /// <param name="startDate">Reservation start time</param>
        /// <param name="length">Number of days</param>
        public void MakeReservation(int userid, int accomondationid, int roomtype, DateTime startDate, int length)
        {
            int price = 0;
            switch (roomtype)
            {
                case 1: price = (int)this.Data.ReturnAccommodationById(accomondationid).OnePersonRoomPrice; break;
                case 2: price = (int)this.Data.ReturnAccommodationById(accomondationid).TwoPersonRoomPrice; break;
                case 3: price = (int)this.Data.ReturnAccommodationById(accomondationid).ThreePersonRoomPrice; break;
                case 4: price = (int)this.Data.ReturnAccommodationById(accomondationid).FourPersonRoomPrice; break;
            }

            this.Data.InsertIntoReservation(userid, accomondationid, roomtype, price, startDate, length, Status.Unprocessed);
        }

        /// <summary>
        /// Update reservation status
        /// </summary>
        /// <param name="resid">Id of the reservation</param>
        /// <param name="endStatus">Status changed to</param>
        public void UpdateReservationStatus(int resid, Status endStatus)
        {
            this.Data.UpdateReservationStatus(resid, endStatus);
        }

        /// <summary>
        /// Lists reservation by user
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <returns>List of reservations</returns>
        public List<Reservation> ListUserReservation(int userid)
        {
            return (from res in this.Data.GetAllReservation()
                    where res.Userid == userid
                    select res).ToList();
        }

        /// <summary>
        /// List reservations by accommodation
        /// </summary>
        /// <param name="accid">Id of the accommodation</param>
        /// <returns>list of reservations</returns>
        public List<Reservation> ListAccommodationReservation(int accid)
        {
            return (from res in this.Data.GetAllReservation()
                    where res.Accommodationid == accid
                    select res).ToList();
        }

        /// <summary>
        /// Lists reservations by accommodation and status
        /// </summary>
        /// <param name="accid">Id of the accommodation</param>
        /// <param name="s">Status of the reservation</param>
        /// <returns>List of reservations</returns>
        public List<Reservation> ListAccommodationReservationByStatus(int accid, Status s)
        {
            return (from res in this.ListAccommodationReservation(accid)
                    where res.Status == s.ToString()
                    select res).ToList();
        }

        /// <summary>
        /// List reservation by user and status
        /// </summary>
        /// <param name="userid">Id of the user</param>
        /// <param name="s">Status of the reservation</param>
        /// <returns>List of reservations</returns>
        public List<Reservation> ListUserReservationByStatus(int userid, Status s)
        {
            return (from res in this.ListUserReservation(userid)
                    where res.Status == s.ToString()
                    select res).ToList();
        }
    }
}
