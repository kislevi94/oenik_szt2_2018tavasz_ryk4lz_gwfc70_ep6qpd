﻿// <copyright file="Authentication.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
#pragma warning restore SA1652 // Enable XML documentation output
{
    using System;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using DataLayer;

    /// <summary>
    /// Authentication and Registration methods
    /// </summary>
    public class Authentication : IAuthentication
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Authentication"/> class.
        /// </summary>
        /// <param name="data">Datalayer interface</param>
        public Authentication(IData data)
        {
            this.Data = data;
        }

        private IData Data { get; }

        /// <summary>
        /// Registrate an user
        /// </summary>
        /// <param name="isAdv">Is advertiser</param>
        /// <param name="username">Username</param>
        /// <param name="password">Passord</param>
        /// <param name="name">Name</param>
        /// <param name="email">Email</param>
        /// <param name="phone">Phone</param>
        /// <param name="addresscode">Address code</param>
        /// <param name="city">City</param>
        /// <param name="address">Rest of address</param>
        public void Registration(bool isAdv, string username, string password, string name, string email, string phone, string addresscode, string city, string address)
        {
            if (this.IsUserNameValid(username))
            {
                if (isAdv)
                {
                    this.Data.InsertIntoAdvertiser(username, this.GenerateSHA2Hash(password), name, email, phone, addresscode + " " + city + " " + address);
                }
                else
                {
                    this.Data.InsertIntoUser(username, this.GenerateSHA2Hash(password), name, email, phone, addresscode + " " + city + " " + address);
                }
            }
            else
            {
                throw new Exception("Username already taken");
            }
        }

        /// <summary>
        /// Authenticate an user
        /// </summary>
        /// <param name="username">Username of the user</param>
        /// <param name="password">Password of the user</param>
        /// <returns>Authenticated user</returns>
        public Users UserAuthentication(string username, string password)
        {
            Users tempuser = this.Data.GetAllUser().Single(user => user.Username == username);
            if (this.CheckPassword(true, tempuser, password))
            {
                return tempuser;
            }

            throw new Exception("Incorrect username or password");
        }

        /// <summary>
        /// Authenticate an advertiser
        /// </summary>
        /// <param name="username">Username of the advetiser</param>
        /// <param name="password">Password of the advertiser</param>
        /// <returns>Authenticated advertiser</returns>
        public Advertiser AdvertiserAuthentication(string username, string password)
        {
            Advertiser tempadv = this.Data.GetAllAdvertiser().Single(adv => adv.Username == username);
            if (this.CheckPassword(false, tempadv, password))
            {
                return tempadv;
            }

            throw new Exception("Incorrect username or password");
        }

        /// <summary>
        /// Checks if username is unique
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>Result of the decision</returns>
        private bool IsUserNameValid(string username)
        {
            bool valid = false;
            Advertiser a = this.Data.GetAllAdvertiser().SingleOrDefault(x => x.Username == username);
            Users u = this.Data.GetAllUser().SingleOrDefault(x => x.Username == username);
            if (a == null && u == null)
            {
                valid = true;
            }

            return valid;
        }

        /// <summary>
        /// Checks if the MD5 hash generated from input string equals the one stored in database
        /// </summary>
        /// <param name="isUser">Decide if it is user or advertiser</param>
        /// <param name="entity">reference entity to check</param>
        /// <param name="password">Password input string to check</param>
        /// <returns>Result of checkink if the SHA2 hash generated from input string equals the one stored in database</returns>
        private bool CheckPassword(bool isUser, object entity, string password)
        {
            string dbPassword = string.Empty;
            if (isUser)
            {
                dbPassword = (entity as Users).Password;
            }
            else
            {
                dbPassword = (entity as Advertiser).Password;
            }

            if (this.GenerateSHA2Hash(password) == dbPassword)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Generate SHA2 hash from a string
        /// </summary>
        /// <param name="input">String input for the method</param>
        /// <returns>Generated hash</returns>
        private string GenerateSHA2Hash(string input)
        {
            UnicodeEncoding uE = new UnicodeEncoding();
            byte[] hashValue;
            byte[] message = uE.GetBytes(input);
            SHA512Managed hashString = new SHA512Managed();
            string encodedData = Convert.ToBase64String(message);
            string hex = string.Empty;
            hashValue = hashString.ComputeHash(uE.GetBytes(encodedData));
            foreach (byte x in hashValue)
            {
                hex += string.Format("{0:X2}", x);
            }

            return hex;
        }
    }
}
