﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OgavirTours
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using OgavirTours.Pages;
    using OgavirTours.Util.Entities;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            Switcher.PageSwitcher = this;
            ApplicationState state = new ApplicationState();
            state.CurrentUser = null;
            Switcher.Switch(new MainPage(), state);
        }

        /// <summary>
        /// Navigates to the given page.
        /// </summary>
        /// <param name="nextPage">Next Page</param>
        [Obsolete]
        public void Navigate(UserControl nextPage)
        {
            this.Content = nextPage;
        }

        /// <summary>
        /// Navigates to the next page if that is ISwitchable
        /// </summary>
        /// <param name="nextPage">Next Page</param>
        /// <param name="state">State of the next page </param>
        public void Navigate(UserControl nextPage, ApplicationState state)
        {
            this.Content = nextPage;
            ISwitchable s = nextPage as ISwitchable;
            if (s != null)
            {
                s.UtilizeState(state);
            }
            else
            {
                throw new ArgumentException("NextPage is not ISwitchable  " + nextPage.Name.ToString());
            }
        }
    }
}
