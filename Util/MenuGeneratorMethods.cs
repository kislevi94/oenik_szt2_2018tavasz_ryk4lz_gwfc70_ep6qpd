﻿// <copyright file="MenuGeneratorMethods.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Util
{
    using System.Collections.ObjectModel;
    using System.Windows.Controls;

    /// <summary>
    /// Vreates MenuItems due to permission
    /// </summary>
    public class MenuGeneratorMethods
    {
        internal ObservableCollection<MenuItem> CreateLoggedInUserMenuItems()
        {
            return new ObservableCollection<MenuItem>()
            {
                new MenuItem() { Header = "Main Page" },
                new MenuItem() { Header = "Messages" },
                new MenuItem() { Header = "My reservations" },
                new MenuItem() { Header = "Special search" },
                new MenuItem() { Header = "Log out" },
                new MenuItem() { Header = "Exit" }
            };
        }

        internal ObservableCollection<MenuItem> CreateNotLoggedInUserMenuItems()
        {
            {
                return new ObservableCollection<MenuItem>()
                {
                    new MenuItem() { Header = "Main Page" },
                    new MenuItem() { Header = "Exit" }
                };
            }
        }

        internal ObservableCollection<MenuItem> CreateAdvertiserMenuItems()
        {
            return new ObservableCollection<MenuItem>()
            {
                new MenuItem() { Header = "Main Page" },
                new MenuItem() { Header = "Messages" },
                new MenuItem() { Header = "Handle reservations" },
                new MenuItem() { Header = "Special search" },
                new MenuItem() { Header = "Log out" },
                new MenuItem() { Header = "Exit" }
            };
        }

        internal ObservableCollection<MenuItem> CreateModeratorMenuItems()
        {
            return new ObservableCollection<MenuItem>()
            {
                new MenuItem() { Header = "Main Page" },
                new MenuItem() { Header = "Messages" },
                new MenuItem() { Header = "Handle reservations" },
                new MenuItem() { Header = "Special search" },
                new MenuItem() { Header = "Log out" },
                new MenuItem() { Header = "Exit" }
            };
        }

        internal ObservableCollection<MenuItem> CreateAdminMenuItems()
        {
            return new ObservableCollection<MenuItem>()
            {
                new MenuItem() { Header = "Main Page" },
                new MenuItem() { Header = "Messages" },
                new MenuItem() { Header = "Handle reservations" },
                new MenuItem() { Header = "Special search" },
                new MenuItem() { Header = "List and moderate" },
                new MenuItem() { Header = "Log out" },
                new MenuItem() { Header = "Exit" }
            };
        }
    }
}