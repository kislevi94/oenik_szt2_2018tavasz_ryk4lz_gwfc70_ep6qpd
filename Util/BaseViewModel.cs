﻿// <copyright file="BaseViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Controls;

    /// <summary>
    /// Children of this class are bindable from xaml.
    /// </summary>
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<MenuItem> menu;

        /// <summary>
        /// Event for property changing.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets menu
        /// </summary>
        public ObservableCollection<MenuItem> Menu
        {
            get
            {
                return this.menu;
            }

            set
            {
                this.menu = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// If not null, PropertyChanged
        /// </summary>
        /// <param name="s">Caller mmber name</param>
        public void OnPropertyChanged([CallerMemberName] string s = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }
    }
}
