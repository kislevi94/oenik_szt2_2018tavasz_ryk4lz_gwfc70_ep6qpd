﻿// <copyright file="Permission.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Util.Enums
{
    /// <summary>
    /// Types of permissions due to the type of the user.
    /// </summary>
    public enum Permission
    {
        /// <summary>
        /// Basic logged in user.
        /// </summary>
        UserLoggedIn,

        /// <summary>
        /// User who can just search and watch accomodations profiles
        /// </summary>
        UserNotLoggedIn,

        /// <summary>
        /// Same as moderator, but can add new accomodation and delete them.
        /// </summary>
        Advertiser,

        /// <summary>
        /// User who has perm. to modify accomodation profiles, rate visitors etc.
        /// </summary>
        Moderator
    }
}
