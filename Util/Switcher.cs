﻿//-----------------------------------------------------------------------
// <copyright file="Switcher.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OgavirTours
{
    using System.Windows.Controls;

    /// <summary>
    /// Handles switching between pages
    /// </summary>
    public static class Switcher
    {
        /// <summary>
        /// Where all of the pages are shown.
        /// </summary>
        private static MainWindow pageSwitcher;

        /// <summary>
        /// Gets or sets  pageSwitcher.
        /// </summary>
        public static MainWindow PageSwitcher
        {
            get
            {
                return pageSwitcher;
            }

            set
            {
                pageSwitcher = value;
            }
        }

        /// <summary>
        /// Navigates to the given page and passes information between pages
        /// </summary>
        /// <param name="newPage">Destination page</param>
        /// <param name="state">Contains information (need to be parsed)</param>
        public static void Switch(UserControl newPage, ApplicationState state)
        {
            PageSwitcher.Navigate(newPage, state);
        }
    }
}
