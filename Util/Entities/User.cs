﻿// <copyright file="User.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Util.Entities
{
    using OgavirTours.Interfaces;

    /// <summary>
    /// Represents a <see cref="User"/>
    /// </summary>
    public class User : BaseViewModel, IUserEntity
    {
        private int userId;
        private string username;
        private string password;
        private string email;
        private string phoneNumber;
        private string address;
        private bool isModerator;
        private double rating;
        private int ratingNumber;
        private string name;

        /// <summary>
        /// Gets or sets username
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }

            set
            {
                this.username = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets password
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                this.password = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets name
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets email
        /// </summary>
        public string Email
        {
            get
            {
                return this.email;
            }

            set
            {
                this.email = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets phonenumber
        /// </summary>
        public string PhoneNumber
        {
            get
            {
                return this.phoneNumber;
            }

            set
            {
                this.phoneNumber = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets address
        /// </summary>
        public string Address
        {
            get
            {
                return this.address;
            }

            set
            {
                this.address = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets ismoderator
        /// </summary>
        public bool IsModerator
        {
            get
            {
                return this.isModerator;
            }

            set
            {
                this.isModerator = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets rating
        /// </summary>
        public double Rating
        {
            get
            {
                return this.rating;
            }

            set
            {
                this.rating = value;
                this.OnPropertyChanged();
            }
        }

        public int UserId
        {
            get
            {
                return this.userId;
            }

            set
            {
                this.userId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets userid
        /// </summary>
        public int Ratingnumber
        {
            get
            {
                return this.ratingNumber;
            }

            set
            {
                this.ratingNumber = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
