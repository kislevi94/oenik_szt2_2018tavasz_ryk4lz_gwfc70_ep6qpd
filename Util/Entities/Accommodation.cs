﻿// <copyright file="Accommodation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace OgavirTours.Util.Entities
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Media.Imaging;
    using static System.Net.Mime.MediaTypeNames;

    /// <summary>
    /// Accommodation entity view model
    /// </summary>
    public class Accommodation : BaseViewModel
    {
        private Advertiser advertiser;
        private int accommodationid;
        private string name;
        private string address;
        private int advertiserid;
        private BitmapImage image;
        private int onePersonRoomPrice;
        private int twoPersonRoomPrice;
        private int threePersonRoomPrice;
        private int fourPersonRoomPrice;
        private bool wifi;
        private bool petFriendly;
        private bool luggageRoom;
        private bool pool;
        private double rating;
        private int ratingNumber;
        private ObservableCollection<Comment> comments;
        private ObservableCollection<Moderator> moderator;
        private ObservableCollection<Reservation> reservation;

        /// <summary>
        /// Initializes a new instance of the <see cref="Accommodation"/> class.
        /// </summary>
        public Accommodation()
        {
            this.comments = new ObservableCollection<Comment>();
            this.moderator = new ObservableCollection<Moderator>();
            this.reservation = new ObservableCollection<Reservation>();
        }

        /// <summary>
        /// Gets or sets AccommodationId
        /// </summary>
        public int Accommodationid
        {
            get
            {
                return this.accommodationid;
            }

            set
            {
                this.accommodationid = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Name
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Address
        /// </summary>
        public string Address
        {
            get
            {
                return this.address;
            }

            set
            {
                this.address = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Advertiserid
        /// </summary>
        public int Advertiserid
        {
            get
            {
                return this.advertiserid;
            }

            set
            {
                this.advertiserid = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Image
        /// </summary>
        public BitmapImage Image
        {
            get
            {
                return this.image;
            }

            set
            {
                this.image = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets OnePersonRoomPrice
        /// </summary>
        public int OnePersonRoomPrice
        {
            get
            {
                return this.onePersonRoomPrice;
            }

            set
            {
                this.onePersonRoomPrice = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets TwoPersonRoomPrice
        /// </summary>
        public int TwoPersonRoomPrice
        {
            get
            {
                return this.twoPersonRoomPrice;
            }

            set
            {
                this.twoPersonRoomPrice = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets ThreePersonRoomPrice
        /// </summary>
        public int ThreePersonRoomPrice
        {
            get
            {
                return this.threePersonRoomPrice;
            }

            set
            {
                this.threePersonRoomPrice = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets FourPersonRoomPrice
        /// </summary>
        public int FourPersonRoomPrice
        {
            get
            {
                return this.fourPersonRoomPrice;
            }

            set
            {
                this.fourPersonRoomPrice = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is there wifi in the accommodation
        /// </summary>
        public bool Wifi
        {
            get
            {
                return this.wifi;
            }

            set
            {
                this.wifi = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is the accommodation pet-friendly
        /// </summary>
        public bool PetFriendly
        {
            get
            {
                return this.petFriendly;
            }

            set
            {
                this.petFriendly = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is there luggage-room in the accommodation
        /// </summary>
        public bool LuggageRoom
        {
            get
            {
                return this.luggageRoom;
            }

            set
            {
                this.luggageRoom = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is there pool in the accommodation
        /// </summary>
        public bool Pool
        {
            get
            {
                return this.pool;
            }

            set
            {
                this.pool = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Rating
        /// </summary>
        public double Rating
        {
            get
            {
                return this.rating;
            }

            set
            {
                this.rating = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Nuber of ratings
        /// </summary>
        public int RatingNumber
        {
            get
            {
                return this.ratingNumber;
            }

            set
            {
                this.ratingNumber = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the advertiser of the accommodation
        /// </summary>
        public Advertiser Advertiser
        {
            get
            {
                return this.advertiser;
            }

            set
            {
                this.advertiser = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the comments of the accommodation
        /// </summary>
        public ObservableCollection<Comment> Comments
        {
            get
            {
                return this.comments;
            }

            set
            {
                this.comments = value;
            }
        }

        /// <summary>
        /// Gets the moderators of the accommodation
        /// </summary>
        public ICollection<Moderator> Moderator
        {
            get
            {
                return this.moderator;
            }
        }

        /// <summary>
        /// Gets the reservations of the accommodation
        /// </summary>
        public ICollection<Reservation> Reservation
        {
            get
            {
                return this.reservation;
            }
        }

        /// <summary>
        /// When an accommodations' ToString() is called, returns the name
        /// </summary>
        /// <returns>Current accommodationss' name</returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}