﻿// <copyright file="Comment.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Util.Entities
{
    using System;

    public class Comment : BaseViewModel
    {
        private int commentId;
        private int userId;
        private int accommodationId;
        private DateTime commentDate;
        private string text;
        private string username;

        /// <summary>
        /// Gets or sets Username
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }

            set
            {
                this.username = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Commentid
        /// </summary>
        public int Commentid
        {
            get
            {
                return this.commentId;
            }

            set
            {
                this.commentId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Userid
        /// </summary>
        public int Userid
        {
            get
            {
                return this.userId;
            }

            set
            {
                this.userId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Accommodationid
        /// </summary>
        public int Accommodationid
        {
            get
            {
                return this.accommodationId;
            }

            set
            {
                this.accommodationId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Commentdate
        /// </summary>
        public DateTime Commentdate
        {
            get
            {
                return this.commentDate;
            }

            set
            {
                this.commentDate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Text of the comment
        /// </summary>
        public string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                this.text = value;
                this.OnPropertyChanged();
            }
        }
    }
}
