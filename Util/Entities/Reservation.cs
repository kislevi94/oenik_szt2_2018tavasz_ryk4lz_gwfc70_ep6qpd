﻿// <copyright file="Reservation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Util.Entities
{
    public class Reservation : BaseViewModel
    {
        private int reservationId;
        private int userId;
        private int accommodationId;
        private int roomType;
        private int price;
        private System.DateTime startDate;
        private System.DateTime reservationDate;
        private int length;
        private string status;
        private User user;

        /// <summary>
        /// Gets or sets the Id of the reservation
        /// </summary>
        public int ReservationId
        {
            get
            {
                return this.reservationId;
            }

            set
            {
                this.reservationId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the UserId
        /// </summary>
        public int UserId
        {
            get
            {
                return this.userId;
            }

            set
            {
                this.userId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the AccommodationId of the reservation
        /// </summary>
        public int AccommodationId
        {
            get
            {
                return this.accommodationId;
            }

            set
            {
                this.accommodationId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the RoomType of the reservation
        /// </summary>
        public int RoomType
        {
            get
            {
                return this.roomType;
            }

            set
            {
                this.roomType = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Price of the reservation
        /// </summary>
        public int Price
        {
            get
            {
                return this.price;
            }

            set
            {
                this.price = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the StartDate of the reservation
        /// </summary>
        public System.DateTime StartDate
        {
            get
            {
                return this.startDate;
            }

            set
            {
                this.startDate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the ReservationDate of the reservation
        /// </summary>
        public System.DateTime ReservationDate
        {
            get
            {
                return this.reservationDate;
            }

            set
            {
                this.reservationDate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Length of the reservation
        /// </summary>
        public int Length
        {
            get
            {
                return this.length;
            }

            set
            {
                this.length = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Status of the reservation
        /// </summary>
        public string Status
        {
            get
            {
                return this.status;
            }

            set
            {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the User
        /// </summary>
        public User User
        {
            get
            {
                return this.user;
            }

            set
            {
                this.user = value;
                this.OnPropertyChanged();
            }
        }
    }
}
