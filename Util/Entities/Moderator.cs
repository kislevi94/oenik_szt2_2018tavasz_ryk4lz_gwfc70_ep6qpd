﻿// <copyright file="Moderator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Util.Entities
{
    public class Moderator : User
    {
        private int moderatorId;
        private int userId;
        private int accomodationId;
        private User user;

        /// <summary>
        /// Gets or sets the Id of the moderator
        /// </summary>
        public int ModeratorId
        {
            get
            {
                return this.moderatorId;
            }

            set
            {
                this.moderatorId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the UserId of the moderator
        /// </summary>
        public int UserId
        {
            get
            {
                return this.userId;
            }

            set
            {
                this.userId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the AccommodationId
        /// </summary>
        public int Accommodationid
        {
            get
            {
                return this.accomodationId;
            }

            set
            {
                this.accomodationId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets User
        /// </summary>
        public User User
        {
            get
            {
                return this.user;
            }

            set
            {
                this.user = value;
                this.OnPropertyChanged();
            }
        }
    }
}
