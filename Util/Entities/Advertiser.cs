﻿// <copyright file="Advertiser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Util.Entities
{
    using OgavirTours.Interfaces;

    public class Advertiser : BaseViewModel, IUserEntity
    {
        private int advertiserid;
        private string username;
        private string password;
        private string name;
        private string email;
        private string phonenumber;
        private string address;

        /// <summary>
        /// Gets or sets Advertiserid
        /// </summary>
        public int Advertiserid
        {
            get
            {
                return this.advertiserid;
            }

            set
            {
                this.advertiserid = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Username
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }

            set
            {
                this.username = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Password
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                this.password = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Name
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        public string Email
        {
            get
            {
                return this.email;
            }

            set
            {
                this.email = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets PhoneNumber
        /// </summary>
        public string PhoneNumber
        {
            get
            {
                return this.phonenumber;
            }

            set
            {
                this.phonenumber = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Address
        /// </summary>
        public string Address
        {
            get
            {
                return this.address;
            }

            set
            {
                this.address = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// When an advertisers' ToString() is called, returns the name
        /// </summary>
        /// <returns>Current advertisers' name</returns>
        public override string ToString()
        {
            return this.name;
        }
    }
}
