﻿// <copyright file="Message.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours.Util.Entities
{
    public class Message : BaseViewModel
    {
        private int messageId;
        private string from;
        private string to;
        private System.DateTime date;
        private string subject;
        private string text;

        /// <summary>
        /// Gets or sets MessageId
        /// </summary>
        public int MessageId
        {
            get
            {
                return this.messageId;
            }

            set
            {
                this.messageId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets who the message is from
        /// </summary>
        public string From
        {
            get
            {
                return this.from;
            }

            set
            {
                this.from = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets who the message is to
        /// </summary>
        public string To
        {
            get
            {
                return this.to;
            }

            set
            {
                this.to = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets Date of the message
        /// </summary>
        public System.DateTime Date
        {
            get
            {
                return this.date;
            }

            set
            {
                this.date = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Subject of the message
        /// </summary>
        public string Subject
        {
            get
            {
                return this.subject;
            }

            set
            {
                this.subject = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the text of the message
        /// </summary>
        public string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                this.text = value;
                this.OnPropertyChanged();
            }
        }
    }
}
