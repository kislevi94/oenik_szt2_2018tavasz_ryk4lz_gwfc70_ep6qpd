﻿// <copyright file="ApplicationState.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours
{
    using System.Collections.ObjectModel;
    using System.Windows.Controls;
    using OgavirTours.Interfaces;
    using OgavirTours.Pages;
    using OgavirTours.Util;
    using OgavirTours.Util.Entities;

    public class ApplicationState
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationState"/> class.
        /// </summary>
        public ApplicationState()
        {
            this.LogicsHandler = new LogicsHandler();
            this.MenuGeneratorMethods = new MenuGeneratorMethods();
        }

        /// <summary>
        /// Gets Logic
        /// </summary>
        public LogicsHandler LogicsHandler { get; private set; }

        /// <summary>
        /// Gets MenugeneratorMethods
        /// </summary>
        public MenuGeneratorMethods MenuGeneratorMethods { get; private set; }

        /// <summary>
        /// Gets or sets CurrentUser
        /// </summary>
        public IUserEntity CurrentUser { get; set; }

        /// <summary>
        /// Gets Menu due to permission
        /// </summary>
        public ObservableCollection<MenuItem> Menu
        {
            get
            {
                if (this.CurrentUser is User || this.CurrentUser == null)
                {
                    if (this.CurrentUser == null)
                    {
                        return this.MenuGeneratorMethods.CreateNotLoggedInUserMenuItems();
                    }

                    if ((this.CurrentUser as User).UserId == -1)
                    {
                        return this.MenuGeneratorMethods.CreateAdminMenuItems();
                    }

                    if ((this.CurrentUser as User).UserId == 0)
                    {
                        return this.MenuGeneratorMethods.CreateNotLoggedInUserMenuItems();
                    }
                    else if ((bool)(this.CurrentUser as User).IsModerator)
                    {
                        return this.MenuGeneratorMethods.CreateModeratorMenuItems();
                    }
                    else
                    {
                        return this.MenuGeneratorMethods.CreateLoggedInUserMenuItems();
                    }
                }
                else if (this.CurrentUser is Advertiser)
                {
                    return this.MenuGeneratorMethods.CreateAdvertiserMenuItems();
                }
                else
                {
                    throw new System.Exception("Cannot find the type of the entity");
                }
            }
        }

        /// <summary>
        /// Based on which menuitem the user clicks, redirect to the right usercontrol
        /// </summary>
        /// <param name="s">menuitems' header</param>
        public void MenuHandler(string s)
        {
            switch (s)
            {
                case "Exit":
                    System.Windows.Application.Current.Shutdown();
                    break;
                case "Main Page":
                    Switcher.Switch(new MainPage(), this);
                    break;
                case "Handle reservations":
                    Switcher.Switch(new ReservationHandlerPage(), this);
                    break;
                case "My reservations":
                    Switcher.Switch(new UserProfilePage(), this);
                    break;
                case "Special search":
                    Switcher.Switch(new AdvancedSearchPage(), this);
                    break;
                case "Log out":
                    this.CurrentUser = null;
                    Switcher.Switch(new MainPage(), this);
                    break;
                case "Messages":
                    Switcher.Switch(new MessagesPage(), this);
                    break;
                case "List and moderate":
                    Switcher.Switch(new AdminHandlerPage(), this);
                    break;
                case "My profile":
                    // TODO: own profile page
                    Switcher.Switch(new ReservationHandlerPage(), this);
                    break;
                default:
                    break;
            }
        }
    }
}