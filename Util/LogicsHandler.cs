﻿// <copyright file="LogicsHandler.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OgavirTours
{
    using DataLayer;
    using Logic;

    public class LogicsHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogicsHandler"/> class.
        /// </summary>
        public LogicsHandler()
        {
            IData data = new Queries();

            this.UserAndAdministratorManagement = new UserAndAdministratorManagement(data);
            this.Search = new Search(data);
            this.ReservationManagement = new ReservationManagement(data);
            this.MessageManagement = new MessageManagement(data);
            this.CommentManagement = new CommentManagement(data);
            this.Authentication = new Authentication(data);
            this.AccommodationManagement = new AccommodationManagement(data);
        }

        /// <summary>
        /// Gets UserAndAdministratorManagement interface
        /// </summary>
        public IUserAndAdministratorManagement UserAndAdministratorManagement { get; private set; }

        /// <summary>
        /// Gets Search interface
        /// </summary>
        public ISearch Search { get; private set; }

        /// <summary>
        /// Gets ReservationManagement interface
        /// </summary>
        public IReservationManagement ReservationManagement { get; private set; }

        /// <summary>
        /// Gets MessageManagement interface
        /// </summary>
        public IMessageManagement MessageManagement { get; private set; }

        /// <summary>
        /// Gets CommentManagement interface
        /// </summary>
        public ICommentManagement CommentManagement { get; private set; }

        /// <summary>
        /// Gets Authentication interface
        /// </summary>
        public IAuthentication Authentication { get; private set; }

        /// <summary>
        /// Gets AccommodationManagement interface
        /// </summary>
        public IAccommodationManagement AccommodationManagement { get; private set; }
    }
}
